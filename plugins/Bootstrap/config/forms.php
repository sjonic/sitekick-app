<?php
$config = [
	'inputContainer'      => '<div class="input-group {{type}}{{required}}">{{content}}</div>',
	'inputContainerError' => '<div class="input-group {{type}}{{required}} has-error">{{content}}{{error}}</div>',
	'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
    'checkboxFormGroup' => '{{label}}',
    'checkboxWrapper' => '<div class="checkbox">{{input}}{{label}}</div>',
	'nestingLabel' => '{{input}}<label{{attrs}}>{{text}}</label>',
    'label' => '<label{{attrs}}>{{text}}</label>',
    'error' => '<div class="help-block">{{content}}</div>',
    'submitContainer' => '{{content}}',
    'button' => '<button{{attrs}}>{{text}}</button>',
    'inputsubmit' => '<input type="{{type}}"{{attrs}}>',

];
