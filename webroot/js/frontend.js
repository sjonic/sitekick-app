/**
 * Global Sitekick-frontend functions
 * @author NOBEARS
 * @copyright NOBEARS
 */

$(document).ready(function(){

    //submit subscribe form
    if( $('#subscribe-form').length > 0 ){
        $('#subscribe-form').submit(function(event){
            var $form = $('#subscribe-form');
            event.preventDefault();
            // show loader
            $(this).find('input[type="submit"]')
                .prop('disabled', true)
                .attr('data-original-background', $(this).find('input[type="submit"]').css('background-image'))
                .css('background-image', 'url(/img/loading.gif)');

            // ajax call
            $.ajax({
                url     : $form.attr('action'),
                method  : 'post',
                data    : $form.serialize()
            }).done(function(response){
                $('#subscribe-form-success').remove();
                $form.after('<div class="alert alert-success" style="display: none;" id="subscribe-form-success" role="alert">'+response+'</div>');
                $('#subscribe-form-success').slideDown();
                // reload button
                $form.find('input[type="submit"]').css('background-image', $form.find('input[type="submit"]').data('original-background')).prop('disabled', false);
            }).error(function(response){
                $('#subscribe-form-success').remove();
                $form.after('<div class="alert alert-danger" style="display: none;" id="subscribe-form-error" role="alert">'+response+'</div>');
                $('#subscribe-form-error').slideDown();
            });
        });
    }
});