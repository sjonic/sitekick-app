$(document).ready(function(){
    initEditor();
});

var filePickerCallback = function(callback, value, meta) {
    // Provide file and text for the link dialog
    if (meta.filetype == 'file') {
        tinymce.activeEditor.windowManager.open({
            title: translation_core_27,
            url: '/sitekick/media/inlineLibrary/file',
            width: 600,
            height: 400,
        });
    }

    // Provide image and alt text for the image dialog
    if (meta.filetype == 'image') {
        tinymce.activeEditor.windowManager.open({
            title: translation_core_27,
            url: '/sitekick/media/inlineLibrary',
            width: 600,
            height: 400,
        });

    }
};

function initEditor(){

    // check for non-admin-users and non-editable-content
    if( adminUser !== true ) {
        $('._added-editor-message').remove();
        $('.redactor').each(function () {
            if ($(this).hasClass('disabled') || $(this).val().indexOf('sitekick_') >= 0) {
                $(this).before('<div class="message _added-editor-message">' + SITEKICK_LOAD_MESSAGE + '</div>');
                $(this).wrap('<div class="disabled" />');
            }
        });
    }

    var toolbar = 'styleselect | bullist numlist | link image table | media fullpage code';
    if( $(window).width() < 750 ){
        toolbar = 'styleselect | bold | bullist numlist | link image table | media'
    }

    if( redactorForms === true ){
        toolbar += ' | sitekickforms';
    }

    // run default editor
    tinymce.init({
        selector: '.redactor',
        menubar: false,
        language: sitekickLanguage,
        height : 300,
        content_css : '/css/sitekick/tinymce-content.css',
        toolbar: toolbar,
        plugins: [
            'autolink sitekicklink image imagetools lists charmap preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality template paste sitekickforms'
        ],
        contextmenu: "undo redo cut copy paste | sitekicklink image inserttable | cell row column deletetable",
        link_class_list: [
            {title: 'None', value: ''},
            {title: 'Button', value: 'button ' + redactorButtonClass}
        ],
        style_formats_merge: true,
        style_formats: editorStyles,
        media_alt_source: false,
        media_poster: false,
        convert_urls: false,
        relative_urls: false,
        remove_script_host: false,
        link_title: false,
        link_list: "/sitemap_index/editor.json",
        file_picker_types: 'file image',
        file_picker_callback: filePickerCallback,
        setup: function(editor) {
            editor.on('blur CloseWindow', function(e) {
                var t = editor.getContent();

                // remove weird space character
                t = t.replace(/&#8232;/g, '')
                    .replace('/\\u2028/g', '')
                    .replace('/  /g', '')
                ;
                editor.setContent(t);

            });
        }
    });


    // run inline editor
    tinymce.init({
        selector: '.redactor-inline:not(.redactor-title)',
        menubar: false,
        inline: true,
        language: sitekickLanguage,
        height : 300,
        content_css : '',
        toolbar: toolbar,
        plugins: [
            'autolink sitekicklink image imagetools lists charmap preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality template paste'
        ],
        contextmenu: "undo redo cut copy paste | sitekicklink image inserttable | cell row column deletetable",
        link_class_list: [
            {title: 'None', value: ''},
            {title: 'Button', value: 'button ' + redactorButtonClass}
        ],
        style_formats_merge: true,
        style_formats: editorStyles,
        media_alt_source: false,
        media_poster: false,
        relative_urls: false,
        convert_urls: false,
        remove_script_host: false,
        link_title: false,
        link_list: "/sitemap_index/editor.json",
        file_picker_types: 'file image',
        file_picker_callback: filePickerCallback,
        setup: function(editor) {
            editor.on('blur CloseWindow', function(e) {
                var t = editor.getContent();

                // remove weird space character
                t = t.replace(/&#8232;/g, '')
                    .replace('/\\u2028/g', '')
                    .replace('/  /g', '')
                ;
                editor.setContent(t);

            });
        }
    });


    // run title editors
    tinymce.init({
        selector: '.redactor-title',
        menubar: false,
        inline: true,
        language: sitekickLanguage,
        height : 300,
        content_css : '',
        toolbar: false,
        forced_root_block: "",
        plugins: [
            'autolink sitekicklink image imagetools lists charmap preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality template paste'
        ],
        contextmenu: "undo redo cut copy paste",
        link_class_list: [
            {title: 'None', value: ''},
            {title: 'Button', value: 'button ' + redactorButtonClass}
        ],
        convert_urls: false,
        relative_urls: false,
        remove_script_host: false,
        link_title: false,
        link_list: "/sitemap_index/editor.json",
        file_picker_types: 'file image',
        file_picker_callback: filePickerCallback
    });



}