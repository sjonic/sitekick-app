/**
 * TinyMCE Sitekick Link plugin
 * Own link plugin built for inserting internal-links
 * @author sjonic.nl - Gijs
 * @copyright sjonic.nl
 */
tinymce.PluginManager.add('sitekicklink', function(editor, url) {
    editor.addButton('link', {
        icon: 'link',
        tooltip:"Insert/edit link",
        onclick: function() {
            var elementId;

            if (tinymce.activeEditor.selection.getNode().tagName == 'A' && tinymce.activeEditor.selection.getNode().id) {
                elementId = tinymce.activeEditor.selection.getNode().id;
            }
            openSitekickLinkEditor(editor,elementId);
        }
    });

    editor.addMenuItem('sitekicklink', {
        text: 'Link',
        icon: 'link',
        context: 'tools',
        onclick: function() {
            var elementId;

            if (tinymce.activeEditor.selection.getNode().tagName == 'A' && tinymce.activeEditor.selection.getNode().id) {
                elementId = tinymce.activeEditor.selection.getNode().id;
            }
            openSitekickLinkEditor(editor,elementId);
        }
    });

    return {
        getMetadata: function () {
            return  {
                name: "Sitekick Link plugin",
                url: "https://sjonic.nl"
            };
        }
    };
});

var openSitekickLinkEditor = function(editor,elementId){
    // Open window
    editor.windowManager.open({
        title: 'Insert link',
        url: sitekickUrl + 'editor/link',
        width: 500,
        height: 310,
        buttons: [
            {
                text: 'Insert',
                onclick: function(e){
                    // store ID of active tiny mce editor, since this is required when getting & setting the cursor position
                    var tinyMceActiveEditorId = $(tinymce.activeEditor.getBody()).attr('data-id');

                    var frame = $(e.currentTarget).find("iframe").get(0);
                    var content = frame.contentDocument;

                    var url = $(content).find('#link-form #url').val();
                    var title = $(content).find('#link-form #text').val()

                    // if <a> element doesn't have an id yet, generate one
                    if(!elementId) {
                        tempElementId = uuid();
                    } else {
                        tempElementId = elementId;
                    }

                    // create full string to add to tinymce editor
                    var string = '';
                    if( url == '' ){
                        string = title;
                    } else {
                        string = '<a id="'+tempElementId+'" href="'+url+'" ';
                        if( $(content).find('#target').val() != '' ){
                            string += ' target="'+$(content).find('#target').val() + '" ';
                        }
                        if( $(content).find('#class').val() != '' ){
                            string += ' class="'+$(content).find('#class').val() + '" ';
                        }
                        if( title == '' ){
                            title = url;
                        }
                        string += '>' + title + '</a>';
                    }

                    if (string !== '') {
                        /**
                         * 3 options:
                         * a) an existing link is selected: replace existing with new link
                         * b) nothing is selected (in case of a link to a file), but an elementId is present: replace this element with new link
                         * c) add link where the cursor is located
                         */
                        if( tinymce.get(tinyMceActiveEditorId).selection.getNode().tagName == 'A' ){
                            var node = tinymce.get(tinyMceActiveEditorId).selection.getNode();
                            tinymce.get(tinyMceActiveEditorId).selection.select(node);
                            tinymce.get(tinyMceActiveEditorId).selection.setContent(string);
                        } else if (elementId) {
                            var element = tinymce.get(tinyMceActiveEditorId).dom.get(elementId);
                            tinymce.get(tinyMceActiveEditorId).selection.select(element,false);
                            tinymce.get(tinyMceActiveEditorId).selection.setContent(string);
                        } else {
                            tinymce.get(tinyMceActiveEditorId).insertContent(string);
                        }
                    }
                    // the current cursor position is stored, since closing the window forces the cursor to jump to the start of the body
                    var bookmark = tinymce.get(tinyMceActiveEditorId).selection.getBookmark(2,true);
                    top.tinymce.get(tinyMceActiveEditorId).windowManager.close();
                    // then set the cursor at the stored bookmark
                    tinymce.get(tinyMceActiveEditorId).selection.moveToBookmark(bookmark);


                },
                classes: 'btn btn-primary widget abs-layout-item primary first btn-has-text'
            },
            {
                text: 'Close',
                onclick: 'close'
            },
        ]
    }, {
        node: editor.selection.getNode(),
        selectedText: editor.selection.getContent()
    });
}

function uuid()
{
    var seed = Date.now();
    if (window.performance && typeof window.performance.now === "function") {
        seed += performance.now();
    }

    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (seed + Math.random() * 16) % 16 | 0;
        seed = Math.floor(seed/16);

        return (c === 'x' ? r : r & (0x3|0x8)).toString(16);
    });

    return uuid;
}
