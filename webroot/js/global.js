var modal;
var closeButton;
var urlbackwards;

$(document).ready(function() {

    //clear empty submenu's
    $(".dd-list").each(function( index ) {
        if($(this).has("li").length == 0){
            $(this).remove();
        }
    });

	$('.menu-trigger').on("click", function() {
		$("header").toggleClass('open').addClass('fixed');
	});
	
	$('.sorting').on("click", function() {
		$(this).toggleClass('open');
		$(".horizontal").toggle();
	});
	

	if(!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$('.background').scrolly({bgParallax: true});
		$('.parallax').scrolly({bgParallax: true});
	}
	

	$(window).scroll(function() {
		if(!$('header').hasClass("absolute-fixed")) {
			if($(window).scrollTop() > 450) {
				$('header').removeClass('absolute').addClass('fixed');
			} else {
				if($('header').hasClass('fixed') && $(window).scrollTop() <= 35) {
					$('header').removeClass('fixed').addClass('absolute');
				}
			}
		}
	});
	
	
	// Navigation
	$('.navigation a').on("click", function(event) {
		$("header").toggleClass('open');
		
		// Internal page navigation & Horizontal element navigation
		if($(this).data('section') && $(this).data('horizontal-content') && $('.' + $(this).data('section')).length) {
			$('.navigation a').removeClass('active');
			$(this).addClass('active');
			navigate_to($(this).data('section'), $(this).data('horizontal-content'), event);
		
		// Internale page navigation
		} else if($(this).data('section') && $('.' + $(this).data('section')).length) {
			$('.navigation a').removeClass('active');
			$(this).addClass('active');
			event.preventDefault(event);
			navigate_to($(this).data('section'), '', event);
			
		// External page navigation
		} else if($(this).data('section')) {
			event.preventDefault(event);
			window.location = $(this).attr('href') + '#' + $(this).data('section');
		}
	});
	if(window.location.hash) {
		navigate_to(window.location.hash.substring(1), '');
	}	
	$(document).on("scroll", activateNav);
	
	
	// Quotes slider
	$(".quotesSlider").swipe({
		swipeLeft: function(event, direction, distance, duration, fingerCount, fingerData) {
			var current = $(".quotesSlider .quotes").find(".active");
			var currentBullet = $(".quotesSlider .bullets").find(".active");
			$('.quotesSlider .quotes li').removeClass("fadeOut");
			
			if(current.prev().is("li")) {
				current.addClass("fadeOut").removeClass("active").prev().addClass("active");
				currentBullet.removeClass("active").prev().addClass("active");
			} else {
				current.addClass("fadeOut").removeClass("active").parent().find("li").last().addClass("active");
				currentBullet.removeClass("active").parent().find("span").last().addClass("active");
			}
		},
		swipeRight: function(event, direction, distance, duration, fingerCount, fingerData) {
			var current = $(".quotesSlider .quotes").find(".active");
			var currentBullet = $(".quotesSlider .bullets").find(".active");
			$('.quotesSlider .quotes li').removeClass("fadeOut");
			
			if(current.next().is("li")) {
				current.addClass("fadeOut").removeClass("active").next().addClass("active");
				currentBullet.removeClass("active").next().addClass("active");
			} else {
				current.addClass("fadeOut").removeClass("active").parent().find("li").first().addClass("active");
				currentBullet.removeClass("active").parent().find("span").first().addClass("active");
			}
		}
	});
	$(".quotesSlider .controls span").click(function() {
		var current = $(".quotesSlider .quotes").find(".active");
		var currentBullet = $(".quotesSlider .bullets").find(".active");
		$('.quotesSlider .quotes li').removeClass("fadeOut");

		if($(this).hasClass("prev")) {
			if(current.prev().is("li")) {
				current.addClass("fadeOut").removeClass("active").prev().addClass("active");
				currentBullet.removeClass("active").prev().addClass("active");
			} else {
				current.addClass("fadeOut").removeClass("active").parent().find("li").last().addClass("active");
				currentBullet.removeClass("active").parent().find("span").last().addClass("active");
			}
		} else if($(this).hasClass("next")) {
			if(current.next().is("li")) {
				current.addClass("fadeOut").removeClass("active").next().addClass("active");
				currentBullet.removeClass("active").next().addClass("active");
			} else {
				current.addClass("fadeOut").removeClass("active").parent().find("li").first().addClass("active");
				currentBullet.removeClass("active").parent().find("span").first().addClass("active");
			}
		}
	});
	
	
	// Horizontal content slider
	$(".horizontal-container").each(function(index, element) {
		var navigation = $(element).children(".horizontal");
		var content = $(element).children(".horizontal-content");

		$(navigation).children().first().addClass("active");
		$(content).children().first().addClass("active");
		
		$(navigation).children().click(function(event) {
			navigate_to("", $(this).data("horizontal-content"), event);
		});
	});
});


function navigate_to(element, horizontalElement, event) {	
	// Navigate to offset
	if(element) {
		if(event) {
			event.preventDefault(event);
		}
		
		$('html,body').animate({
			scrollTop: $('.' + element).offset().top-100
		}, 400);
	}
	
	// Navigate to horizontal element
	if(horizontalElement) {
		$('.horizontal-container .horizontal').children().removeClass("active");
		$('.horizontal-container .horizontal').find("[data-horizontal-content='" + horizontalElement + "']").addClass("active");
		
		$('.horizontal-container .horizontal-content').children().removeClass("active");
		$('.horizontal-container .horizontal-content').find("[data-horizontal-content='" + horizontalElement + "']").addClass("active");
	} 
}


function loader(state) {
	//if(state && state == "start") {
	//	$('body').append('<div id="loader"><div id="spinner"><div class="bounce1"></div><div class="bounce2"></div>	<div class="bounce3"></div></div></div>');
	//} else if(state && state == "done") {
	//	$('#loader').remove();
	//}
}


function closeDialog() {
	if(history.pushState) {
		history.pushState(null, null, urlbackwards);
	}
	
	modal.fadeOut(250, function() {
		$('body').removeClass('overflow-hidden');
		this.remove();
	});
}


function loadDialog( post, url, type ) {
	urlbackwards = window.location.pathname;
		
	if (!history.pushState) {
		document.location.href = url;
	} else {
		history.pushState(null, null, url);
	}
	
	$('body').addClass('overflow-hidden');
	modal = $('<div class="modal" />');
	closeButton = $('<span class="close icon-cross font-medium" />');

	$(post).find('.article').parent().each(function(){
		modal.append($(this).html());
	});

	modal.find('.head .container').append(closeButton);
	
	$('body').append(modal);
	$(window).trigger('resize');
	
	closeButton.on('click', closeDialog);
}


function loadPost(e) {
	var viewportWidth = $(window).innerWidth;
	if(viewportWidth < 767) return;
	e.preventDefault(e);

	loader("start");

	var url = $(this).attr('href'),
		elurl = url.split('/'),
		slug = elurl.pop(),
		type = $(this).data('type');

	$.ajax({
		url: url
	}).success(function(data){
		loadDialog(data, url, type);

		$('body').removeClass('loading');
		$(window).trigger('resize');

		loader("done");
	});
}


function activateNav(event) {
	var scrollPos = $(document).scrollTop();

	$('.navigation a').each(function() {
		if($(this).data('section')) {
			var linkElement = $(this).data('section');
			
			if($('.' + linkElement).offset()) {
				var elementOffset = $('.' + linkElement).offset().top - 110;
				var elementHeight = $('.' + linkElement).height() - 110;

				if((elementOffset) <= scrollPos && (elementOffset + elementHeight) > scrollPos) {
					$(this).addClass('active');
				} else {
					$(this).removeClass('active');
				}
			}
		}
	});
}

