$(document).ready(function() {

	var validEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	$(".jsValidator").attr('novalidate', 'novalidate');
	$(".jsValidator button[type=submit]").click(function() {

        $('.errorField').removeClass("errorField");

        $(".jsValidator [rel=required]").each(function() {
        	if( $(this).hasClass('redactor') ){
        		var val = tinyMCE.get($(this).attr('id')).getContent();
        		if( val.length == 0 ){
                    $(this).closest('.input-group').addClass("errorField");
				}
			}
            else if( $(this).val().length == 0 ) {
				$(this).addClass("errorField");
				if($(this).hasClass("redactor")) {
					$(this).prev(".redactor-editor").addClass("errorField");
				}
				if($(this).hasClass("sk-multiple-select-input")) {
					$(this).parents(".sk-multiple-select").addClass("errorField");
				}
			}
			else if( $(this).hasClass("email") && !$(this).val().match(validEmail) ) {
				$(this).addClass("errorField");
			}
			else if( $(this).prop("data-min-chars") && $(this).val().length < $(this).data("min-chars") ) {
				$(this).addClass("errorField");	
			}
			else if( $(this).prop("data-max-chars") && $(this).val().length > $(this).data("max-chars") ) {
				$(this).addClass("errorField");	
			}			
            else {
                $(this).removeClass("errorField");   
				// if($(this).hasClass("redactor")) {
				// 	$(this).prev(".redactor-editor").removeClass("errorField");
				// }
				if($(this).hasClass("sk-multiple-select-input")) {
					$(this).parents(".sk-multiple-select").removeClass("errorField");
				}
            }    
        });    
        
        if( $(".jsValidator .errorField").length > 0 ) {
			
			if( $(".jsValidator .errorField:first").hasClass("redactor") ) {
			
				$("html,body").animate({
					scrollTop: $(".jsValidator .errorField:first").parent("div").offset().top-180
				}, 500);   
			
			}
			else {
				$("html,body").animate({
					scrollTop: $(".jsValidator .errorField:first").offset().top-180
				}, 500);    
			}	
            return false;
        }
		
	});
});