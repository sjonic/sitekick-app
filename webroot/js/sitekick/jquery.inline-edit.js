/*jslint browser: true*/
/*global $, jQuery, alert, confirm, sweetAlert*/
(function ($) {
    'use strict';

    // InlineEdit controlebar
    $('ul.sitekick_inlinecontrols li a').on('click tap', function () {
        var elementAction = $(this).parent().data('action'),
            elementRedirect;

        if (elementAction === "edit") {
            return;
        }

        if (elementAction === "deleteRedirect") {
            if ($(this).parent().data('redirect')) {
                elementRedirect = $(this).parent().data('redirect');
            } else {
                return false;
            }
        }

        if (elementAction === "delete" || elementAction === "deleteRedirect") {
            if (confirm(translation_core_1)) {
                $.ajax({
                    url: $(this).attr('href'),
                    success: $.proxy(function () {
                        $(this).parent().parent().parent().slideUp();

                        if (elementAction === "deleteRedirect") {
                            window.location.href = elementRedirect;
                        }

                        return false;
                    }, this)
                });
            }
        }

        return false;
    });


    // InlineEdit function
    $.fn.inlineEdit = function () {

        var contentCache = [];

        this.each(function () {

            // check if not already initialized
            if( $(this).hasClass('_inline-edit-initialized') ){
                return false;
            }

            // disable for mobile
            if ($(window).width() < 768) {
                return false;
            }

            var id = Math.floor((Math.random() * 100000) + 1);
            $(this).addClass('_inline-edit-initialized').attr('data-inline-edit-id', id);

            // set default settings
            contentCache[id]  = $(this).html();

            $(this).on('click', runEditor);

        });

        // run editor
        function runEditor(){

            var $editor = $(this);

            // check if editor is already running
            if( $(this).hasClass('editor-active') ){
                return false;
            }

            $editor.addClass('editor-active');


            var id          = $editor.data('id'),
                plugin      = ((typeof $editor.data('plugin') !== 'undefined') ? $editor.data('plugin').toLowerCase() : 'pages'),
                field       = ((typeof $editor.data('field') !== 'undefined') ? $editor.data('field') : ''),
                path        = sitekickUrl + plugin + "/inline_edit",
                editorType  = $editor.data('editor');

            // check if editor is allowed (maybe someone is already editing)
            var returnCheck = $.ajax({
                url: sitekickUrl + plugin + '/editcheck/' + id+ '/' + field,
                type: 'GET',
                async: false,
                cache: false
            });
            var check = $.parseJSON(returnCheck.responseText);
            if ( typeof check['allowed'] === 'undefined' || parseInt(check['allowed']) === 0) {
                sweetAlert({
                    title: "",
                    text: "Op dit moment is <strong>" + check['editing_user']['firstname'] + " " + check['editing_user']['lastname'] + "</strong> deze pagina aan het bewerken. Om er voor te zorgen dat jullie elkaars werk niet overschrijven, kan je deze pagina op dit moment niet bewerken. Wacht tot <strong>" + check['editing_user']['firstname'] + " " + check['editing_user']['lastname'] + "</strong> klaar is of tot de pagina automatisch wordt vrijgegeven over <strong>" + check['editing_togo'] + "</strong>",
                    type: "info",
                    confirmButtonColor: "#2eb582",
                    confirmButtonText: translation_core_29,
                    closeOnConfirm: false
                });
                return false;
            }

            // get correct inline edit field
            $(this).load(path + "/" + id + "/" + field + "/" + editorType, function () {

                // set correct class for correct editor
                switch( editorType ){
                    case "regular":
                        var newHTML = '<div class="redactor-inline">'+$editor.find('.redactor').val()+'</div>';
                        $editor.find('.redactor').after(newHTML).removeClass('redactor').hide();
                        break;
                    case "title":
                        $editor.find('.redactor').removeClass('redactor').addClass('redactor-title');
                        $editor.find('.redactor-inline').removeClass('redactor-inline').addClass('redactor-title');
                        break;
                }

                // load editor
                initEditor();

                // bind save / cancel buttons
                $editor.find('.inline-cancel').click(cancelEditor);
                $editor.find('.inline-save').click(saveEditor);

            });

        }

        // cancel editor
        function cancelEditor(){
            var $editor = $(this).closest('.inline-edit');
            var editorID = $editor.data('inline-edit-id');

            // check if we need to alert
            var content = $editor.find('.redactor').val();
            if( $editor.find('.redactor-inline').length ){
                content = $editor.find('.redactor-inline').html();
                $editor.find('[name="content"]').val(content);
            }
            if( content !== contentCache[$editor.data('inline-edit-id')] ){
                sweetAlert({
                    title: "",
                    text: translation_core_14,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#2eb582",
                    confirmButtonText: translation_core_9,
                    cancelButtonText: translation_core_10,
                    closeOnConfirm: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        $editor.removeClass('editor-active');
                        $editor.html(contentCache[editorID]);
                    }
                });
            } else {
                $editor.removeClass('editor-active');
                $editor.html(contentCache[editorID]);
            }

            return false;
        }

        // save editor
        function saveEditor(){
            var $editor = $(this).closest('.inline-edit');
            $editor.removeClass('editor-active');

            // get/set content in input
            var content = $editor.find('.redactor').val();
            if( $editor.find('.redactor-inline, .redactor-title').length ){
                content = $editor.find('.redactor-inline, .redactor-title').html();
                if( $editor.find('#field-'+$editor.data('field')).length ){
                    $editor.find('#field-' + $editor.data('field')).val(content);
                }
                if( $editor.find('#' + $editor.data('field')).length ){
                    $editor.find('#' + $editor.data('field')).val(content);
                }
            }

            $editor.find('.inline-cancel, .inline-save').hide();
            $editor.find('.inline-edit-controls').append('<li><div class="ajax-loading load"></div></li>');

            // saving new content
            var data = $(this).closest("form").serialize(),
                url = $(this).closest("form").attr("action");
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (msg) {
                    $editor.html(msg);
                    if( $editor.text() === '' ){
                        $editor.html(translation_core_6);
                    }

                    sweetAlert({
                        title: "",
                        text: translation_core_7,
                        type: "success",
                        timer: 1500,
                        confirmButtonColor: "#2eb582"
                    });
                }
            });

            return false;
        }
    };
}(jQuery));


$(document).ready(function () {
    $(".inline-edit").inlineEdit();
});