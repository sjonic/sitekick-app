$(document).ready(function () {

    $(".settingbutton").click(function () {
        $('.settingbutton.btn-active').removeClass('btn-active');
        $(this).addClass('btn-active');
        $('#search-input').val( $(this).text() );
        $('#search-input').trigger('keyup');
    });

    $('#search-input').on('keyup', function () {
        var value = $(this).val().toLowerCase();
        $('#dataTable tbody tr').hide();
        $('#dataTable tbody tr').each(function () {
            var $tr = $(this);
            if ($tr.text().toLowerCase().indexOf(value) >= 0) {
                $tr.show();
            }
        });
    });
});