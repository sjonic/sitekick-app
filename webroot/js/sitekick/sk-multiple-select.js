(function( $ ) {    
    
    $.fn.skms = function(options) {

        var settings = $.extend({
			name: false,			
			placeholder: false,
			empty: false,
			options: [],
			selected: [],
			required: false,
			multiple: true,
			callback: function() {}
        }, options );

        this.each(function() {
			$(this).append("<div class=\"selected-options\"></div>");
			$(this).append("<input type=\"text\" placeholder=\"" + settings.placeholder + "\" />");	
			var list = "";
			for (var key in settings.options) {
				list += "<li onclick=\"\" data-value=\"" + settings.options[key]["id"] + "\">" + settings.options[key]["name"] + "</li>";
			}	
			$(this).append("<ul>" + list + "</ul>");
			if( settings.required == true ) {
				$(this).append("<textarea class=\"sk-multiple-select-input\" name=\"" + settings.name + "\" rel=\"required\" required=\"required\"></textarea>");
			}
			else {
				$(this).append("<textarea class=\"sk-multiple-select-input\" name=\"" + settings.name + "\"></textarea>");
			}
			$(this).append("<a onclick=\"\" class=\"sk-arrow\"></a>");
			setSelectedOptions($(this), settings.options, settings.selected);


			$(this).find(".sk-arrow").on("click", function(event) {
				if( $(this).hasClass("expand") ) {
					$(this).removeClass("expand");
					$(this).parents(".sk-multiple-select").children("ul").fadeOut(300);
				}
				else {
					$(this).addClass("expand");
					$(this).parents(".sk-multiple-select").children("ul").css("top", $(this).parent(".sk-multiple-select").outerHeight() ).fadeIn(300);
				}

				return false;

			});

			$(this).find("input").on("focus", function() {
				$(this).next("ul").css("top", $(this).parent(".sk-multiple-select").outerHeight() ).fadeIn(300);
				$(this).parent(".sk-multiple-select").children(".sk-arrow").addClass("expand");
			});

			$(this).find("input").on("keyup", function() {
				var keyword = $(this).val().toLowerCase();
				var noResults = translation_core_15.replace("{KEYWORD}", keyword);

				$(this).next("ul").children("li").each(function() {
					var name = $(this).text().toLowerCase();
					if ( name.indexOf(keyword) < 0 ) {
						$(this).addClass("no-match");
					}
					else {
						$(this).removeClass("no-match");
					}
				});
				if( $(this).parent(".sk-multiple-select").children("ul").children("li:visible").length == 0 ) {
					$(this).parent(".sk-multiple-select").children("ul").append("<li class=\"no-results\">" + noResults + "</li>")
				}
				else {
					$(this).parent(".sk-multiple-select").children("ul").children(".no-results").remove();
				}
			});

			$(this).find("ul li:not(.empty)").on("click", function() {

				if ( settings.multiple == false ) {
					$(this).parents(".sk-multiple-select").children(".selected-options").html("");
					$(this).parents(".sk-multiple-select").children("ul").children("li").removeClass("disable");
				}

				$(this).parents(".sk-multiple-select").children(".selected-options").append("<div data-id=\"" + $(this).data("value") + "\" data-name=\"" + $(this).text() + "\">" + $(this).text() + " <i></i></div>");
				$(this).addClass("disable");
				$(this).parents(".sk-multiple-select").children("input[type=text]").val("");

				if( $(this).parents(".sk-multiple-select").children("ul").children("li").not(".disable").length == 0 ) {
					$(this).parents(".sk-multiple-select").children("ul").append("<li class=\"empty\">" + settings.empty + "</li>");
				}
				$(this).parent("ul").css("top", $(this).parents(".sk-multiple-select").outerHeight());

				generateData($(this).parents(".sk-multiple-select"));
			});

			$(this).on("click", ".selected-options div i", function() {
				var $this = $(this).parents(".sk-multiple-select");
				var id = $(this).parent("div").data("id");
				$(this).parent("div").fadeOut(100, function() {
					$(this).remove();
					$this.children("ul").css("top", $this.outerHeight());
					generateData($this);
				});
				$this.children("ul").children("li[data-value='" + id + "']").removeClass("disable");
				$this.children("ul").children("li.empty").remove();
			});

		});	

		$("html").on("click touchstart", "body", function(e) {
			if( !$(e.target).parents().hasClass("sk-multiple-select") ) {
				$(".sk-multiple-select ul").fadeOut(300);
				$(".sk-arrow").removeClass("expand");
			}
		});

		function setSelectedOptions($this, options, selected) {
			for (var key in selected) {

				for (var k in options) {

					if( selected[key]["id"] == options[k]["id"] || selected[key]["name"] == options[k]["name"] ) {
						$this.children(".selected-options").append("<div data-id=\"" + options[k]["id"] + "\" data-name=\"" + options[k]["name"] + "\">" + options[k]["name"] + " <i></i></div>");
						$this.children("ul").children("li[data-value='" + options[k]["id"] + "']").addClass("disable");
					}
				}
			}

			if( $this.children("ul").children("li").not(".disable").length == 0 ) {
				$this.children("ul").append("<li class=\"empty\">" + settings.empty + "</li>");
			}

			generateData($this);
		}

		function generateData($this) {

			var data = [];					
			var items = $this.children(".selected-options").children("div").length;

			for (i = 0; i < items; i++) {
				var id = $this.children(".selected-options").children("div").eq(i).data("id");
				var name = $this.children(".selected-options").children("div").eq(i).data("name");
				data.push({id: id, name: name});
			}

			var json = JSON.stringify(data);
			if( json == "[]" ) {
				$this.children("textarea").val("");
			}
			else {
				$this.children("textarea").val(json);
			}

			settings.callback.call($this, data);
		}	
    };
    
}( jQuery ));