
$( document ).ready(function() {

    //always go to firs error
    if ($('.has-error').length) {
        $('html, body').animate({scrollTop: $(".has-error").first().offset().top - 180}, 800);
    }

    //menu counters
    $( ".menu_counter" ).each(function( index ) {
        var plugin = this.id;
        plugin = plugin.replace("counter_","");

        $.get(sitekickUrl + plugin+"/counter/",{}).done(function( data ) {

            $("#counter_"+plugin).html(data);
            if(data == 0){
                $("#counter_"+plugin).hide();
            }else{
                $("#counter_"+plugin).show();
            }

        });

    });


    //tooltip
    $( "i.icon-info" ).hover(
        function() {
            if($.hasData(this)) {
                $(this).popover({container: "body"}).show().click();
            }
        }, function() {
            if($.hasData(this)) {
                $(this).popover('hide');
            }
        }
    );

    //$( window ).resize(function() {
    //    if ($(window).width() <= 640 && $(".sidebar").css('left') != "-640px") {
    //        $(".navbar-toggle").click();
    //    }
    //});

    $(".navbar-toggle").click(function () {
		
        if($("#help_background").is(':visible')){
            $(".help").click();
            return false;
        }


            if ($(".main").hasClass("fullscreen")) {

                    $(".sidebar").animate({
                        left: parseInt($(".sidebar").css('left'), 10) == 0 ? -640 : 0
                    }, {duration: 500, queue: false});


                    if ($(window).width() >= 640) {

                        $(".col-md-10").animate({
                            width: '83.3333%'
                        }, {duration: 500, queue: false});

                        $(".main").animate({
                                margin: '0 20 0 ' + ($(".sidebar").width() + 40) + 'px'
                            }, {duration: 500, queue: false}
                        );
                    }

                    $(".main").promise().done(function () {
                        cleanStyles();
						$(window).trigger('resize');
                    });


                    $(".main").removeClass('fullscreen');
                    eraseCookie("nosidebar");

            } else {

                    $(".sidebar").animate({
                        left: parseInt($(".sidebar").css('left'), 10) == 0 ? -640 : 0
                    }, {duration: 500, queue: false});


                    $(".main.col-md-10").animate({
                        width: '100%'
                    }, {duration: 500, queue: false});

                    $(".main.col-md-offset-2").animate({
                        margin: '0'
                    }, {duration: 500, queue: false});
				
				
					$(".main").promise().done(function () {
						$(window).trigger('resize');
                    });


                    $(".main").addClass('fullscreen').removeAttr("style");
                    createCookie("nosidebar", 1);
        }

    });


    /**
     * Show help overlay
     */
    $(".help").click(function(){

        if($("#help_background").is(':visible')){

            $("#help_background").css('height',$( window ).height()-70).slideUp();
            $("#help").slideUp();
        }else{
            $("#help_background").css('height',$( window ).height()-70).slideDown();
            $("#help").slideDown();
            $('html, body').animate({ scrollTop: 0 }, 0);
        }

        return false;
    });


});

window.onload = function(){
    // sticky save button
    if( $('.finish').length ){

        var posSaveButton = $('.finish').offset().top - $(window).height();

        // create sticky version
        var parent = $('.finish').closest('div');
        parent.after('<div class="sitekick-sticky-finish"></div>');
        $('.sitekick-sticky-finish').append($('.finish').clone());

        $(document).on('scroll', function(){
            var t = $(document).scrollTop();
            if( t > 150 && t <= posSaveButton  ){
                $('.sitekick-sticky-finish').addClass('_visible');
            } else {
                $('.sitekick-sticky-finish').removeClass('_visible');
            }
        });
    }
};

//check for session if gone set ajaxlogin to work
function checkSession(){

    $.get(websiteUrl + "api/session/",{}).done(function( data ) {
       if(data == 0){
           $(".ajaxlogin_background").show();
           $(".ajaxlogin").show();
           $('html, body').animate({ scrollTop: 0 }, 0);
       }
    });
    return false;
}


function cleanStyles(){
    $( ".main").removeAttr("style");
}


//Maak een cookie met javascript
function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

//Lees een cookie met javascript
function readCookie(name) {
    var nameEQ = escape(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
    }
    return null;
}

//Verwijder een cookie met javascript
function eraseCookie(name) {
    createCookie(name, "", -1);
}

//Open een link (enkel voor mobiel)
function loadMobilePage(url){
    if ($(window).width() <= 640){
        location.href = sitekickUrl + url;
    }

    return false;
}

//check sessions for ajax login
setInterval("checkSession()", 30000);

/**
 * Start loading global on a page
 */
var globalPageLoading = 0;
function startPageLoading(){
    globalPageLoading++;
    $('body').addClass('loading');
    $('.finish').prop('disabled', true);
}

/**
 * Stop loading process
 */
function stopPageLoading(){
    globalPageLoading--;

    if( globalPageLoading <= 0 ){
        $('body').removeClass('loading');
        $('.finish').prop('disabled', false);
    }
}