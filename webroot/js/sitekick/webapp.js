(function(document,navigator,standalone) {
    // prevents links from apps from oppening in mobile safari
    // this javascript must be the first script in your <head>
    if ((standalone in navigator) && navigator[standalone]) {
        var curnode, location=document.location, stop=/^(a|html)$/i;
        document.addEventListener('click', function(e) {
            curnode=e.target;
            while (!(stop).test(curnode.nodeName)) {
                curnode=curnode.parentNode;
            }
            // Condidions to do this only on links to your own app
            // if you want all links, use if('href' in curnode) instead.
            if('href' in curnode && ( curnode.href.indexOf('http') || ~curnode.href.indexOf(location.host) ) ) {
                e.preventDefault();
                location.href = curnode.href;
            }
        },false);

        //add extra 20 pixels for status bar
        //document.write('<style>.container-fluid{margin-top:30px;}#container_dashboard{margin-top:120px;}.sidebar{top:100px;} #progress-bar{top:100px !important;} .alert{top:100px!important;;} </style>');
    }
})(document,window.navigator,'standalone');