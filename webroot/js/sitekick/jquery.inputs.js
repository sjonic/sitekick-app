(function( $ ) {    
    
    $.fn.switchButton = function(options) {

        var settings = $.extend({
            label: true,
            style: false,
			appendTo: $(this).closest("form")
        }, options );

        this.each(function() {


            var label = $(this).next("label").html();
            var name = $(this).attr("name");
            var id = $(this).attr("id");
            var rel = $(this).attr("data-class");

            if($(this).is(":checked")) {
                var value = 1;
            }
            else {
                var value = 0;
            }

            if(value == 1) {
                if(settings.label == true && label) {
                    $(this).parent("div").html("<label>" + label + "</label><div class=\"slider-frame " + settings.style + " "+ rel +" \"><span class=\"slider-button\" data-input=\"" + id + "\" data-value=\""+ value +"\">" + translation_core_9 + "</span></div>");
                }
                else {
                    $(this).parent("div").html("<div class=\"slider-frame " + settings.style + " "+ rel +"\"><span class=\"slider-button\" data-input=\"" + id + "\" data-value=\""+ value +"\">" + translation_core_9 + "</span></div>");
                }
            }
            else {
                if(settings.label == true && label) {
                    $(this).parent("div").html("<label>" + label + "</label><div class=\"slider-frame " + settings.style + " "+ rel +"\"><span class=\"slider-button off\" data-input=\"" + id + "\" data-value=\""+ value +"\">" + translation_core_10 + "</span></div>");
                }
                else {
                    $(this).parent("div").html("<div class=\"slider-frame " + settings.style + " "+ rel +"\"><span class=\"slider-button off\" data-input=\"" + id + "\" data-value=\""+ value +"\">" + translation_core_10 + "</span></div>");
                }
            }
            settings.appendTo.append("<input type=\"hidden\" name=\"" + name + "\" id=\"" + id + "\" value=\"" + value + "\" />");
        });

        $('.slider-frame').off("click").click(function() {
            if($(this).children("span").hasClass("off")) {
                $(this).children("span").removeClass("off").text(translation_core_9);
                $('#'+$(this).children("span").attr("data-input")).val(1).prop('checked', true).trigger('change');
            }
            else {
                $(this).children("span").addClass("off").text(translation_core_10);
                $('#'+$(this).children("span").attr("data-input")).val(0).prop('checked', false).trigger('change');
            }

            if( typeof options != "undefined" && typeof options.onclick != "undefined" ){
                options.onclick($(this));
            }
        });

    };
    
    $.fn.charCount = function(options) {

        var settings = $.extend({
            maxChars: this.attr("data-maxlength"),
            seoSolutionRef: this.attr("data-seo-solution-key")
        }, options );

        count = 0;

        var strLength = $(this).val().length;
        var count = settings.maxChars - strLength;

        if (settings.seoSolutionRef) {
            this.wrap("<div id=\"" + settings.seoSolutionRef + "\" class=\"inputwrap\"></div>");
        } else {
            this.wrap("<div class=\"inputwrap\"></div>");
        }

        var counting = translation_core_13.replace("{COUNTER}", count);

		if(count >= 0) {
			this.after("<div class=\"char_count\">" + counting + "</div>");
		}
		else {
			this.after("<div class=\"char_count red\">" + counting + "</div>");
		}

        this.keyup(function() {

            var maxChars = settings.maxChars;
            var strLength = $(this).val().length;

            var counting = translation_core_13.replace("{COUNTER}", (maxChars-strLength));

			if((maxChars-strLength) >= 0) {
				$(this).next(".char_count").html(counting);
			}
			else {
				$(this).next(".char_count").html("<span class=\"red\">" + counting + "</span>");
			}
        });

        var maxChars = settings.maxChars;
        var strLength = $(this).val().length;

        var counting = translation_core_13.replace("{COUNTER}", (maxChars-strLength));

        if((maxChars-strLength) >= 0) {
            $(this).next(".char_count").html(counting);
        }
        else {
            $(this).next(".char_count").html("<span class=\"red\">" + counting + "</span>");
        }

    };
	
	/**
	 * Generate slug from string
	 * @param   {String} raw
	 * @returns {String} slugifyd
	 */
	$.fn.slugify = function(doReturn) {
		var slug = this.val();

		slug = slug.replace(/[^a-z0-9-/]/gi, '-');
		slug = slug.replace(/-+/g, '-');
		slug = slug.replace(/^-|-$/g, '-');
		slug = slug.toLowerCase();
		slug = $.trim(slug);
        if( doReturn === true ){
            return slug;
        } else {
		    this.val(slug);
        }
	};

}( jQuery ));