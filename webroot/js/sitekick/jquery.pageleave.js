$(document).ready(function() {

    var currentForm  = $(".container-fluid form");
    var initFormData = currentForm.serialize();

    //if form change check old data with new to set unload function
    $('#sitekick-navbar a[href!=\'javascript:;\'], .nav-sidebar a[href!=\'javascript:;\']').click(function(){

        var outUrl = $(this).attr('href');

        if(initFormData != currentForm.serialize() && outUrl != "#"){

            sweetAlert({
                    title: "",
                    text: translation_core_14,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#2eb582",
                    confirmButtonText: translation_core_9,
                    cancelButtonText: translation_core_10,
                    closeOnConfirm: false
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = outUrl;
                    }
                });


            return false;
        }
    });
});