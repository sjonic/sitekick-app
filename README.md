# Sitekick Core

Install with composer

### Install a new Sitekick

Follow the following steps: 

- Create a new database
- Create a new project from the default-template GIT
- Update the .env file with the correct data (copy it from .env.default)
- Replace everything within your project TEMPLATE_NAME to your projectname (camelcased)
- Run `composer update`
- Visit `project-url.locahost/install-sitekick`
- Update within the DB > Settings the values with the correct data (especially Theme + Email FROM )
- Login to Sitekick, navigate to Settings > Information and run all patches again
- Ready to go!  


### Run SASS compiler for your project
From within your PROJECT_ROOT:
```bash
npm install
npm update
gulp sass 
```

If you have a project where you already committed some css files. Update your gitignore file and remove the current css-files within the cache: 

```bash
git rm -r --cached **/*.css
git rm -r --cached **/*.css.map
```

If the server doesn't have gulp yet please install it via: 

```bash
sudo npm install --global gulp-cli
```