<?php
use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Request;

Router::defaultRouteClass('Route');
$request = new Request();
$here       = $request->getUri()->getPath();


// Disable Wordpress-bots
Router::scope('/', function($routes){
    $routes->redirect('/:random/wp-login.php', '/', ['status' => 301, 'random' => '(.*)']);
    $routes->redirect('/:random/wordpress/**', '/', ['status' => 301, 'random' => '(.*)?']);
    $routes->redirect('/wordpress/:random', '/', ['status' => 301, 'random' => '(.*)?']);
});

// Disable Joomla
if( $request->getQuery('option') && strpos($request->getQuery('option'), 'com_') !== false ){
    header( "Status: 405 Method Not Allowed" );
    header( "Location: /" );
    die(0);
}

// Install sitekick
Router::connect('/install-sitekick', ['controller' => 'Setup', 'action' => 'install']);

// Sitemap
Router::scope('/sitemap_index', function($routes) {
    $routes->extensions(['json', 'xml']);
    $routes->connect('/', ['controller' => 'App','action' => 'sitemap','plugin' => false]);
    $routes->connect('/editor', ['controller' => 'App','action' => 'sitemapeditor', 'plugin' => false]);
});

$scopes = function ($routes) {
    $routes->scope('/sitemap', function ($routes) {
        $routes->extensions(['json', 'xml', 'html']);
        $routes->connect('/', ['controller' => 'App', 'action' => 'sitemap', 'plugin' => false]);
        $routes->connect('/editor', ['controller' => 'App', 'action' => 'sitemapeditor', 'plugin' => false]);
    });
};

Router::prefix('sitekick', function($routes) {
    $routes->connect('/', ['controller' => 'Dashboard','action' => 'index']);
    $routes->connect('/settings/inline_edit/*', ['controller' => 'Settings', 'action' => 'inline_edit', 'plugin' => false]);
    $routes->connect('/pacman', ['controller' => 'Dashboard','action' => 'pacman']);
	$routes->connect('/languages/:action', ['controller' => 'Languages']);
	$routes->connect('/types/:action', ['controller' => 'Types']);

    $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
    $routes->connect('/:controller/:action', [], ['routeClass' => 'InflectedRoute']);
    $routes->connect('/:controller/:action/*', [], ['routeClass' => 'InflectedRoute']);

    $routes->connect('/:controller', ['controller' => 'Navigations','action' => 'index']);
    $routes->connect('/:controller', ['controller' => 'Users','action' => 'index']);
});


//missing images route
Router::connect('/files/*', ['controller' => 'App', 'action' => 'placeholder']);
Router::connect('/img/*', ['controller' => 'App', 'action' => 'placeholder']);
Router::connect('/js/*', ['controller' => 'App', 'action' => 'placeholder']);
Router::connect('/css/*', ['controller' => 'App', 'action' => 'placeholder']);
Router::connect('/fonts/*', ['controller' => 'App', 'action' => 'placeholder']);

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
//Plugin::routes();
Router::connect('/api/:action/*', ['controller' => 'Api']);

// set correct language for translation use in the routes
$db = ConnectionManager::get('default');
$collection = $db->schemaCollection();
$tables = $collection->listTables();
$languages = [''];
if( in_array('languages', $tables) ) {
    $languagesTable = TableRegistry::getTableLocator()->get('Languages');
    if ($languagesRecords = $languagesTable->find('all')->where(['active' => 1])) {
        foreach ($languagesRecords as $r) {
            $languages[] = $r->abbreviation;
        }
    }
}
foreach ($languages as $lang) {
    Router::scope("/$lang", ['lang' => $lang], $scopes);
}