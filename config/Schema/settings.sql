INSERT INTO `settings` (`description`, `name`, `value`, `plugin`) VALUES
('Google analytics', 'analytics', '', 'Base'),
('Naam afzender ontvanger', 'mail_from_name', '', 'Base'),
('E-mail afzender (om spam markering te verkomen moet dit een e-mail adres zijn van hetzelfde domein)', 'mail_from_email', '', 'Base'),
('E-mail ontvangers (to) (naam#email@adres.nl,naam#email@adres.nl)', 'mail_to', '', 'Base'),
('E-mail ontvangers (cc) (naam#email@adres.nl,naam#email@adres.nl)', 'mail_cc', '', 'Base'),
('E-mail ontvanger (bcc) (naam#email@adres.nl,naam#email@adres.nl)', 'mail_bcc', '', 'Base'),
('Thema', 'theme', '', 'Base'),
('Typekit', 'typekit', '', 'Base'),
('Site naam', 'site_name', '', 'Base');
