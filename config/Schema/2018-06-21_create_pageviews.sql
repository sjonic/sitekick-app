CREATE TABLE `pageviews` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ipaddress` varchar(255) NOT NULL DEFAULT '',
  `page` varchar(255) NOT NULL DEFAULT '',
  `referral` varchar(255) DEFAULT NULL,
  `server_data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `settings` (`description`, `name`, `value`, `plugin`, `explanation`) VALUES ("Pageviews IP-blacklist", "blacklist_ips", "127.0.0.1;37.153.199.82", "Base", "Gescheiden met punt-komma");