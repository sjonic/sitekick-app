UPDATE type_fields
SET options = REPLACE(options, '"scale":0.', '"scale":')
WHERE options LIKE '%"scale":0.%';

UPDATE type_fields
SET options = REPLACE(options, '"cropper_scale":0.', '"cropper_scale":')
WHERE options LIKE '%"cropper_scale":0.%';