CREATE TABLE `revisions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lang` varchar(55) NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `foreign_key` int(11) NOT NULL,
  `additional` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `postdata` longtext NOT NULL,
  `method` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `revisions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `revisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;