INSERT INTO `settings` (`description`, `name`, `value`, `plugin`) VALUES
('Gebruik Sitekick cache', 'sitekick_cache', '0', 'Base');

ALTER TABLE `navigations` ADD `cache` TINYINT(1) NOT NULL DEFAULT '1';