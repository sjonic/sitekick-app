ALTER TABLE `pageviews` CHANGE `referral` `session_id` VARCHAR(255);
CREATE INDEX `pageviews_session` ON `pageviews` (`session_id`);
ALTER TABLE `pageviews` DROP COLUMN `server_data`;