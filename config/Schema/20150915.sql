--
-- Table structure for table `fields`
--

CREATE TABLE IF NOT EXISTS `fields` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `fieldtype` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE IF NOT EXISTS `types` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_single` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `menu_slug` varchar(255) NOT NULL,
  `active` text,
  `can_highlight` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `type_fields`
--

CREATE TABLE IF NOT EXISTS `type_fields` (
`id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `fieldtype` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `options` text NOT NULL,
  `required` tinyint(1) DEFAULT '0',
  `overview` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `rank` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Indexes for table `type_fields`
--
ALTER TABLE `type_fields`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `type_fields`
--
ALTER TABLE `type_fields`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `types` ADD `use_pagebuilder` TINYINT(1) NOT NULL AFTER `menu_slug`;
ALTER TABLE `types` CHANGE `use_pagebuilder` `use_pagebuilder` TINYINT(1) NOT NULL DEFAULT '1';

ALTER TABLE `types` ADD `admin` TINYINT(1) NOT NULL DEFAULT '0' ;