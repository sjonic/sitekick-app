ALTER TABLE `types` ADD `in_sitemap` INT NOT NULL DEFAULT '1' AFTER `use_pagebuilder`;
ALTER TABLE `types` ADD `overview` VARCHAR(255) NOT NULL DEFAULT 'table' AFTER `in_sitemap`;

ALTER TABLE `files` ADD `session_id` VARCHAR(255) NULL DEFAULT NULL AFTER `user_id`;