CREATE TABLE IF NOT EXISTS `redirects` (
`id` int(10) unsigned NOT NULL,
  `from_url` varchar(255) NOT NULL,
  `to_url` varchar(255) NOT NULL,
  `type` int(3) NOT NULL DEFAULT '301'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `redirects`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

ALTER TABLE `redirects`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;