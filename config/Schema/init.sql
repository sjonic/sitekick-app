
CREATE TABLE IF NOT EXISTS `errors` (
`id` int(11) unsigned NOT NULL,
  `message` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` int(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE utf8_general_ci AUTO_INCREMENT=1 ;

ALTER TABLE `errors`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

 ALTER TABLE `errors`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
`id` int(11) NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `file` varchar(255) NOT NULL,
  `alt` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'image'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci AUTO_INCREMENT=1 ;

ALTER TABLE `files` ADD `options` TEXT NOT NULL ;

ALTER TABLE `files`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

 ALTER TABLE `files`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
`id` int(11) unsigned NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `executed` TINYINT(1) NOT NULL DEFAULT '0'

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE utf8_general_ci AUTO_INCREMENT=1 ;

ALTER TABLE `migrations`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

 ALTER TABLE `migrations`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;


-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
`id` int(11) unsigned NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `plugin` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE utf8_general_ci AUTO_INCREMENT=1 ;

ALTER TABLE `settings`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

 ALTER TABLE `settings`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;


--
-- Table structure for table `support`
--

CREATE TABLE IF NOT EXISTS `support` (
`id` int(10) unsigned NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci AUTO_INCREMENT=1 ;

ALTER TABLE `support`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `support`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `navigations`
--

CREATE TABLE IF NOT EXISTS `navigations` (
`id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `template` varchar(255),
  `display` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `has_content` tinyint(1) NOT NULL DEFAULT '1',
  `autofill` tinyint(1) NOT NULL,  
  `rank` int(11) NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `seo_description` varchar(255) NOT NULL,
  `seo_index` tinyint(1) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `temp` tinyint(1) DEFAULT '0',
  `can_delete` TINYINT(1) NULL DEFAULT '1',
  `is_contact` TINYINT(1) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE utf8_general_ci AUTO_INCREMENT=1;

ALTER TABLE `navigations`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

  ALTER TABLE `navigations`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;


--
-- Insert start-up content
--

INSERT INTO `navigations` (`id`, `parent_id`, `lft`, `rght`, `name`, `display`, `active`, `has_content`, `autofill`, `rank`, `seo_title`, `seo_description`, `seo_index`, `slug`, `plugin`, `created`, `modified`, `temp`) VALUES
(1, 0, 0, 0, 'Home', 1, 1, 1, 0, 0, '', '', 1, 'home', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

INSERT INTO `navigations` (`id`, `parent_id`, `lft`, `rght`, `name`, `display`, `active`, `has_content`, `autofill`, `rank`, `seo_title`, `seo_description`, `seo_index`, `slug`, `plugin`, `created`, `modified`, `temp`, `can_delete`, `is_contact`) VALUES
(3, 0, 1, 2, 'Contact', '1', '1', '1', '0', '0', '', '', '0', 'contact', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', NULL, '1');