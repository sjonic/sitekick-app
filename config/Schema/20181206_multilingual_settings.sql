ALTER TABLE `settings` ADD COLUMN `multilingual` TINYINT(1) DEFAULT 0 AFTER `id`;

/* update some default settings to multilingual */
UPDATE `settings` SET multilingual = 1 WHERE `name` IN (
                                                        "name",
                                                        "name_single",
                                                        "slug",
                                                        "signout_link_text",
                                                        "frontend_slug"
                                                       );