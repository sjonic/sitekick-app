CREATE TABLE IF NOT EXISTS `languages` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `own_label` varchar(255) NOT NULL,
  `abbreviation` varchar(3) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX LANG_FIELD(`abbreviation`, `locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `languages` (`id`, `label`, `own_label`, `abbreviation`, `locale`, `active`) VALUES (NULL, 'Nederlands', 'Nederlands', 'nl', 'nl_NL', '1');

CREATE TABLE IF NOT EXISTS `i18n` (
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `locale`      varchar(6) NOT NULL,
    `model`       varchar(255) NOT NULL,
    `foreign_key` int(10) NOT NULL,
    `field`       varchar(255) NOT NULL,
    `content`     TEXT,
    PRIMARY KEY (`id`),
    UNIQUE INDEX I18N_LOCALE_FIELD(`locale`, `model`, `foreign_key`, `field`),
    INDEX I18N_FIELD(`model`, `foreign_key`, `field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `i18n` (`locale`, `model`, `foreign_key`, `field`, `content`) VALUES
('nl_NL', 'Navigations', 3, 'slug', 'contact');