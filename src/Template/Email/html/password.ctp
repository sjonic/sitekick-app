<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
	<tbody>
	<!-- Title -->
	<tr>
		<td style="font-family: Helvetica, arial, sans-serif; font-size: 28px; color: #333333; text-align:center; line-height: 30px;" st-title="fulltext-heading">
			<?php echo __('Stel je wachtwoord in'); ?>
		</td>
	</tr>
	<!-- End of Title -->
	<!-- spacing -->
	<tr>
		<td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
	</tr>
	<!-- End of spacing -->
	<!-- content -->
	<tr>
		<td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align:center; line-height: 30px;" st-content="fulltext-content">
            <?php echo __('{0} {1} geeft je toegang tot Sitekick. Dit is het beheersysteem achter', [$authUser['firstname'], $authUser['lastname']]); ?>

			<?php echo $this->Html->link($this->Url->build('/',true),$this->Url->build('/',true),['escape' => false,'style' => 'text-decoration: none; color: #3cb9d5;']); ?>
            <br><br>
            <?php echo __('Is werken met Sitekick lastig? Helemaal niet! Het systeem is zo gebouwd dat iedereen ermee overweg kan. Of je nou met een pc, tablet of smartphone werkt.'); ?>
			<br><br>
			<?php echo __('Waar wacht je nog op?'); ?>
		</td>
	</tr>
	<!-- End of content -->
	<!-- button -->
    
	<table width="140" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
		<tbody>
		<tr>
			<td width="169" height="45" align="center">
				<div class="imgpop">
					<?php echo $this->Html->link(__('Aan de slag!'), ['_full' => true, 'controller' => 'Users', 'plugin' => 'Users','prefix' => 'sitekick','action' => 'setpassword', $hash, $locale],['escape' => false, "style" => "background: #2eb582 none repeat scroll 0 0; border: medium none; border-radius: 3px; color: #fff; display: inline-block; font-size: 16.5px; padding: 10px 20px; font-family: Helvetica, arial, sans-serif; font-size: 16px; text-align:center; text-decoration: none;"]); ?>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- end of button -->
	</tbody>
</table>