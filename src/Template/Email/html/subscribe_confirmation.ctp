<!-- confirmation -->
<?php echo __('Bedankt voor jouw inschrijving voor onze nieuwsbrief.<br>Klik op onderstaande link om jouw inschrijving te bevestigen.');
?>

<br><br><br>

<div>
	<!--[if mso]>
	<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="<?php echo $this->Url->build([
					'controller'	=> 'Mailings',
					'action'		=> 'subscribe', $data->unique_hash
				], true); ?>" style="height:40px;v-text-anchor:middle;width:300px;" arcsize="10%" stroke="f" fillcolor="#66a2c0">
		<w:anchorlock/>
		<center style="color:#ffffff;font-family:sans-serif;font-size:20px;font-weight:bold;">
			<?php echo __('Inschrijving bevestigen'); ?>
		</center>
	</v:roundrect>
	<![endif]-->
	<![if !mso]>
	<table cellspacing="0" cellpadding="0"> <tr>
			<td align="center" width="300" height="40" bgcolor="#66a2c0" style=" color: #ffffff; display: block;">
				<a href="<?php echo $this->Url->build([
					'controller'	=> 'Mailings',
					'action'		=> 'subscribe', $data->unique_hash
				], true); ?>" style="font-size:20px; font-weight: bold; font-family:sans-serif; text-decoration: none; line-height:40px; width:100%; display:inline-block">
    <span style="color: #ffffff;">
      <?php echo __('Inschrijving bevestigen'); ?>
    </span>
				</a>
			</td>
		</tr> </table>
	<![endif]>
</div>


<br><br>


<?php echo __('Met vriendelijke groet'); ?>