<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
	<tbody>
	<!-- Title -->
	<tr>
		<td style="font-family: Helvetica, arial, sans-serif; font-size: 28px; color: #333333; text-align:center; line-height: 30px;" st-title="fulltext-heading">
            <?php echo __('Wachtwoord vergeten?'); ?>
		</td>
	</tr>
	<!-- End of Title -->
	<!-- spacing -->
	<tr>
		<td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
	</tr>
	<!-- End of spacing -->
	<!-- content -->
	<tr>
		<td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align:center; line-height: 30px;" st-content="fulltext-content">
            <?php echo __('Beste {0}, ben je je wachtwoord vergeten? Kan gebeuren, je moet tegenwoordig ook zoveel wachtwoorden onthouden.', [$firstname]); ?>
			<br><br>
			<?php echo __('Klik op onderstaande link om een nieuw wachtwoord in te stellen.'); ?>
		</td>
	</tr>

	<tr>
		<td width="169" height="45" align="center">
			<div class="imgpop">
				<br><br><?php echo $this->Html->link(__('Wachtwoord instellen'), ['_full' => true, 'controller' => 'Users', 'plugin' => 'Users','prefix' => 'sitekick','action' => 'setpassword', $hash],['escape' => false, 'style' => "background: #2eb582 none repeat scroll 0 0; border: medium none; border-radius: 3px; color: #fff; display: inline-block; font-size: 16.5px; padding: 10px 20px; font-family: Helvetica, arial, sans-serif; font-size: 16px; text-align:center; text-decoration: none;"]); ?><br><br><br>
			</div>
		</td>
	</tr>		
		
	<tr>
		<td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align:center; line-height: 30px;" st-content="fulltext-content">
			<?php echo __('Heb je geen nieuw wachtwoord aangevraagd? Dan kun je dit bericht gewoon negeren en verwijderen.'); ?><br><br>
		</td>
	</tr>
	<!-- End of content -->
	</tbody>
</table>