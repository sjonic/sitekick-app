<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
    <tbody>
    <!-- Title -->
    <tr>
        <td style="font-family: Helvetica, arial, sans-serif; font-size: 28px; color: #333333; text-align:center; line-height: 30px;" st-title="fulltext-heading">
            <?php echo __('Contactformulier'); ?>
        </td>
    </tr>
    <!-- End of Title -->
    <!-- spacing -->
    <tr>
        <td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
    </tr>
    <!-- End of spacing -->
    <!-- content -->
    <tr>
        <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align:center; line-height: 30px;" st-content="fulltext-content">

            <strong><?php echo __('Naam'); ?></strong><br/>
            <?php echo $name; ?>
            <br/><br/>
			
			<?php if($companyname): ?>
			<strong><?php echo __('Bedrijfsnaam'); ?></strong><br/>
            <?php echo $companyname; ?>
            <br/><br/>
			<?php endif; ?>

            <strong><?php echo __('Telefoonnummer'); ?></strong><br/>
            <?php echo $phone; ?>
            <br/><br/>

            <strong><?php echo __('E-mailadres'); ?></strong><br/>
            <?php echo $email; ?>
            <br/><br/>

            <strong><?php echo __('Bericht'); ?></strong><br/>
            <?php echo nl2br($message); ?>

        </td>
    </tr>
    <!-- End of content -->
    <!-- button -->
    <table width="140" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
        <tbody>
        <tr>
            <td width="169" height="45" align="center">
                <div class="imgpop">
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <!-- end of button -->
    </tbody>
</table>