<table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
	<tbody>
	<!-- Title -->
	<tr>
		<td style="font-family: Helvetica, arial, sans-serif; font-size: 28px; color: #333333; text-align:center; line-height: 30px;" st-title="fulltext-heading">
            <?php echo __('{0} ({1}) heeft je hulp nodig!', [$name, $email]); ?>
		</td>
	</tr>
	<!-- End of Title -->
	<!-- spacing -->
	<tr>
		<td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
	</tr>
	<!-- End of spacing -->
	<!-- content -->
	<tr>
		<td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align:center; line-height: 30px;" st-content="fulltext-content">
			<?php echo $question; ?><br><br>
            <?php echo __('Bezoek de site van {0} op:', [$name]); ?> <?php echo $this->Html->link($this->Url->build('/',true),$this->Url->build('/',true),['escape' => false,'style' => 'text-decoration: none; color: #3cb9d5;']); ?>
		</td>
	</tr>
	<!-- End of content -->
	</tbody>
</table>