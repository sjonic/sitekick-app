<?php
if( isset($load_translations) && $this->elementExists('js_include_translations') ) {
    echo $this->element('js_include_translations');
}
?>

<!-- Sitekick navigation bar -->
<div id="sitekick-navbar" role="navigation">
    <div class="row">
        <!-- Logo area -->
        <div class="logo-area">
            <div class="row">
                <div class="navbar-collapse-container">
                    <?php if($SitekickAreaActive): ?>
                        <div class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-list"></span>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="logo-container">
                    <?php echo $this->Html->link($this->Html->image("sitekick/template/logo_sitekick.svg",['width' => 137, 'height' => 47]),'/sitekick',['escape'=> false]); ?>
                </div>
            </div>
        </div>

        <!-- Navigation area -->
        <div id="sitekick-navbar-shortcuts">

            <!-- Admin mode switch buttons mobile -->
            <ul class="admin-mode visible-xs">
                <li class="admin-mode-text hidden-xs hidden-sm"><?php echo __('Beheermodus:'); ?></li>

                <?php if($SitekickAreaActive == true) { ?>
                    <li id="sitefront">
                        <?php echo $this->Html->link('<span class="icon-web"></span>',$front_url,['escape' => false, 'class' => ($SitekickAreaActive ? "" : "active")]); ?>
                    </li>
                <?php } else if($SitekickAreaActive == false) { ?>
                    <li id="siteadmin">
                        <?php echo $this->Html->link('<span class="icon-sitekick"></span>', $back_url, ['escape' => false, 'class' => ($SitekickAreaActive ? "active" : "")]); ?>
                    </li>
                <?php } ?>

            </ul>

            <!-- Admin mode switch buttons desktop -->
            <ul class="admin-mode hidden-xs">
                <li class="admin-mode-text hidden-xs hidden-sm"><?php echo __('Beheermodus:'); ?></li>
                <li id="siteadmin">
                    <?php echo $this->Html->link('<span class="icon-sitekick"></span>', $back_url, ['escape' => false, 'class' => ($SitekickAreaActive ? "active" : "")]); ?>
                </li>
                <li id="sitefront">
                    <?php echo $this->Html->link('<span class="icon-web"></span>',$front_url,['escape' => false, 'class' => ($SitekickAreaActive ? "" : "active")]); ?>
                </li>
            </ul>

            <ul class="nav navbar-nav hidden-xs" id="newitems">
                <!-- Options dropdown -->
                <li class="dropdown options">
                    <a href="javascript:;" class="dropdown-toggle" onclick="$(this).closest('.dropdown').toggleClass('open');"><?php echo __('Toevoegen'); ?> <span class="icon-dropdown option-icon"></span></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <?php foreach($settings as $key => $item): ?>

                            <?php if($this->isAllowed->plugin($key) && isset($item['menu']) && $item['in_menu'] == 1 && isset($item['in_dropdown']) && $item['in_dropdown'] == 1  && isset($item['name_single']) && isset($item['slug'])): ?>
                                <?php if( isset($item['new_pages_allowed']) && $item['new_pages_allowed'] == 0 ) { } else { ?>
                                    <li><?php echo $this->Html->link('<span class="'.$item['icon'].'"></span>'.$item['name_single'], '/sitekick/'.  ($key != "Pages" && $item['slug'] ? $item['slug'] : strtolower($key) ) .'/add' ,['escape' => false]); ?></li>
                                <?php } ?>
                            <?php endif; ?>

                            <?php
                            if(isset($item['use_types'])){
                                foreach($types as $type){
                                    $class = '';
                                    if($type['admin'] == 1 || $type['can_add'] == 0){
                                        $class .= " adminonly";
                                    }
                                    if ( $type['can_add'] == 1 && ( $type['admin'] == 0 || ($type['admin'] == 1 && $authUser['superadmin'] == 1 ) ) ) {
                                        $typeSlug = '/sitekick/' . strtolower( $key ) . "/" . $type['content_type'] . '/add';
                                        echo '<li>' . $this->Html->link( '<span class="' . $type['icon'] . '"></span>' . $type['name_single'], $typeSlug, [
                                                'escape' => false,
                                                'class'  => $class
                                            ] ) . '</li>';
                                    }
                                }
                            }
                            ?>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </div>

        <!-- User area -->
        <div id="sitekick-navbar-account">
            <div class="float-right">

                <?php if(isset($settings['Base']['test_environment']) && $settings['Base']['test_environment'] == 1) : ?>
                    <ul class="testenv">
                        <li>
                            <a><?php echo __('Test'); ?></a>
                        </li>
                    </ul>
                <?php endif; ?>


                <?php
                if( !empty($languages) && ($languages->count() > 1 && !isset($isFront)) ){


                    $abbrExtra = strtolower(substr($activeLanguage->locale, -2, 2));

                    ?>
                    <!-- language dropdown -->
                    <ul id="languages-navbar">
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle languages" onclick="$(this).closest('.dropdown').toggleClass('open');">
                                <span class="account-text"><span class="flag-icon flag-icon-circle flag-icon-<?php echo $abbrExtra; ?>"></span> <?php echo $activeLanguage->own_label; ?></span>
                                <span class="account-text-small"><span class="flag-icon flag-icon-circle flag-icon-<?php echo $abbrExtra; ?>"></span> <?php echo $activeLanguage->abbreviation; ?></span>
                                <span class="icon-dropdown icon-language-collapse account-icon"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <?php
                                foreach($languages as $lang){

                                    $abbrExtra = strtolower(substr($lang['locale'], -2, 2));

                                    echo '<li>'.$this->Html->link('<span class="flag-icon flag-icon-circle flag-icon-'.$abbrExtra.' flag-icon-'.strtolower($lang['abbreviation']).'"></span> ' . __($lang['own_label']), [
                                            'prefix' 	=> 'sitekick',
                                            'plugin'	=> null,
                                            'controller'=> 'Languages',
                                            'action'=>'set_current_language',
                                            $lang['id']
                                        ], ['escape'=>FALSE]).'</li>';
                                }
                                ?>
                            </ul>
                        </li>
                    </ul><!-- ./ language dropdown -->
                    <?php
                }
                ?>

                <ul id="profile">
                    <!-- Account dropdown -->
                    <li class="dropdown">
                        <a href="javascript:;" class="profile-dropdown dropdown-toggle" onclick="$(this).closest('.dropdown').toggleClass('open');">
                            <?php
                            if(isset($authUser['avatar']) && !empty($authUser['avatar']) ) {
                                echo $this->Html->image( '/files/Users/thumbnails/' . $authUser['avatar'], [ 'class' => 'img-circle avatar','width' => 46, 'height' => 46 ] );
                            }else{
                                echo $this->Html->image( '/img/sitekick/template/avatar.jpg', [ 'class' => 'img-circle avatar','width' => 46, 'height' => 46 ] );
                            }
                            ?>
                            <span class="account-text"><?php echo $authUser['firstname']." ". $authUser['lastname']; ?></span>
                            <span class="icon-dropdown account-icon"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><?php echo $this->Html->link('<span class="icon-team"></span>' . __('Profiel wijzigen'),'/sitekick/users/edit/' . $authUser['id'],['class' => 'logout-link' ,'escape' => false]); ?></li>
                            <li>
                                <?php
                                $logoutUrl = '/sitekick/users/logout?redirect_url=/';
                                if(isset($isFront)){
                                    $logoutUrl = '/sitekick/users/logout?redirect_url=/';
                                }
                                echo $this->Html->link('<span class="icon-lock-open"></span>' . __('Uitloggen'),$logoutUrl,['class' => 'logout-link' ,'escape' => false]);
                                ?>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>