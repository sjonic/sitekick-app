<?php
	use Cake\Core\Configure;

	$settings = json_decode(Configure::read("settings"),true);

	$this->layout('error');

	if( ! empty($settings['Base']['theme'])){
		$this->layout($settings['Base']['theme'].".error");
	}


	if(isset($this->request->params['prefix']) && $this->request->params['prefix']){
		$this->layout('sitekick_error');
	}
?>


<?php if(isset($this->request->params['prefix']) && $this->request->params['prefix']){ ?>

    <h1><?php echo __('De opgevraagde pagina <strong>{0}</strong> kon niet gevonden worden.', [$_SERVER['REQUEST_URI']]); ?></h1>

    <?php
        if (Configure::read('debug')):
            echo $this->element('exception_stack_trace');
        endif;
    ?>

<?php } else {  ?>
    <main role="main">
        <section class="container-fluid pagecontainer-1">
            <div class="container">
                <h1><?php echo __('Pagina niet gevonden'); ?></h1>

                <div class="row">
                    <div class="col-md-12">
                        <br/>
                        <?php echo __('De opgevraagde pagina <strong>{0}</strong> kon niet gevonden worden.', [$_SERVER['REQUEST_URI']]); ?>
                        <?php
                        if (Configure::read('debug')):
                            echo $this->element('exception_stack_trace');
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php } ?>
<script>
	$('.stack-details').show();
</script>