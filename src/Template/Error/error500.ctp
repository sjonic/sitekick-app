<?php
use Cake\Core\Configure;

$settings = json_decode(Configure::read("settings"),true);

//$this->layout('default');
$this->layout('error');

if( ! empty($settings['Base']['theme'])){
	$this->layout($settings['Base']['theme'].".error");
}
if(isset($this->request->params['prefix']) && $this->request->params['prefix']){
	$this->layout('sitekick_error');
}

if(isset($this->request->params['prefix'])){

}
?>

<h1><?= $message; ?></h1>
<br>
	<strong><?= __d('cake', 'Error'); ?>: </strong>
	<?= __('An Internal Error Has Occurred.'); ?>
<?php
if (Configure::read('debug')):

    // SQL-errors
    if (!empty($error->queryString)){ ?>
        <hr>
        <p class="notice">
            <strong>SQL Query: </strong>
            <?= h($error->queryString) ?>
        </p>
    <?php
    }

    // File tracing
    if ($error instanceof Error){ ?>
        <hr>
        <strong>Error in: </strong>
        <?= sprintf('%s, line %s', str_replace(ROOT, 'ROOT', $error->getFile()), $error->getLine()) ?>
    <?php }

    // xdebug
    if (extension_loaded('xdebug')) :
        xdebug_print_function_stack();
    endif;

    // exception stack tracing
    ?>
    <hr>
    <button type="button" onclick="$('.sitekick-exception-stack-trace').toggle();"><?= __('Bekijk foutcodes'); ?></button>
    <div class="sitekick-exception-stack-trace" style="display: none;">
        <?= $this->element('exception_stack_trace'); ?>
    </div>
    <?php
endif;
?>


<script>
$('.stack-details').show();
</script>