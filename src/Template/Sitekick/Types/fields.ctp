<h1><?php echo __('Velden voor "{0}"', [$fields->name]); ?></h1>

<div class="green-table">

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th><?php echo __('Naam'); ?></th>
					<th><?php echo __('Type'); ?></th>
                    <th><?php echo __('Variabele'); ?></th>
					<th><?php echo __('Verplicht'); ?></th>
					<th><?php echo __('Gebruik in overzicht'); ?></th>
					<th><?php echo __('Actief'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>


			<?php foreach($fields->type_fields as $field): ?>


				<tr data-id="<?php echo $field->id; ?>" id="field-<?= $field->id; ?>">
					<td><?php echo $field->name;?></td>
					<td><?php echo $field->fieldtype;?></td>
                    <td><?php echo $field->key; ?></td>
					<td>
						<?php echo $this->Form->input('required', [
							'value' => $field->required,
							'data-id' => $field->id,
							'class' => 'switch toggle_active',
							'checked'=> isset($field->required) && $field->required == 0 ? false : true
						]);?>
					</td>
					<td>
						<?php echo $this->Form->input('overview', [
							'value' => $field->overview,
							'data-id' => $field->id,
							'class' => 'switch toggle_active',
							'checked'=> isset($field->overview) && $field->overview == 0 ? false : true
						]);?>
					</td>
					<td>
						<?php echo $this->Form->input('active', [
							'value' => $field->active,
							'data-id' => $field->id,
							'class' => 'switch toggle_active',
							'checked'=> isset($field->active) && $field->active == 0 ? false : true
						]);?>
					</td>

					<td>&nbsp;&nbsp;
						<?php echo $this->Html->link('<i class="icon-pencil-2 action-button-small action-edit"></i>', ['action' => 'editfield', $field->id], ['escape' => false]); ?>
						 &nbsp;&nbsp;
						<?php echo $this->Form->postLink('<i class="icon-delete action-button-small action-edit"></i>', ['action' => 'deletefield', $field->id], ['escape' => false, 'confirm' => __('Weet je zeker dat je <strong>{0}</strong> wilt verwijderen?', $field->name)]); ?>
					</td>
				</tr>
				<?php
			endforeach;
			?>
			</tbody>
		</table>
	</div>
</div>

<?php echo $this->Html->link(__('Nieuw veld toevoegen'), ['action' => 'addfield',$fields->id], ['class' => 'btn btn-primary']); ?>

<?php echo $this->Html->script('sitekick/jquery-ui.min'); ?>

<script>
	$(document).ready(function(){

	    $(".table tbody").sortable({
            update: function(){
                var ids = $(".table tbody").sortable("serialize");
                $.ajax({
                    url: sitekickUrl + 'types/saveFieldOrder',
                    data: ids,
                    type: 'post'
                }).done(function(response){
                    console.log(response);
                }).error(function(response){
                    alert("Could not save new ranking");
                });
            }
        });

		$(".switch").switchButton({
			'label' : false,
			'onclick' : function(element){
				var value 	= 1;
				var id 		= $(element).closest('tr').data('id');
				if( $(element).find('.off').length > 0 ){
					value = 0;
				}

				$.ajax({
					method: 'post',
					url: '<?php echo $this->Url->build(['action'=>'updatefield']); ?>/' + id,
					data: { active: value }
				});
			}
		});

	});
</script>