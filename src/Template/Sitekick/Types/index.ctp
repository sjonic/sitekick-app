<h1><?php echo __('Content types'); ?></h1>

<div class="green-table">

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th><?php echo __('Type'); ?></th>
					<th><?php echo __('Naam enkelvoud'); ?></th>
					<th><?php echo __('Naam meervoud'); ?></th>
					<th><?php echo __('Heeft eigen pagina'); ?></th>
					<th><?php echo __('Actief'); ?></th>
					<th><?php echo __('Velden'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($typesOverview as $type):

				?>
				<tr data-id="<?php echo $type->id; ?>">
					<td><?php echo $type->content_type;?></td>
					<td><?php echo $type->name_single;?></td>
					<td><?php echo $type->name;?></td>
                    <td><?php echo ($type->has_page) ? '<span class="icon-check-2"></span>' : ''; ?></td>
					<td><?php echo $this->Form->input('active', [
							'value' => 1,
							'data-id' => $type->id,
							'class' => 'switch toggle_active',
							'checked'=> isset($type->active) && $type->active == 0 ? false : true
						]);?></td>
					<td>
						<?php echo $this->Html->link('<i class="icon-list action-button-small action-edit"></i>', ['action' => 'fields', $type->id], ['escape' => false]); ?>
					</td>
					<td>&nbsp;&nbsp;
						<?php echo $this->Html->link('<i class="icon-pencil-2 action-button-small action-edit"></i>', ['action' => 'edit', $type->id], ['escape' => false]); ?>
						 &nbsp;&nbsp;
						<?php echo $this->Form->postLink('<i class="icon-delete action-button-small action-edit"></i>', ['action' => 'delete', $type->id], ['escape' => false, 'confirm' => __('Weet je zeker dat je <strong>{0}</strong> wilt verwijderen?', $type->content_type)]); ?>
					</td>
				</tr>
				<?php
			endforeach;
			?>
			</tbody>
		</table>
	</div>
</div>

<?php echo $this->Html->link(__('Nieuw content type toevoegen'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>


<script>
	$(document).ready(function(){
		$(".switch").switchButton({
			'label' : false,
			'onclick' : function(element){
				var value 	= 1;
				var id 		= $(element).closest('tr').data('id');
				if( $(element).find('.off').length > 0 ){
					value = 0;
				}

				$.ajax({
					method: 'post',
					url: '<?php echo $this->Url->build(['action'=>'edit']); ?>/' + id,
					data: { active: value }
				});
			}
		});

	});
</script>