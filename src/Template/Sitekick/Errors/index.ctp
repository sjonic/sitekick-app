
<h1><?php echo __('Errorlog'); ?></h1>


<br/>
<?php echo $this->Html->link(__('Errorlog legen'),'/sitekick/errors/deleteAll',['escape' => false, 'class' => 'btn btn-primary']); ?>
<br/><br/>

<div class="green-table">

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th><?php echo __('Bericht'); ?></th>
					<th><?php echo __('Bestand'); ?></th>
					<th><?php echo __('Regel'); ?></th>
					<th><?php echo __('Datum'); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($errors as $error): ?>
				<tr>
					<td><?php echo $error->message; ?></td>
					<td><?php echo $error->file; ?></td>
					<td><?php echo $error->line; ?></td>
					<td><?php echo $error->date; ?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
