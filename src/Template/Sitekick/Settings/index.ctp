<h2><span class="icon-manage"></span><?php echo __('Instellingen'); ?></h2>

<div class="search">
    <input id="search-input" type="search" class="form-control" name="keyword" value=""
           placeholder="<?php echo __('Vind een instelling'); ?>"/>
    <br><br>
</div>

<div class="table-responsive green-table user-table">
    <table class="table table-striped" id="dataTable">

        <thead>
        <tr>
            <th style="width: 30%;"><?php echo __('Omschrijving'); ?></th>
            <th style="width: 30%;"><?php echo __('Waarde'); ?></th>
        </tr>
        </thead>
        <tbody>

        <?php
        if ($systemsettings->isEmpty()) {
            echo '<tr><td colspan="2">' . __('Er zijn geen instellingen gevonden voor jouw Sitekick omgeving.') . '</td>';
        } else {
            foreach ($systemsettings as $setting) {
                ?>
                <tr>
                    <td>
                        <?php echo $setting->description; ?><br/>
                        <span style="font-size: 13px; color:#ababab;"><?php echo $setting->explanation; ?></span>
                    </td>
                    <td style="word-break: break-all;">
                        <?php
                        echo $this->Inline->edit([
                            'entity' => $setting,
                            'field' => 'value',
                            'editor' => 'basic  ',
                            'plugin' => 'Settings',
                            'controller' => 'Settings'
                        ]);
                        ?>
                    </td>
                </tr>
                <?php
            }
        } ?>
        </tbody>
    </table>
</div>


<?php
echo $this->Html->css(['sitekick/jquery.dataTables.min', 'sitekick/settings']);
$this->Html->script([
    'sitekick/jquery-ui.min',
    'sitekick/jquery.dataTables.min',
    'sitekick/settings'
], ['block' => 'scriptBottom']);