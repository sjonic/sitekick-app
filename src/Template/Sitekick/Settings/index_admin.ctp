<h2><span class="icon-manage"></span><?php echo __('Instellingen'); ?></h2>

<?php
echo $this->Html->link('<span class="icon-plus"></span>' . __('Instelling toevoegen'), ['action' => 'add'], ['class' => 'btn btn-primary', 'escape' => false]);
?>

<div class="buttons" style="margin-top: 30px;">
    <?php
    echo $this->Html->link('Base', '#', ['class' => 'settingbutton btn btn-small btn-primary', 'id' => 'base']);
    foreach ($plugins as $plugin) {
        echo '&nbsp;' . $this->Html->link($plugin, '#', ['class' => 'settingbutton btn btn-small btn-primary', 'id' => $plugin]);
    }
    ?>
</div>

<div class="search">
    <input id="search-input" type="search" class="form-control" name="keyword" value=""
           placeholder="<?php echo __('Vind een instelling'); ?>"/>
    <br><br>
</div>

<div class="table-responsive green-table user-table">
    <table class="table table-striped" id="dataTable">

        <thead>
        <tr>
            <th style="width: 10%;"><?php echo __('Plugin'); ?></th>
            <th style="width: 30%;"><?php echo __('Omschrijving'); ?></th>
            <th style="width: 30%;"><?php echo __('Code'); ?></th>
            <th><?php echo __('Waarde'); ?></th>
            <th style="width: 10%"></th>
        </tr>
        </thead>
        <tbody>

        <?php $prevPlugin = "";
        if ($systemsettings->isEmpty()) {
            echo '<tr><td colspan="4"><div class="message">' . __('Er zijn geen instellingen gevonden voor jouw Sitekick omgeving.') . '</div></td>';
        } else {
            foreach ($systemsettings as $setting): ?>
                <?php if ($setting->plugin != $prevPlugin): ?>

                <?php endif; ?>

                <tr>
                    <td><?php echo ucfirst($setting->plugin); ?></td>
                    <td><?php echo $setting->description; ?><br/><span
                                style="font-size: 13px; color:#ababab;"><?php echo $setting->explanation; ?></span></td>
                    <td><code>$settings['<?php echo $setting->plugin; ?>']['<?php echo $setting->name; ?>']</code></td>
                    <td style="word-break: break-all;">
                        <?php
                        echo $this->Inline->edit([
                            'entity' => $setting,
                            'field' => 'value',
                            'editor' => 'basic  ',
                            'plugin' => 'Settings',
                            'controller' => 'Settings'
                        ]);
                        ?>
                    </td>
                    <td class="text-right">
                        <?php echo $this->Html->link('<i class="icon-pencil-2 action-button-small action-edit"></i>', ['action' => 'edit', $setting->id], ['escape' => false]); ?>
                        <?php echo $this->Form->postLink('<i class="icon-delete action-button-small action-delete"></i>', ['action' => 'delete', $setting->id],
                            [
                                'escape' => false,
                                'confirm' => __('Weet je zeker dat je "{0}" wilt verwijderen?', $setting->name)]); ?>
                    </td>
                </tr>

                <?php $prevPlugin = $setting->plugin; ?>

            <?php endforeach;
        } ?>
        </tbody>
    </table>
</div>


<?php
echo $this->Html->css(['sitekick/jquery.dataTables.min', 'sitekick/settings']);
$this->Html->script([
    'sitekick/jquery-ui.min',
    'sitekick/jquery.dataTables.min',
    'sitekick/settings'
], ['block' => 'scriptBottom']);
