<?php echo $this->Form->create('inline'); ?>
<?php echo $this->Form->input('data', ['type' => 'text', 'label' => false, 'value' => $content, 'class' => '']); ?>
<?php echo $this->Form->input('id', ['type' => 'hidden']); ?>
<?php echo $this->Form->input('field', ['type' => 'hidden', 'value' => $field]); ?>

    <ul class="inline-edit-controls" style="bottom: auto; top: -35px;">
        <li class="inline-edit-counter"><?php echo __('Nog <span class="sitekick_counter">0</span> tekens over'); ?></li>
        <li><?php echo $this->Form->button('Opslaan', ['class' => 'inline-save']); ?></li>
        <li><?php echo $this->Form->button('&#10005;', ['class' => 'inline-cancel']); ?></li>
    </ul>

<?php echo $this->Form->end(); ?>