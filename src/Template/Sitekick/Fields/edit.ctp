<h1><?php echo __('Veld bewerken'); ?></h1>

<?php
echo $this->Form->create($field);
echo '<div class="formblock formblock-compact row">';
echo $this->Form->input('name', ['label' => __('Naam')]);
echo $this->Form->input('fieldtype', ['label' => __('Type')]);
echo '</div>';
echo $this->Form->submit(__('Veld opslaan'), ['class'=> 'btn btn-primary']);
echo '&nbsp; ' . $this->Html->link(__('Annuleren'), ['action' => 'index']);
echo $this->Form->end();
?>