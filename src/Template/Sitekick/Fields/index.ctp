<h1><?php echo __('Velden'); ?></h1>

<div class="green-table">

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th><?php echo __('Naam'); ?></th>
					<th><?php echo __('Type'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($overview as $field):
				?>
				<tr data-id="<?php echo $field->id; ?>">
					<td><?php echo $field->name;?></td>
					<td><?php echo $field->fieldtype;?></td>
					<td>
						<?php echo $this->Html->link('<i class="icon-pencil-2 action-button-small action-edit"></i>', ['action' => 'edit', $field->id], ['escape' => false]); ?>
						 &nbsp;&nbsp;
						<?php echo $this->Form->postLink('<i class="icon-delete action-button-small action-edit"></i>', ['action' => 'delete', $field->id], ['escape' => false, 'confirm' => __('Weet je zeker dat je <strong>{0}</strong> wilt verwijderen?', $field->name)]); ?>
					</td>
				</tr>
				<?php
			endforeach;
			?>
			</tbody>
		</table>
	</div>
</div>

<?php echo $this->Html->link(__('Veld toevoegen'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>