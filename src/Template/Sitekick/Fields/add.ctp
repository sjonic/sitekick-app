<h1><?php echo __('Veld toevoegen'); ?></h1>

<?php
echo $this->Form->create($field);
echo '<div class="formblock formblock-compact row">';
echo $this->Form->input('name', ['label' => __('Naam')]);
echo $this->Form->input('fieldtype', ['label' => __('Type')]);
echo '</div>';
echo $this->Form->submit(__('Veld opslaan'), ['class'=> 'btn btn-primary']);
echo $this->Form->end();
?>

<script>
	$(document).ready(function(){
		$(".switch").switchButton();
	});
</script>