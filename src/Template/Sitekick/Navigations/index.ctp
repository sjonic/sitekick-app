<!-- File: /src/Template/Navigations/index.ctp -->
<?php $this->Html->script('sitekick/jquery.inputs.js', array('block' => 'scriptBottom')); ?>
<?php $this->Html->script('sitekick/jquery.nestable.js', array('block' => 'scriptBottom')); ?>
<?php $this->Html->css('sitekick/switchbuttons.css', array('block' => true)); ?>


<h2><span class="icon-menu"></span><?php echo __('Pagina’s en menu'); ?></h2>

<!--top-->
<div class="main-top">
	<?php if($settings['Pages']['new_pages_allowed'] == 1) { ?>
    <a class="btn btn-primary add-page" href="<?php echo $this->Url->build(['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'add']); ?>"><span class="icon-plus"></span> <?php echo __('Pagina toevoegen'); ?></a>
    <?php } ?>
</div>


<div id="overview-head" class="<?php if(!empty($settings['Pages']['overview_use_collapse']) && $settings['Pages']['overview_use_collapse'] == 1) echo 'overview-head--collapse' ?> ">
	<ul>
		<li><span><?php echo __('Pagina'); ?></span></li>
		<li><span><?php echo __('Opties'); ?></span></li>
		<li class="hidden-xs"><span><?php echo __('Menu-item'); ?></span><i class="icon-info" data-toggle="toggle" data-placement="top" title="Menu-item" data-content="<?php echo __('Deze pagina weergeven in het menu?'); ?>"></i></li>
		<li class="hidden-xs"><span><?php echo __('Online'); ?></span><i class="icon-info" data-toggle="toggle" data-placement="top" title="Online" data-content="<?php echo __('Mag deze pagina online te vinden zijn (geïndexeerd in Google)?'); ?>"></i></li>

	</ul>
</div>

<form>
	<div class="dd" id="nestable">
		<!-- Default homeitem -->
		<div class="dd-item <?php if(!empty($settings['Pages']['overview_use_collapse']) && $settings['Pages']['overview_use_collapse'] == 1) echo 'dd-item--collapse' ?>" data-id="1">
			<div class="nomove"><span class="icon-list"></span></div>

			<div class="dd-handle">
				<div class="title">
					<?php echo $this->Html->link($navigations->toArray()[0]->name, ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'edit', 1], ['escape'=>true]); ?>
                </div>

                <div class="display hidden-xs"></div>
				<div class="active hidden-xs"></div>
                <div class="options">
                    <?php echo $this->Html->link('<span class="icon-pencil-2"></span>', ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'edit', 1], ['escape'=>false, 'title' => __('Homepage bewerken'),'class'=>'action-button-small action-edit-white']); ?>
                </div>
			</div>
		</div>

		<ol class="dd-list">
			<?php foreach($navigations as $key => $navigation) { ?>

				<?php if($key > 0): ?>
				
					<?php if(empty($navigation->plugin) || !empty($navigation->plugin) && isset($settings[$navigation->plugin])) { ?>
				
						<li class="dd-item <?php if(!empty($settings['Pages']['overview_use_collapse']) && $settings['Pages']['overview_use_collapse'] == 1) echo 'dd-item--collapse' ?> <?php if(!empty($navigation->children)) echo 'has-children' ?>" data-id="<?php echo $navigation->id; ?>">
							<div class="move <?php if($key ==1): ?>move-page<?php endif;?>"><span class="icon-list"></span></div>
							<div class="dd-handle">
								<div class="title">
									<?php if($navigation->plugin): ?>
										<?php echo $this->Html->link($settings[$navigation->plugin]['name'], '/sitekick/'.strtolower($settings[$navigation->plugin]['slug']).'/index', ['title' => $settings[$navigation->plugin]['name_single'] . " toevoegen",'escape'=>true]); ?>
									<?php else:
										// check empty translation
										if($navigation->name == '' ){
											foreach($navigation->_translations as $t){
												if( !empty($t->name) ){
													$navigation->name = '<span class="text-muted">' . $t->name . ' ' . __('(nog niet vertaald)') . '</span>';
													break;
												}
											}
										} ?>

										<?php echo $this->Html->link($navigation->name, ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'edit', $navigation->id], ['escape'=> false, 'title' => __('{0} bewerken', [$settings['Pages']['name_single']])]); ?>
									<?php endif; ?>
								</div>

                                <div class="active hidden-xs <?php if($key ==1): ?>online-page<?php endif;?>"><?php echo $this->Form->input('active', ['label'=>false, 'type' => 'checkbox', 'class'=>'switch', 'data-class'=>'btn_active', 'id'=>$navigation->id, 'checked'=>$navigation->active]); ?></div>
								<div class="display hidden-xs <?php if($key ==1): ?>display-page<?php endif;?>"><?php echo $this->Form->input('display', ['label'=>false, 'type' => 'checkbox', 'class'=>'switch', 'data-class'=>'btn_display', 'id'=>$navigation->id, 'checked'=>$navigation->display]); ?></div>
                                <div class="options">
                                    <?php if($navigation->plugin): ?>
                                        <?php echo $this->Html->link('<span class="icon-plus"></span>', '/sitekick/'.strtolower($settings[$navigation->plugin]['slug']).'/index', ['title' => __('{0} toevoegen', [$settings[$navigation->plugin]['name_single']]),'escape'=>false, 'class'=>'action-button-small action-edit-white']); ?>
                                    <?php else: ?>

                                        <?php echo $this->Html->link('<span class="icon-pencil-2"></span>', ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'edit', $navigation->id], ['title' => __('{0} bewerken', [$settings['Pages']['name_single']]), 'escape'=>false, 'class'=>'action-button-small action-edit-white']); ?>
                                        <?php echo $this->Html->link('<span class="icon-reload"></span>', ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'copy', $navigation->id], ['title' => __('{0} kopiëren', [$settings['Pages']['name_single']]), 'escape'=>false, 'class'=>'action-button-small action-edit-white copy-page']); ?>
                                        <?php if($navigation->can_delete): ?>
                                            <?php echo $this->Html->link('<span class="icon-delete"></span>', ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'delete', $navigation->id], ['title' => __('{0} verwijderen', [$settings['Pages']['name_single']]), 'escape'=>false, 'class'=>'action-button-small action-delete']); ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>

								</div>

                                <?php if(isset($navigation->children)) { ?>

								<ol class="dd-list">
									<?php foreach($navigation->children as $child) { ?>
										<li class="dd-item" data-id="<?php echo $child->id; ?>">
											<div class="move"><span class="icon-list"></span></div>
											<div class="dd-handle">

												<?php if($child->plugin): ?>
													<div class="title"><?php echo $this->Html->link($settings[$child->plugin]['name'], '/sitekick/'.strtolower($settings[$child->plugin]['slug']).'/index', ['title' => __('{0} overzicht', [$settings[$child->plugin]['name_single']]),'escape'=>true]); ?></div>
												<?php else:
													// check empty translation
													if($child->name == '' ){
														foreach($child->_translations as $ct){
															if( !empty($ct->name) ){
																$child->name = '<span class="text-muted">' . $ct->name . ' ' . __('(nog niet vertaald)') . '</span>';
																break;
															}
														}
													} ?>

													<div class="title"><?php echo $this->Html->link($child->name, ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'edit', $child->id], ['escape' => false]); ?></div>
												<?php endif; ?>

                                                <div class="active hidden-xs"><?php echo $this->Form->input('active', ['label'=>false, 'type' => 'checkbox', 'class'=>'switch', 'data-class'=>'btn_active', 'id'=>$child->id, 'checked'=>$child->active]); ?></div>
												<div class="display hidden-xs"><?php echo $this->Form->input('display', ['label'=>false, 'type' => 'checkbox', 'class'=>'switch', 'data-class'=>'btn_display', 'id'=>$child->id, 'checked'=>$child->display]); ?></div>
                                                <div class="options hidden-xs">

                                                    <?php if($child->plugin): ?>
                                                        <?php echo $this->Html->link('<span class="icon-plus"></span>', '/sitekick/'.strtolower($settings[$child->plugin]['slug']).'/add', ['title' => __('{0} toevoegen', [$settings[$child->plugin]['name_single']]),'escape'=>false, 'class'=>'action-button-small action-edit-white']); ?>
                                                    <?php else: ?>
                                                        <?php echo $this->Html->link('<span class="icon-pencil-2"></span>', ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'edit', $child->id], ['title' => __('{0} bewerken', [$settings['Pages']['name_single']]),'escape'=>false, 'class'=>'action-button-small action-edit-white']); ?>
                                                        <?php echo $this->Html->link('<span class="icon-reload"></span>', ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'copy', $child->id], ['title' => __('{0} kopiëren', [$settings['Pages']['name_single']]), 'escape'=>false, 'class'=>'action-button-small action-edit-white copy-page']); ?>
                                                        <?php if($child->can_delete): ?>
                                                            <?php echo $this->Html->link('<span class="icon-delete"></span>', ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'delete', $child->id], ['title' => __('{0} verwijderen', [$settings['Pages']['name_single']]), 'escape'=>false, 'class'=>'action-button-small delete action-delete']); ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>


                                                </div>
											</div>

											<?php if(isset($child->children)) { ?>
												<ol class="dd-list">
													<?php foreach($child->children as $c) { ?>
														<li class="dd-item" data-id="<?php echo $c->id; ?>">
															<div class="move"><span class="icon-list"></span></div>
															<div class="dd-handle sub-sub">
																<div class="title">
																	<?php
																	// check empty translation
																	if($c->name == '' ){
																		foreach($c->_translations as $ctt){
																			if( !empty($ctt->name) ){
																				$c->name = '<span class="text-muted">' . $ctt->name . ' ' . __('(nog niet vertaald)') . '</span>';
																				break;
																			}
																		}
																	} ?>

																	<?php echo $this->Html->link($c->name, ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'edit', $c->id], ['escape'=>false]); ?></div>
																<div class="options hidden-xs">
																	<?php echo $this->Html->link('<span class="icon-pencil-2"></span>', ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'edit', $c->id], ['title' => __('{0} bewerken', [$settings['Pages']['name_single']]),'escape'=>false, 'class'=>'action-button-small action-edit-white']); ?>
																	<?php echo $this->Html->link('<span class="icon-reload"></span>', ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'copy', $c->id], ['title' => __('{0} kopiëren', [$settings['Pages']['name_single']]), 'escape'=>false, 'class'=>'action-button-small action-edit-white copy-page']); ?>
																	<?php if($c->can_delete): ?>
																		<?php echo $this->Html->link('<span class="icon-delete"></span>', ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'delete', $c->id], ['title' => __('{0} verwijderen', [$settings['Pages']['name_single']]), 'escape'=>false, 'class'=>'action-button-small delete action-delete']); ?>
																	<?php endif; ?>
																</div>
																<div class="display hidden-xs"><?php echo $this->Form->input('display', ['label'=>false, 'type' => 'checkbox', 'class'=>'switch', 'data-class'=>'btn_display', 'id'=>$c->id, 'checked'=>$c->display]); ?></div>
																<div class="active hidden-xs"><?php echo $this->Form->input('active', ['label'=>false, 'type' => 'checkbox', 'class'=>'switch', 'data-class'=>'btn_active', 'id'=>$c->id, 'checked'=>$c->active]); ?></div>
																</div>
														</li>
													<?php } ?>
												</ol>
											<?php } ?>
										</li>
									<?php } ?>
								</ol>
							<?php } ?>
						</li>
					<?php } ?>	
				<?php endif; ?>
			<?php } ?>
		</ol>

	</div>
</form>


<?php if(!$this->request->isMobile()): ?>


	<?php
	echo $this->Html->css('sitekick/bootstrap-tour');
	echo $this->Html->script('sitekick/bootstrap-tour');
	?>
	<script>

		var tour = new Tour({

			template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='prev'>&lsaquo;</button><span data-role='separator'> </span><button class='btn btn-default' data-role='next'>&rsaquo;</button><button class='btn btn-default' data-role='end'>Stop</button></div> </nav></div > ",

			steps: [
				{
					element: ".add-page",
					content: "<?php echo __('Met deze knop voeg je een nieuwe pagina toe'); ?>",
					placement: "bottom",
					backdropPadding: "20"
				},
				{
					element: ".move-page",
					content: "<?php echo __('Bepaal de volgorde van je menu en pagina\'s door te slepen'); ?>",
					placement: "bottom"
				},
				{
					element: ".display-page",
					content: "<?php echo __('Bepaal hier als je menuitem of pagina online zichtbaar en vindbaar is.'); ?>",
					placement: "bottom"
				},
				{
					element: ".online-page",
					content: "<?php echo __('Bepaal hier als je menuitem of pagina zichtbaar is in het hoofmenu'); ?>",
					placement: "bottom"
				}
			]});

		$('.starttour').show();
		$(".starttour").click(function(){
			tour.init();
			tour.start();
			tour.restart();
			return false;
		});
	</script>

<?php endif; ?>

<!-- copy modal -->
<?php echo $this->Element('Sitekick/Navigations/copy'); ?>
<!-- ./copy modal -->

<script>
	$(document).ready(function() {

		// copy page
		$('.copy-page').click(function(e){
			e.preventDefault();

			// set title
			var title = $(this).closest('.dd-handle').find('.title').text().trim() + ' (kopie)';
			$('#copy-form #title').val(title);

			// set form link
			$('#copy-form').attr('action', $(this).attr('href'));

			// set modal
			$('#copyModal').modal();
		});

		var updateOutput = function(e)
		{
			var list   = e.length ? e : $(e.target),
				output = list.data('output');
			if (window.JSON) {
				var data = window.JSON.stringify(list.nestable('serialize'));
				$.ajax({
					type: "POST",
					data: { ranking: data },
					url: "navigations/ranking"
				});
			}
		};

		// activate Nestable for list 1
		$('#nestable').nestable({
			group: 1,
			maxDepth: <?php echo $settings['Pages']['menu_levels']; ?>,
			handleClass: 'move'
		}).on('change', updateOutput);

		<?php
		if( (!empty($settings['Pages']['overview_use_collapse']) && $settings['Pages']['overview_use_collapse'] == 1) &&
            (!empty($settings['Pages']['pages_default_collapsed']) && $settings['Pages']['pages_default_collapsed'] == 1) ){
		    echo "$('#nestable').nestable('collapseAll');";
        } ?>

		$(".switch").switchButton({
			label: false,
			style: 'small'
		});


		$(".btn_display").click(function() {

			var id = $(this).children("span").attr("data-input");
			if($(this).children("span").hasClass("off")) {
				var value = 0;
			}
			else {
				var value = 1;
			}
			$.ajax({
				type: "POST",
				url: "navigations/update",
				data: { "data[Navigations][id]" : id, "data[Navigations][display]" : value }
			});
			return false;
		});

		$(".btn_active").click(function() {
			var id = $(this).children("span").attr("data-input");
			if($(this).children("span").hasClass("off")) {
				var value = 0;
			}
			else {
				var value = 1;
			}
			$.ajax({
				type: "POST",
				url: "navigations/update",
				data: { "data[Navigations][id]" : id, "data[Navigations][active]" : value }
			});
			return false;
		});

		$(".action-delete").click(function() {

			var url = $(this).attr("href");
			item =  this;

			message = "<?php echo __('Weet je zeker dat je deze {0} wil verwijderen!', [strtolower($settings['Pages']['name_single'])]); ?>";

			parentItems = $(item).parent("div").parent("div").parent("li").find("li").length;
			if(parentItems > 0){
				message = "<?php echo __('Je staat op het punt een hoofd{0} te verwijderen. De pagina\’s die hier onder vallen worden niet verwijderd. Ze komen onderaan de paginalijst te staan en worden automatisch op inactief gezet.', [strtolower($settings['Pages']['name_single'])]); ?>";
			}

			sweetAlert({
					title: "",
					text: message,
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#2eb582",
					confirmButtonText: "Ja",
					cancelButtonText: "Nee",
					closeOnConfirm: false
				},
				function(isConfirm){
					if (isConfirm) {

						parent = $(item).parent("div").parent("div").parent("li");

						$(parent).find("li").each(function(){

							displayBtn = $(this).find(".btn_display");
							displayBtnState = displayBtn.find('span');
							if(!displayBtnState.hasClass("off")) {
								$(this).find(".btn_display").click();
							}

							$(this).appendTo("#nestable ol:first");
						});

					    $("#nestable").nestable('serialize');

					    parent.fadeOut(500);

					    $.ajax({
						    type: "POST",
						    url: url, 
							success: function(feedback) {

								if(feedback.length > 0) {
							
									sweetAlert({
										title: "",
										text: "<?php echo $settings["Pages"]["name_single"]; ?> <?php echo __('verwijderd.'); ?> " + feedback,
										type: "success",
										confirmButtonColor: "#2eb582",
										confirmButtonText: "<?php echo __('OK, sluiten'); ?>",
										closeOnConfirm: false
									});
								}	
								else {
									sweetAlert({
										title: "",
										text: "<?php echo $settings["Pages"]["name_single"]; ?> <?php echo __('verwijderd.'); ?>",
										type: "success",
										confirmButtonColor: "#2eb582",
										confirmButtonText: "<?php echo __('OK, sluiten'); ?>",
										closeOnConfirm: false,
										timer: 2500
									});								
								}
							}
					    });
					}
				});

			return false;
		});

	});
</script>