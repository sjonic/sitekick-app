<h1><?php echo __('Server informatie'); ?></h1>

<div class="green-table">

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th width="300">Info</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($info as $key => $val): ?>
					<tr>
						<td><?php echo $key; ?></td>
						<td><?php echo $val; ?></td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>
</div>


<br/>
<br/>
<br/>
<h1><?php echo __('Plugin informatie'); ?></h1>


<div class="green-table">

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
			<tr>
				<th width="300"><?php echo __('Plugin'); ?></th>
				<th><?php echo __('Versie'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php
            if( !empty($versions) ){
                foreach($versions as $packageName => $version): ?>
                    <tr>
                        <td><?php echo $packageName; ?></td>
                        <td><?php echo $version; ?></td>
                    </tr>
                <?php
                endforeach;
            }?>
			</tbody>
		</table>
	</div>
</div>




<br/>
<br/>
<br/>
<h1>
    SQL Patches
</h1>

<div class="row">
    <div class="col-sm-6">
        <?= $this->Html->link(__('Download SQL-dump'), ['action' => 'dump'], [
            'data-toggle' => 'tooltip',
            'title' => 'Download SQL-dump, plaats deze optioneel in de plugins/Klant/config/Schema map',
            'class' => 'btn btn-primary'
        ]); ?>
    </div>
    <div class="col-sm-6 text-right">
        <?= $this->Html->link(__('Nieuwe installatie uitvoeren'), '/install-sitekick', ['class' => '']); ?><br>
        <?= $this->Html->link(__('Alle mislukte opnieuw uitproberen'), '#', ['class' => 'btn-run-all-patches']); ?>
    </div>
</div>
<br/><br/>

<div class="green-table">

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
			<tr>
				<th width="300"><?php echo __('Patch'); ?></th>
				<th><?php echo __('Plugin'); ?></th>
				<th><?php echo __('Uitgevoerd'); ?></th>
			</tr>
			</thead>
			<tbody>

			<?php foreach($migrations as $migration){ ?>
			<tr>
				<td><?php echo $migration->file; ?></td>
				<td><?php echo ucfirst($migration->plugin); ?></td>
				<td>
                    <?php
                    if( $migration->executed == 1 ){
                        echo __('Ja');
                    } else {
                        echo __('Nee - ') . $this->Html->link(__('Opnieuw proberen'), '#', ['class' => 'btn-run-patch', 'data-id' => $migration->id]);
                    }
                    ?>
                </td>
			</tr>
			<?php } ?>

			</tbody>
		</table>
	</div>
</div>

<script>

    var goingToPatchIds = [];

    $(document).ready(function(){
        $('.btn-run-patch').on('click', function(e){
            e.preventDefault();
            setPatch(this);
            patchItems();
        });

        $('.btn-run-all-patches').on('click', function(e){
            e.preventDefault();
            $('.btn-run-patch').each(function(){
                setPatch(this);
            });
            patchItems();
        })
    });

    function setPatch( patchItem ){
        var $p = $(patchItem);
        $p.closest('td').html('<img src="<?= $this->Url->build('/') ?>img/sitekick/loading.svg" height="20" width="20" />');
        goingToPatchIds.push( $p.data('id') );
    }

    function patchItems()
    {
        if( goingToPatchIds.length === 0 ){
            alert('Er zijn geen migraties om te patchen');
            return false;
        }

        $("#patches-modal").remove();

        $.post("<?= $this->Url->build(['action' => 'runPatches']) ?>", {ids: goingToPatchIds}, function(response){
           $("body").append(response);
           $("#patches-modal").modal();

            $('#patches-modal').on('hide.bs.modal', function () {
                window.location.reload();
            })
        });
    }
</script>