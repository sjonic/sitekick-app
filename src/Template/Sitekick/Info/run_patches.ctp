<div id="patches-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo __('Patches resultaat'); ?></h4>
            </div>
            <div class="modal-body">
                <?php
                if( !empty($response) ){
                    foreach($response as $row){
                        ?>
                        <div class="btn-margin colborder">
                            <strong><?= ucfirst($row['plugin']) . ' // ' . $row['file'] ?></strong>
                            <div class="<?= (!empty($row['errors'])) ? 'text-error' : '' ?>">
                                <?= (!empty($row['patch_response'])) ? $row['patch_response'] : $row['error']; ?>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Sluiten en pagina verversen'); ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->