<?php
echo $this->Element('Sitekick/header');
echo $this->Html->css('sitekick/jquery.bootstrap-select.css');
echo $this->Html->css('/js/tinymce/skins/lightgray/skin.min.css');
?>
<style type="text/css">
    body {
        padding: 15px;
    }

    .input-group {
        width: 100%;
        display: flex;
        margin-bottom: 15px;
        align-items: center;
    }

    #internal-link-loading {
        padding-left: 30%;
        margin: 15px 0;
        height: 25px;
    }

    .input-group label {
        width: 30%;
        float: left;
        flex-grow: 0;
        flex-shrink: 0;
    }

    .input-group .form-control {
        width: 70%;
        flex-grow: 0;
        flex-shrink: 0;
    }

    .bootstrap-select .dropdown-menu li a span.text {
        max-width: 250px;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .bootstrap-select .bs-searchbox .form-control {
        width: 100%;
    }

    .mce-btn {
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    #open-file-browser {
        height: 42px;
        margin-left: -5px;
        width: 42px;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        outline: none;
    }
</style>
<div id="link-form">

    <div id="internal-link-loading">
        <?php
        echo $this->Html->image('sitekick/loading.svg', ['style' => 'height: 20px;', 'alt' => 'bezig...' ]);
        echo __('Bezig met interne links laden...');
        ?>
    </div>
    <?php
    echo '<div class="bootstrap-select-wrapper" style="display: none;" id="bootstrap-select-internal-link">' . $this->Form->control('internal_url', [
            'label' => __('Interne link'),
            'required',
            'title' => __('Zoek een interne link'),
            'data-live-search' => 'true',
            'options' => [],
        ]) . '</div>';
    ?>

    <div id="file-browser-list" class="input-group select">
        <label for="file-browser-select"><?php echo __('Link naar bestand') ?></label>
        <div id="file-browser-list-loading" style="width: 100%;">
            <?= $this->Html->image('sitekick/loading.svg', ['style' => 'height: 20px;', 'alt' => 'bezig...' ]); ?>
            Bezig met bestanden ophalen...
        </div>
        <div class="bootstrap-select-wrapper" id="bootstrap-select-file-browser" style="display: none; width: 100%;">
            <select name="file-browser-select" data-live-search="true" id="file-browser-select">
                <option value=""><?= __('Zoek een bestand') ?></option>
            </select>
        </div>
    </div>

    <div class="input-group">
        <label for="url"><?php echo __('URL') ?></label>
        <input type="text" name="url" id="url" required style="width: 100%"/>
    </div>

    <?php
    echo $this->Form->control('text', ['label' => __('Linktekst'), 'required']);
    echo $this->Form->control('target', ['label' => __('Doel'), 'options' => [
        '' => __('Geen'),
        '_blank' => __('Nieuw tabblad / venster')
    ]]);
    echo $this->Form->control('class', ['label' => __('Stijl'), 'options' => $classes]);

    echo $this->Html->script('sitekick/jquery.bootstrap-select.js');
    echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/i18n/defaults-' . $activeLanguage->locale . '.min.js');
    ?></div>


<script>
    $(document).ready(function () {

        var args = top.tinymce.activeEditor.windowManager.getParams();

        // format URL / email
        $('#url').on('change', function () {
            var v = $(this).val();
            if (v.indexOf('www') >= 0 && v.indexOf('http') < 0) {
                $(this).val('http://' + v);
            }
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if( re.test(v.toLowerCase()) && v.indexOf('mailto:') < 0 ){
                $(this).val('mailto:' + v);
            }

            $('#internal-url').selectpicker('val', $(this).val());
        });

        // open file browser

        $.getJSON('/sitekick/media/inlineLibrary/file.json?limit=1000', function(response){

            $.each(response, function(key, file){
                $('#file-browser-select').append('<option value="'+file.url+'">' + file.file + '</option>' );
            });

            $('#file-browser-list-loading').hide();
            $('#bootstrap-select-file-browser').show();
            $('#file-browser-select').selectpicker({
                dropdownAlignRight: 'right',
                liveSearchPlaceholder: '<?= __('Zoeken...'); ?>',
            }).on('change', function(){
                $('#url').val( $(this).val() );
            });

            $('#file-browser-select').selectpicker('val', $('#url').val());

        });

        if (typeof args.node != 'undefined' && args.node.nodeName == 'A') {
            $('#url').val($(args.node).attr('href'));
            $('#target').val($(args.node).attr('target'));
            $('#text').val($(args.node).text());

            // set correct class
            if( $('#class option[value="'+$(args.node).attr('class')+'"]').length ){
                $('#class').val($(args.node).attr('class'));
                $('#class option[value="'+$(args.node).attr('class')+'"]').prop('selected', true);
            } else {
                $('#class').append('<option value="'+$(args.node).attr('class')+'" selected>'+$(args.node).attr('class')+'</option>');
            }

        } else if (typeof args.selectedText != 'undefined') {
            $('#text').val(args.selectedText);
        }

        // internal links
        $.get('/sitemap_index/editor.json', function (response) {

            $('#internal-link-loading').remove();

            var inputHtml = '<option value=""><?= __('Kies een interne pagina') ?></option>';
            var inputHtml = '';
            if (response.length) {
                $(response).each(function (key, group) {
                    var groupHtml = '<optgroup label="' + group.title + '">';
                    if (group.menu.length) {
                        $(group.menu).each(function (key, menuItem) {
                            groupHtml += '<option value="' + menuItem.value + '">' + menuItem.title + '</option>';
                        });
                    }
                    group += '</optgroup>';
                    inputHtml += groupHtml;
                });
                $('#internal-url').html(inputHtml);
            }

            $('#internal-url').selectpicker({
                dropdownAlignRight: 'right',
                liveSearchPlaceholder: '<?= __('Zoeken...'); ?>',
            });
            $('#internal-url').selectpicker('val', $('#url').val());


            $('#bootstrap-select-internal-link').show();
            $('#internal-url').on('change', function () {
                if( $('#internal-url').selectpicker('val') != null ){
                    $('#url').val($(this).val());
                    if ($('#text').val() == '') {
                        $('#text').val($(this).find('option:selected').text());
                    }
                }

            });
        });

    });
</script>
