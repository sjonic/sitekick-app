<?php if (isset($settings["Seo"]["dashboard"])) {
    if ($settings["Seo"]["dashboard"]) {
        $this->Html->script('Seo.overlay', array('block' => 'scriptBottom'));
        $this->Html->css('Seo.sitekick_dashboard', array('block' => 'scriptBottom'));
    }
} ?>
    <div class="dashboard">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo __('Direct naar'); ?></h1>
                <div class="row placeholders placeholders-shortlinks">
                    <?php
                    foreach ($plugins as $plugin) {
                        if ($this->isAllowed->plugin($plugin) && isset($settings[$plugin]['dashboard']) && $settings[$plugin]['dashboard'] == 1) {
                            if ($this->elementExists($plugin . '.Sitekick/dashboard')) {
                                echo $this->element($plugin . '.Sitekick/dashboard');
                            }
                        }
                    }

                    if (!empty($itemTypes)) {
                        foreach ($itemTypes as $plugin) {
                            if ( ($plugin->can_add && !$plugin->admin && $this->isAllowed->plugin($plugin->content_type)) || ($plugin->can_add && $authUser['superadmin'] == 1)) {
                                echo $this->element('Items.Sitekick/dashboard', ['plugin' => $plugin]);
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php if (!empty($settings["Base"]["analytics"]) ): ?>

            <div class="row" style="margin-top: 30px;">
                <div class="col-md-12">
                    <h2>
                        <span class="icon-seo-2"></span>
                        <?php echo __('Statistieken'); ?>

                        <small><i class="icon-info"
                                  data-toggle="toggle"
                                  data-placement="top"
                                  data-trigger="hover"
                                  title="<?php echo __('Wat is dit?'); ?>"
                                  data-content="<?php echo __('Sinds de ingang van de AVG-wetgeving in mei 2018 is deze functie niet meer gekoppeld aan Google Analytics. Voor meer statistieken verwijzen we naar je Google Analaytics-account.'); ?>"
                            ></i></small>
                    </h2>

                    <div id="load-statistics">
                        <?php echo $this->Html->image('sitekick/loading.svg', ['alt' => __('loading'), 'style' => 'width: 30px; height: 30px;']); ?>
                    </div>
                </div>
            </div>
            <script>
                $.ajax({
                    url: '<?= $this->Url->build(['prefix' => 'sitekick', 'controller' => 'Seo', 'action' => 'dashboard']) ?>',
                    type: 'get',
                    success: function(response){
                        $('#load-statistics').replaceWith(response);
                        $('.datepicker').datepicker({language: 'nl', format: 'dd-mm-yyyy', autoclose: true, weekStart: 1});
                    },
                    error: function(error){
                        $('#load-statistics').html('<?= __d('Seo', 'Er zijn geen statistieken gevonden.') ?>');
                    }
                });
            </script>

        <?php endif; ?>

        <?php
        foreach ($plugins as $plugin) {
            if ($this->isAllowed->plugin($plugin) && isset($settings[$plugin]['dashboard']) && $settings[$plugin]['dashboard'] == 1) {
                try {
                    echo $this->cell($plugin . '.Sitekick');
                } catch( Cake\View\Exception\MissingCellException $error ){}
            }
        }
        ?>

    </div>


<?php if (!$this->request->isMobile()): ?>

    <?php
    echo $this->Html->css('sitekick/bootstrap-tour');
    echo $this->Html->script('sitekick/bootstrap-tour');
    ?>
    <script>



        var tour = new Tour({

            template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='prev'>&lsaquo;</button><span data-role='separator'> </span><button class='btn btn-default' data-role='next'>&rsaquo;</button><button class='btn btn-default' data-role='end'>Stop</button></div> </nav></div > ",

            steps: [
                {
                    element: "#siteadmin",
                    content: "<?php echo __('Terug naar Sitekick. Het kan altijd via deze knop.'); ?>",
                    placement: "bottom",
                    backdropPadding: "20"
                },
                {
                    element: "#sitefront",
                    content: "<?php echo __('Klik hier om naar de voorkant van je website te gaan. Zo zie je wat bezoekers ook zien.'); ?>",
                    placement: "bottom"
                },

                {
                    element: "#newitems",
                    content: "<?php echo __('Wil je snel een nieuwe pagina of bericht toevoegen? Dat kan met dit menu.'); ?>",
                    placement: "bottom"
                },
                {
                    element: "#profile",
                    content: "<?php echo __('Je e-mailadres, foto of wachtwoord wijzigen? Dat kan hier. Doe je best.'); ?>",
                    placement: "bottom"
                },
                {
                    element: ".navbar-toggle",
                    content: "<?php echo __('Meer ruimte om te bewerken? Klap hier het menu in en leef je uit.'); ?>",
                    placement: "right"
                },
                {
                    element: ".sidebar",
                    content: "<?php echo __('Hier vind je alles wat je nodig hebt  binnen Sitekick. Lekker overzichtelijk.'); ?>",
                    placement: "right"
                }
            ]
        });

        $('.starttour').show();
        $(".starttour").click(function () {
            tour.init();
            tour.start();
            tour.restart();
            return false;
        });
    </script>

<?php endif;

