

<div class="row">
	<div class="col-xs-12">
		<h1>Pacman</h1>
	</div>
</div>

<div class="row placeholders">

	<div class="col-xs-5">


		<div id="pacman"></div>
		<?php echo $this->Html->script('/pacman/pacmanSK'); ?>
		<?php echo $this->Html->script('/pacman/modernizr-1.5.min'); ?>

		<script>

				var el = document.getElementById("pacman");

				if (Modernizr.canvas && Modernizr.localstorage &&
					Modernizr.audio && (Modernizr.audio.ogg || Modernizr.audio.mp3)) {
					window.setTimeout(function () {
						PACMAN.init(el, "/pacman/");
					}, 0);
				} else {
					el.innerHTML = "Sorry, te oude browser p<br /><small>" +
					"(firefox 3.6+, Chrome 4+, Opera 10+ and Safari 4+)</small>";
				}

		</script>


	</div>
	<div class="col-xs-5">
		<p>
			Een korte uitleg<br/>

			<ul class="list-group">
				<li class="list-group-item">
					<span class="btn btn-primary btn-small">N</span> om een spel te starten
				</li>
				<li class="list-group-item">
					<span class="btn btn-primary btn-small">P</span> Even toe aan een break? Druk dan op de toets
				</li>
				<li class="list-group-item">
					<div class="btn btn-primary btn-small">&uarr;</div>
					<div class="btn btn-primary btn-small">&darr;</div>
					<div class="btn btn-primary btn-small">&larr;</div>
					<div class="btn btn-primary btn-small">&rarr;</div>
					Met de pijltjes toetsen beweeg je
				</li>
			</ul>
		</p>
	</div>
</div>
