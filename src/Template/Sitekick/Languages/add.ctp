<h1>Taal toevoegen</h1>

<?php
echo $this->Form->create($language);
echo '<div class="formblock formblock-compact row">';
echo $this->Form->input('label', ['label' => __('Taalnaam')]);
echo $this->Form->input('own_label', ['label' => __('Taalnaam (in het Nederlands)')]);
echo $this->Form->input('abbreviation', ['label' => __('Afkorting (2 letters)')]);
echo $this->Form->input('locale', ['label' => __('Locale (3 letters)')]);
echo $this->Form->input('active', [
	'type' => 'checkbox',
	'label' => [
		'text' => 'Actief',
		'style' => 'float: left; margin-left: 0; margin-right: 20px;'
	],
	'class' => 'switch',
	'value'=> 1,
	'checked'=> isset($language->active) && $language->active == 0 ? false : true
]);
echo '</div>';
echo $this->Form->submit('Opslaan', ['class'=> 'btn btn-primary']);
echo $this->Form->end();
?>

<script>
	$(document).ready(function(){
		$(".switch").switchButton();
	});
</script>