<h1><?php echo __('Talen'); ?></h1>

<div class="green-table">

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th><?php echo __('Taal'); ?></th>
					<th><?php echo __('Taal in eigen taal'); ?></th>
					<th><?php echo __('Afkorting'); ?></th>
					<th><?php echo __('Locale'); ?></th>
					<th><?php echo __('Actief'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($languagesOverview as $language):
				?>
				<tr data-id="<?php echo $language->id; ?>">
					<td><?php echo $language->label;?></td>
					<td><?php echo $language->own_label;?></td>
					<td><?php echo $language->abbreviation;?></td>
					<td><?php echo $language->locale;?></td>
					<td><?php echo $this->Form->input('active', [
							'value' => 1,
							'data-id' => $language->id,
							'class' => 'switch toggle_active',
							'checked'=> isset($language->active) && $language->active == 0 ? false : true
						]);?></td>
					<td>
						<?php echo $this->Html->link('<i class="icon-pencil-2 action-button-small action-edit"></i>', ['action' => 'edit', $language->id], ['escape' => false]); ?>
						 &nbsp;&nbsp;
						<?php echo $this->Form->postLink('<i class="icon-delete action-button-small action-edit"></i>', ['action' => 'delete', $language->id], ['escape' => false, 'confirm' => __('Weet je zeker dat je <strong>{0}</strong> wilt verwijderen?', $language->own_label)]); ?>
					</td>
				</tr>
				<?php
			endforeach;
			?>
			</tbody>
		</table>
	</div>
</div>

<?php echo $this->html->link(__('Nieuwe taal toevoegen'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>


<script>
	$(document).ready(function(){
		$(".switch").switchButton({
			'label' : false,
			'onclick' : function(element){
				var value 	= 1;
				var id 		= $(element).closest('tr').data('id');
				if( $(element).find('.off').length > 0 ){
					value = 0;
				}

				$.ajax({
					method: 'post',
					url: '<?php echo $this->Url->build(['action'=>'edit']); ?>/' + id,
					data: { active: value }
				});
			}
		});

	});
</script>