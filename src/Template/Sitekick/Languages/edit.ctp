<h1><?php echo $language->own_label; ?> bewerken</h1>

<?php
echo $this->Form->create($language);
echo '<div class="formblock formblock-compact row">';
echo $this->Form->input('label', ['label' => __('Taalnaam')]);
echo $this->Form->input('own_label', ['label' => __('Taalnaam (in het Nederlands)')]);
echo $this->Form->input('abbreviation', ['label' => __('Afkorting (2 letters)')]);
echo $this->Form->input('locale', ['label' => __('Locale (3 letters)')]);
echo $this->Form->input('active', [
	'type' => 'checkbox',
	'label' => __('Actief'),
	'class' => 'switch',
	'value'=> 1,
	'checked'=> isset($language->active) && $language->active == 0 ? false : true
]);
echo '</div>';
echo $this->Form->submit(__('Opslaan'), ['class'=> 'btn btn-primary']);
echo '&nbsp; ' . $this->Html->link(__('Annuleren'), ['action' => 'index']);
echo $this->Form->end();
?>

<script>
	$(document).ready(function(){
		$(".switch").switchButton();
	});
</script>