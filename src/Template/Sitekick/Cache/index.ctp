
<h1><?php echo __('Cache informatie'); ?></h1>

<?php
echo $this->Flash->render();
?>
<div class="row" style="margin-bottom: 30px;">
    <div class="col-md-6">
        <?php
        echo $this->Form->button(__('Cache opnieuw opbouwen'), [
                'type' => 'button',
                'class' => 'btn btn-primary',
                'id' => 'btn-rebuild-cache',
                'data-url' => $this->Url->build(['controller' => 'Cache', 'action' => 'rebuild'])
        ]);
        ?>
        <div id="cache-loader" style="display: none; margin-top: 5px;"><img src="/img/sitekick/loading.svg" style="height: 18px" /> <?= __('Bezig met opnieuw opbouwen. Sluit dit venster niet, het kan even duren.') ?></div>
        <?php
        if( !empty($lastBuildLog) && !empty($files) ){
            echo '<div style="margin-top: 5px;">' . __('Laatste keer opgebouwd op: {0} om {1}', [
                    $lastBuildLog->format('d-m-Y'),
                    $lastBuildLog->format('H:i'),
            ]) . '</div>';
        }
        ?>

    </div>
    <div class="col-md-6 text-right">
        <?php echo $this->Html->link(__('Alle cache legen'),
            ['controller' => 'Cache', 'action' => 'clear']
            ,['escape' => false, 'class' => 'btn btn-primary']); ?>
    </div>

</div>

<div class="green-table">

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
			<tr>
				<th><?php echo __('Bestand'); ?></th>
				<th width="50"></th>
			</tr>
			</thead>
			<tbody>
			<?php
            if( !empty($files) ){
                foreach($files as $file){
                    echo "<tr>
                        <td>" . $file . "</td>
                        <td>".$this->Html->link('<span class="icon-delete"></span>', '/sitekick/cache/delete/'.$file, ['title' => $file . " verwijderen", 'escape'=>false, 'class'=>'action-button-big action-delete can-delete', 'id' => $file])."</td>
                    </tr>";
                }
            } else {
                echo '<tr><td colspan="2">'.__('Er zijn nog geen cache-bestanden gevonden.').'</td></tr>';
            }

			?>
			</tbody>
		</table>
	</div>
</div>

<script>
    $('#btn-rebuild-cache').on('click', function(){
        $(this).prop('disabled', true);
        $('#cache-loader').fadeIn();
        $.get($(this).data('url'), function(response){
            window.location.reload();
        }).fail(function(){
            alert('<?= __('Er is iets mis gegaan tijdens het opbouwen van de cache. Probeer het later nog eens.')?>');
        });
    });
</script>