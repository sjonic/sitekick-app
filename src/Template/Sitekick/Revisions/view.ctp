<?php $this->Html->css('sitekick/revisions', array('block' => true)); ?>

<?php $data = unserialize($revision->postdata); ?>
<div class="revisions-wrapper">
    <div class="container">

    <div class="row">
        <div class="col-xs-12">
            <h2>
                <span class="icon-seo-2"></span>
                <?php echo __('Versiebeheer'); ?>
            </h2>
        </div>
    </div>

    <div class="row details">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php echo __('Aangepast door'); ?>:<br>
            <span><?php echo $revision->user['firstname']; ?> <?php echo $revision->user['lastname']; ?></span>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <?php echo __('Aangepast op'); ?>:<br>
            <span><?php echo $revision->date; ?></span>
        </div>
        <?php if( strtolower($revision->plugin) == 'pages' ) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <?php echo __('Menutitel'); ?>:<br>
                <span><?php echo $data['Navigations']['name']; ?></span>
            </div>
        <?php } ?>
        <?php if( strtolower($revision->plugin) == 'pages' ) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <?php echo __('URL'); ?>:<br>
                <span>/<?php echo $data['Navigations']['slug']; ?></span>
            </div>
        <?php } ?>
    </div>
</div>
    <div id="preview" class="container">
    <?php if( strtolower($revision->plugin) == 'pages' ) { ?>
        <?php if( isset($data['Navigations']['headers']) && isset($data['Navigations']['headers'][0]) && !empty($data['Navigations']['headers'][0]['id']) ) { ?>
            <div class="row block">
                <div class="col col-xs-12">
                    <div class="block _header clearfix">
                        <div class="content">
                            <?php echo $data['Navigations']['headers'][0]['content']; ?>
                        </div>

                        <?php
                        $options = [
                            'id'     => $data['Navigations']['headers'][0]['header_id'],
                            'device' => 'desktop'
                        ];
                        echo $this->Cell('Headers.Header::getById', [ $options ]); ?>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php
        $content = json_decode($data['content']);
        foreach( $content->template as $row ) {

            if( !empty($row->title) ) {
                echo '<div class="row block block-heading">';
                    echo '<div class="col col-xs-12">';
                        echo '<div class="heading">';
                            echo '<h2>' . $row->title . '</h2>';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
            }

            echo '<div class="row block">';
            if( $row->template == 1 ) {
                echo '<div class="col col-xs-12">';
                echo '<div class="block">';
                echo reset(reset($row->cols[0])->items)->value;
                echo '</div>';
                echo '</div>';
            }
            else {

                foreach( $row->cols as $col ) {

                    echo '<div class="col col-xs-' . reset($col)->cols . '">';

                    echo '<div class="block">';

                    foreach( reset($col)->items as $item ) {

                        if( $item->type == "text" ) {
                            echo $item->value;
                        }
                        else {
                            foreach( $item->value as $image ) {
                                echo $this->Html->image($image, [ 'class' => count(reset(reset($col)->items)->value) > 1 ? 'thumb' : '' ]);
                            }
                        }

                    }

                    echo '</div>';
                    echo '</div>';

                }

            }
            echo '</div>';
        }
        ?>

    <?php } elseif( strtolower($revision->plugin) == 'news' ) { ?>

        <h2><?php echo __('Introtekst'); ?></h2>

        <div class="row block">
            <div class="col col-xs-3">
                <?php if( isset($data['_files']) && isset($data['_files'][0]) && !empty($data['_files'][0]->cropped) ) { ?>
                    <?php echo $this->Html->image($data['_files'][0]->cropped); ?>
                <?php } ?>
            </div>
            <div class="col col-xs-9">
                <div class="block">
                    <h2><?php echo $data['title']; ?></h2>
                    <?php echo $data['intro']; ?>
                </div>
            </div>
        </div>

        <h2><?php echo __('Inhoud'); ?></h2>
        <div class="row block">
            <div class="col col-sm-8">
                <div class="block">
                    <h1><?php echo $data['title']; ?></h1>
                    <?php echo $data['content']; ?>
                </div>
            </div>
            <div class="col col-sm-4">
                <?php if( isset($data['_files']) && isset($data['_files'][0]) ) { ?>
                    <?php if( count($data['_files']) == 1 ) { ?>
                        <?php echo $this->Html->image($data['_files'][0]->cropped); ?>
                    <?php } else { ?>
                        <div class="thumbnails">
                            <?php foreach( $data['_files'] as $file ) { ?>
                                <?php echo $this->Html->image($file->cropped); ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>

<?php } else if( strtolower($revision->plugin) == 'items' ) {
        echo $this->Cell('Items.Revision', [$revision, $data]);
    } else { ?>

    <h2>Data</h2>
    <?php
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    ?>
<?php } ?>
    </div>
</div>

<div  class="bottom-fixed">
    <button class="execute btn btn-primary add-page"><span class="icon-plus"></span> <?= __('Gebruik deze versie'); ?></button>
    <div class="ajax-loading"></div>
    <?php echo $this->Html->link( __('Annuleren'), $this->request->referer() ); ?>
</div>

<?php
$data['revision'] = 1;
if( $revision->method == 'GET' ) {
    $this->Html->scriptBlock('$(document).ready(function() {
        $("body").on("click", ".execute", function() {

            $(".ajax-loading").addClass("load");
            $(".btn-primary").prop("disabled", true);
            $.ajax({
                type: "get",
                url: "' . $this->Url->build('/').$revision->url . '",
                data: ' . json_encode($data) . ',
                success: function(msg) {
                    window.location.href = "' . $this->Url->build('/') . '" + msg;
                }
            });
            return false;
        });
    })', array('block' => 'scriptBottom'));
}
else {
    $this->Html->scriptBlock('$(document).ready(function() {
        $("body").on("click", ".execute", function() {

            $(".ajax-loading").addClass("load");
            $(".btn-primary").prop("disabled", true);

            $.ajax({
                type: "post",
                url: "' . $this->Url->build('/') . $revision->url . '",
                data: ' . json_encode($data) . ',
                success: function(msg) {
                    window.location.href = "' . $this->Url->build('/') . '" + msg;
                }    
            });
            return false;
        });
    })', array('block' => 'scriptBottom'));
}
?>