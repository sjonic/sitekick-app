<div class="newsitem">
	<h2><?php echo $this->Form->input('previewtitle', ['label'=> false, 'placeholder' => 'Voorbeeld titel','class' => 'previewtitle'],[ 'escape'=>false]); ?></h2>
	<div class="itemimage">
		<?php echo $this->Html->Image('sitekick/template/empty.jpg',['class' => 'preview_thumb']); ?>
		<button class="next_step editlink btn btn-primary btn-small hidden-xs" data-target="step-2">Wijzig afbeelding</button>
	</div>
	
	<div class="contentarea"><?php echo $this->Form->input('intro', ['label'=> false, 'class' => 'redactorbasic previewtext', 'maxlength' => 180],[ 'escape'=>false]); ?></div>

	<a class="button green-button">Lees meer</a>
</div>

<style>
	#news #news_preview {
		font-family: "canada-type-gibson",sans-serif;
		color: #66666c;
		font-weight: 300;
		font-style: normal;
	}
	#news #news_preview .newsitem {
		max-width: 350px;
		margin: 0 auto;
		position: relative;
	}
	#news #news_preview h2 {
		margin: 0;
		padding: 0;
	}
	#news #news_preview #previewtitle {
		color: #ff4045;
		font-size: 32px;
		font-weight: bold;
		font-family: "canada-type-gibson",sans-serif;
		line-height: 38px;
		height: 38px;
		margin: 0 0 15px 0;
	}
	#news #news_preview .itemimage {
		text-align: center;
		margin: 0 0 15px 0;
		background: #f1f1f1;
		position: relative;
	}
	#news #news_preview .itemimage .editlink {
		position: absolute;
		bottom: 10px; right: 10px;
	}
	#news #news_preview .preview_thumb {
		max-height: 200px;
		width: auto;
		max-width: none;
		margin: 0;
	}
	#news #news_preview p {
		color: #66666c;
		font-size: 22px;
		font-family: "canada-type-gibson",sans-serif;
		font-weight: 300;
		font-style: normal;
		line-height: 1.42857143;
		margin-bottom: 16px;
	}
	#news #news_preview .button {
		font-size: 20px;
		margin-top: 0;
		padding: 6px 22px;
		display: inline-block;
		border-color: #2ea871;
		color: #2ea871;
		border: 3px solid;
		border-radius: 5px;
		font-weight: 400;
		font-style: normal;
		transition: all .4s ease-in-out;
		background-color: transparent;
	}
	#news #news_preview .button:hover {
		background: #2ea871;
		color: #fff;
		text-decoration: none;
		border: 3px solid #2ea871;
	}
	#news #news_preview .redactor-editor {
		padding: 0;
		border: 0;
	}
</style>