<section class="container">
    <h1><?php echo __('Sitemap'); ?></h1>

    <?php if( !empty($sitemap) ){
        foreach($sitemap as $plugin=>$urls){
            echo '<h2>' . $plugin . '</h2>';

            foreach($urls as $urlGroup){
                foreach($urlGroup as $linkData){
                    echo $this->Html->link( $linkData['title'], $linkData['loc'] ) . '<br/>';
                }
            }
        }
    } ?>
</section>