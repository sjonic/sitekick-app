
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php foreach($sitemap as $u){ ?>
        <url>
            <loc><?= $u['loc']; ?></loc>
        </url>
    <?php } ?>
</urlset>