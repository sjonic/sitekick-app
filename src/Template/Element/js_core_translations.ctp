<script>
    var translation_core_1 = "<?php echo __('Weet je zeker dat je dit item wilt verwijderen?'); ?>";
    var translation_core_2 = "<?php echo __('Op dit moment is <strong>{USER_FIRSTNAME} {USER_LASTNAME}</strong> deze pagina aan het bewerken. Om er voor te zorgen dat jullie elkaars werk niet overschrijven, kan je deze pagina op dit moment niet bewerken. Wacht tot <strong>{USER_FIRSTNAME} {USER_LASTNAME}</strong> klaar is of tot de pagina automatisch wordt vrijgegeven over <strong>{RELEASED_TIME}</strong>'); ?>";
    var translation_core_3 = "<?php echo __('OK, sluiten'); ?>";
    var translation_core_4 = "<?php echo __('Sla je wijzigingen eerst op om een nieuw element te kunnen bewerken'); ?>";
    var translation_core_5 = "<?php echo __('Sla je wijzigingen eerst op voordat je de pagina kunt verlaten'); ?>";
    var translation_core_6 = "<?php echo __('Klik om te bewerken...'); ?>";
    var translation_core_7 = "<?php echo __('De wijzigingen zijn opgeslagen'); ?>";
    var translation_core_8 = "<?php echo __('Weet je zeker dat je de wijzigingen ongedaan wilt maken?'); ?>";
    var translation_core_9 = "<?php echo __('Ja'); ?>";
    var translation_core_10 = "<?php echo __('Nee'); ?>";
    var translation_core_11 = "<?php echo __('Klik om te bewerken...'); ?>";
    var translation_core_12 = "<?php echo __('Hier worden items uitgelezen'); ?>";
    var translation_core_13 = "<?php echo __('Nog {COUNTER} tekens'); ?>";
    var translation_core_14 = "<?php echo __('Je hebt wijzigingen gedaan die nog niet zijn opgeslagen, weet je zeker dat je verder wil gaan?'); ?>";
    var translation_core_15 = "<?php echo __('Geen resultaten voor {KEYWORD}'); ?>";

    // redactor
    var translation_core_16 = "<?php echo __('Bezig met ophalen...') ?>";
    var translation_core_17 = "<?php echo __('- of plaats zelf een afbeelding -') ?>";
    var translation_core_18 = "<?php echo __('Afbeelding kiezen') ?>";
    var translation_core_19 = "<?php echo __('annuleren') ?>";
    var translation_core_20 = "<?php echo __('Afbeelding-locatie (url)') ?>";
    var translation_core_21 = "<?php echo __('of upload een nieuw bestand') ?>";
    var translation_core_22 = "<?php echo __('Bestand kiezen') ?>";
    var translation_core_23 = "<?php echo __('Annuleren') ?>";
    var translation_core_24 = "<?php echo __('Naam (optioneel)') ?>";
    var translation_core_25 = "<?php echo __('Upload een nieuw bestand') ?>";
    var translation_core_26 = "<?php echo __('Bestandslocatie (url)') ?>";
    var translation_core_27 = "<?php echo __('Mediabibliotheek') ?>";
    var translation_core_28 = "<?php echo __('Download invoegen') ?>";

    // tinymce
    var SITEKICK_LOAD_MESSAGE = "<?php echo __('In onderstaand blok zitten Sitekick-elementen die nodig zijn om speciale content in te laden op jouw website. Neem contact op met NOBEARS om de inhoud van dit blok te wijzigen.') ?>";
    var translation_core_29  = "<?php echo __('Ok, sluiten') ?>";
</script>