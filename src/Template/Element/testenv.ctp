<?php if(isset($settings['Base']['test_environment']) && $settings['Base']['test_environment'] == 1 && !isset($isAuthorized)) :?>
	<div class="test_environment" style="position: fixed; top:0; height:40px; color:#fff; background-color:#EA0000; width:100%; text-align: center; line-height: 40px; font-weight: bold; font-size: 15px; font-family: Open Sans, sans-serif">
		LET OP! Dit is een testomgeving.
	</div>
	<style>
		body{
			margin-top:40px;
		}
	</style>
<?php endif;?>