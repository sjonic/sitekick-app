<script>
    $(document).ready(function () {

        sweetAlert({
            title: "",
            text: "<?php echo $message; ?>",
            type: "success",
			<?php if(!isset($params['timer'])) { ?>
            timer: 2500,
			<?php } ?>
            confirmButtonColor: "#2eb582",
            confirmButtonText: "<?php echo __('OK, sluiten'); ?>"
        });
    });
</script>