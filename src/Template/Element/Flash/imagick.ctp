<div class="imagick-error alert-bar alert-error">
    <span class="icon icon-cross"></span> <?php echo __('Op dit moment is "Imagick" nog niet geïnstalleerd op de server. Neem contact op met de systeembeheerder om deze fout te verhelpen.'); ?>
</div>