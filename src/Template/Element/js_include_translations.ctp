<?php
if( !empty($this->request->params['plugin']) && $this->request->params['plugin'] != 'Media' ) {
    if( $this->elementExists($this->request->params['plugin'].'.js_translations') ) {
        echo $this->element($this->request->params['plugin'].'.js_translations');
    }
}
if( $this->elementExists('Media.js_translations') ) {
    echo $this->element('Media.js_translations');
}
if( $this->elementExists('js_core_translations') ) {
    echo $this->element('js_core_translations');
}
?>