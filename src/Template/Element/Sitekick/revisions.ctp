<div class="heading">
    <h2><span class="icon-paper"></span> <?php echo __('Versiebeheer'); ?></h2>
</div>

<div class="formblock" id="revisions">

    <?php if( count($revisions) > 0 ) { ?>
    <div style="max-height: 300px; overflow: auto">
        <table id="revisions-table">
            <tr>
                <th><?php echo __('Aangepast op'); ?></th>
                <th><?php echo __('Aangepast door'); ?></th>
                <th></th>
            </tr>
            <?php $i = 0; foreach( $revisions as $revision ) { ?>
            <tr <?php if( $i == 0 ) echo 'class="active"'; ?>>
                <td><?php echo $this->Time->format($revision['date'], 'dd-MM-yyyy HH:mm', null, 'Europe/Amsterdam'); ?></td>
                <td><?php echo $revision['user']['firstname']; ?> <?php echo $revision['user']['lastname']; ?></td>
                <td>
                    <?php if( $i > 0 ) {
                        echo $this->Html->link('Bekijk voorbeeld', ['plugin' => false, 'controller' => 'Revisions', 'action' => 'view', $revision['id']]);
                    } else {
                        echo '<strong>Nu actief</strong>';
                    }
                    ?>
                </td>
            </tr>
            <?php $i++; } ?>
        </table>
    </div>
    <?php } else { ?>

        <p><?php echo __('Er zijn op dit moment nog geen oudere versies'); ?></p>

    <?php } ?>
</div>