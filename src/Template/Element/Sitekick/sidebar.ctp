<div class="col-sm-3 col-md-2 sidebar"
    <?php if (isset($nosidebar) && $nosidebar == 1): ?>style="left: -640px;"<?php endif; ?>>

    <?php if( isset($settings['Base']['google_translate'] ) && $settings['Base']['google_translate'] == 1 ) { ?>
        <div class="google-translate">
            <div id="google_translate_element"></div>
            <script type="text/javascript">
            function googleTranslateElementInit() {
                // do not translate textareas due to Redactor
                $('.input-group.textarea, .redactor-box, .tokenfield, #seo_settings .input-group *:not(label)').addClass('notranslate');
                new google.translate.TranslateElement({pageLanguage: 'nl', includedLanguages: '<?php echo ( isset($settings['Base']['google_translate_options']) && !empty($settings['Base']['google_translate_options']) ) ? $settings['Base']['google_translate_options'] : 'en'; ?>', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
            }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
    <?php } ?>

    <ul class="nav nav-sidebar">
        <?php
        $class = '';
        if ($this->request->url == "sitekick") {
            $class = 'class="active"';
        }
        ?>

        <li <?php echo $class; ?>>
            <?php echo $this->Html->link('<span class="icon-home"></span>' . __('Start'), '/sitekick', ['escape' => false]); ?>
        </li>

        <?php foreach ($plugins as $plugin): ?>

            <?php
            $class = '';

            if (str_replace("sitekick/", "", $this->request->url) == strtolower($plugin)) {
                $class = 'class="active"';
            }

            if (!empty($settings[$plugin]['slug'])) {
                if (strpos($this->request->url, strtolower($settings[$plugin]['slug']))) {
                    $class = 'class="active"';
                }
            }
            ?>

            <?php
            if ($this->isAllowed->plugin($plugin) && isset($settings[$plugin]['menu']) && $settings[$plugin]['menu'] == "top" && (isset($settings[$plugin]['in_menu']) && $settings[$plugin]['in_menu'] != 0)): ?>

                <li <?php echo $class; ?>>
                    <?php
                    $slug = '/sitekick/' . strtolower($plugin);
                    if (!empty($settings[$plugin]['slug'])) {
                        $slug = '/sitekick/' . $settings[$plugin]['slug'];
                    }

                    $counter = '';
                    if (!empty($settings[$plugin]['menu_counter'])) {
                        $counter = '<i class="menu_counter badge" id="counter_' . strtolower($plugin) . '"></i>';
                    }

                    $after = '';
                    if (isset($settings[$plugin]['submenu']) && !empty($settings[$plugin]['submenu'])) {
                        $after = '<span id="submenu_' . strtolower($plugin) . '" class="opensub icon-dropdown"></span>';
                    }

                    ?>

                    <?php
                    if (!isset($settings[$plugin]['use_types'])) {
                        if (isset($settings[$plugin]['submenu'])) {
                            echo $this->Html->link('<span class="' . $settings[$plugin]['icon'] . '"></span>' . $settings[$plugin]['name'] . $counter . $after, 'javascript:;', ['escape' => false, 'onclick' => "$(this).closest('li').find('ul').toggle()"]);
                        } else {
                            echo $this->Html->link('<span class="' . $settings[$plugin]['icon'] . '"></span>' . ucfirst($settings[$plugin]['name']) . $counter . $after, $slug, ['escape' => false]);
                        }
                    }
                    ?>

                    <!-- dropdown menu (menu = top) -->
                    <?php if (isset($settings[$plugin]['submenu']) && !empty($settings[$plugin]['in_submenu'])): ?>
                        <ul class="sub submenu_<?php echo strtolower($plugin); ?>">
                            <?php $submenuArray = explode(",", $settings[$plugin]['in_submenu']);
                            foreach ($submenuArray as $submenuItem) { ?>
                                <li>
                                    <?php
                                    $subItem = explode("#", $submenuItem);
                                    echo $this->Html->link(
                                        __(ucfirst($subItem[1])),
                                        '/sitekick/' . $settings[$plugin]['slug'] . '/' . strtolower(str_replace(' ', '', $subItem[0]))
                                    );
                                    ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php endif; ?>
                    <!-- end dropdown menu -->


                    <?php if (isset($settings[$plugin]['submenu']) && isset($settings[$plugin]['slug'])): ?>
                        <ul class="sub submenu_<?php echo strtolower($plugin); ?>">
                            <?php
                            $submenuArray = explode(",", $settings[$plugin]['submenu']);
                            foreach ($submenuArray as $submenuItem) {

                                $submenuItemArray = explode("#", $submenuItem);
                                if (isset($submenuItemArray[1]) && isset($submenuItemArray[0])) {
                                    echo '<li><a href="/sitekick/' . $settings[$plugin]['slug'] . '/' . $submenuItemArray[1] . '">' . $submenuItemArray[0] . '</a></li>';
                                }
                            }
                            ?>
                        </ul>
                    <?php endif; ?>
                </li>


            <?php endif; ?>

            <!-- items plugin -->
            <?php
            if (isset($settings[$plugin]['use_types']) && isset($settings[$plugin]['menu']) && $settings[$plugin]['menu'] == "top" && (isset($settings[$plugin]['in_menu']) && $settings[$plugin]['in_menu'] != 0)):

                foreach ($types as $type) {
                    if ($this->isAllowed->plugin($type['content_type'])) {

                        echo '<li ' . $class . '>';
                        $typeSlug = "/sitekick/items/" . $type['content_type'];
                        $class = '';
                        $activeUrl = explode('/', $this->request->url);

                        if (end($activeUrl) == strtolower($type['content_type'])) {
                            $class = 'active';
                        }

                        if ($type['admin'] == 1) {
                            $class .= " adminonly";
                        }

                        if ($type['admin'] == 0 || $authUser['superadmin'] == 1) {
                            echo $this->Html->link('<span class="' . $type['icon'] . '"></span>' . ucfirst($type['name']) . (isset($after) ? $after : ''), $typeSlug, [
                                'escape' => false,
                                'class' => $class
                            ]);
                        }
                        echo '</li>';
                    }
                }
            endif;
            ?>
            <!-- ./end items plugin -->
        <?php endforeach; ?>


    </ul>
    <ul class="nav nav-sidebar">

        <?php foreach ($plugins as $plugin): ?>
            <?php if ($this->isAllowed->plugin($plugin) && isset($settings[$plugin]['menu']) && $settings[$plugin]['menu'] == "bottom" && (isset($settings[$plugin]['in_menu']) && $settings[$plugin]['in_menu'] != 0)): ?>

                <?php
                $class = '';
                if (strpos($this->request->url, strtolower($plugin))) {
                    $class = 'class="active"';
                }

                if (!empty($settings[$plugin]['slug'])) {
                    if (strpos($this->request->url, strtolower($settings[$plugin]['slug']))) {
                        $class = 'class="active"';
                    }
                }

                ?>

                <li <?php echo $class; ?>>

                    <?php
                    $slug = '/sitekick/' . strtolower($plugin);
                    if (!empty($settings[$plugin]['slug'])) {
                        $slug = '/sitekick/' . $settings[$plugin]['slug'];
                    }

                    $counter = '';
                    if (!empty($settings[$plugin]['menu_counter'])) {
                        $counter = '<i class="menu_counter badge" id="counter_' . strtolower($plugin) . '"></i>';
                    }

                    $after = '';
                    if (!empty($settings[$plugin]['submenu'])) {
                        $after = '<span id="submenu_' . strtolower($plugin) . '" class="opensub icon-dropdown"></span>';
                    }
                    ?>


                    <?php if (isset($settings[$plugin]['submenu'])) {
                        echo $this->Html->link('<span class="' . $settings[$plugin]['icon'] . '"></span>' . $settings[$plugin]['name'] . $counter . $after, 'javascript:;', ['escape' => false]);
                    } else {
                        echo $this->Html->link('<span class="' . $settings[$plugin]['icon'] . '"></span>' . $settings[$plugin]['name'] . $counter . $after, $slug, ['escape' => false]);
                    }
                    ?>

                    <!-- dropdown menu (menu = bottom) -->
                    <?php if (isset($settings[$plugin]['submenu']) && !empty($settings[$plugin]['in_submenu'])): ?>
                        <ul class="sub submenu_<?php echo strtolower($plugin); ?>">
                            <?php $submenuArray = explode(",", $settings[$plugin]['in_submenu']);
                            foreach ($submenuArray as $submenuItem) { ?>
                                <li>
                                    <?php
                                    $subItem = explode("#", $submenuItem);
                                    echo $this->Html->link(
                                        ucfirst($subItem[1]),
                                        '/sitekick/' . $settings[$plugin]['slug'] . '/' . strtolower(str_replace(' ', '', $subItem[0]))
                                    );
                                    ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php endif; ?>
                    <!-- end dropdown menu -->


                    <?php if (isset($settings[$plugin]['submenu']) && isset($settings[$plugin]['slug'])): ?>
                        <ul class="sub submenu_<?php echo strtolower($plugin); ?>">
                            <?php
                            $submenuArray = explode(",", $settings[$plugin]['submenu']);
                            foreach ($submenuArray as $submenuItem) {

                                $submenuItemArray = explode("#", $submenuItem);
                                if (isset($submenuItemArray[1]) && isset($submenuItemArray[0])) {
                                    echo '<li><a href="/sitekick/' . $settings[$plugin]['slug'] . '/' . $submenuItemArray[1] . '">' . $submenuItemArray[0] . '</a></li>';
                                }
                            }
                            ?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>




            <?php
            $class = '';
            if (strpos($this->request->url, 'settings')) {
                $class = 'class="active"';
            }
            ?>

            <li <?php echo $class; ?>>
                <a href="/sitekick/settings"><span class="icon-manage"></span> <?php echo __('Instellingen'); ?> <span id="submenu_settings" class="opensub icon-dropdown"></span></a>

                <ul class="sub submenu_settings">
                    <?php
                    if ($authUser['superadmin'] == 1) {
                        ?>
                        <li class="">
                            <a href="/sitekick/zoekinstellingen"><span class="icon-pencil-2"></span> <?php echo __('Zoekfunctie'); ?></a>
                        </li>
                        <li class="">
                            <a href="/sitekick/languages"><span class="icon-pencil-2"></span> <?php echo __('Talen'); ?></a>
                        </li>

                        <li class="">
                            <a href="/sitekick/types"><span class="icon-pencil-2"></span> <?php echo __('Content types'); ?></a>
                        </li>

                        <li class="">
                            <a href="/sitekick/fields"><span class="icon-pencil-2"></span> <?php echo __('Velden'); ?></a>
                        </li>
                        <li>
                            <a href="/sitekick/cache"><span class="icon-pencil-2"></span> <?php echo __('Cache'); ?></a>
                        </li>
                        <li>
                            <a href="/sitekick/info"><span class="icon-pencil-2"></span> <?php echo __('Informatie'); ?></a>
                        </li>
                        <li>
                            <a href="/sitekick/errors"><span class="icon-pencil-2"></span> <?php echo __('Errorlog'); ?></a>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li>
                            <a href="/sitekick/cache"><span class="icon-pencil-2"></span> <?php echo __('Cache'); ?></a>
                        </li>
                        <?php
                    } ?>

                </ul>
            </li>


        <li <?php echo $class; ?>>
            <a href="#" class="starttour hide-xs" style="display: none;"><span class="icon-help"></span> <?php echo __('Bekijk uitleg!'); ?></a>
        </li>
    </ul>

    <ul class="nav nav-sidebar visible-xs">
        <li><a href="/sitekick/users/edit/<?php echo $authUser['id']; ?>" class="logout-link"><span class="icon-team"></span><?php echo __('Profiel wijzigen'); ?></a></li>
        <li><a href="/sitekick/users/logout?redirect_url=/" class="logout-link"><span class="icon-lock-open"></span><?php echo __('Uitloggen'); ?></a></li>
    </ul>
</div>


<script>

    //open or close submenu
    $(".opensub").click(function () {
        $("." + $(this).attr('id')).toggle();
        return false;
    });

    //always open submenu if main item is active
    $(".nav-sidebar .active .sub").show();

</script>