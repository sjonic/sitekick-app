<script>
    var authLanguage = '<?php $lang = explode('_', $this->request->session()->read('Config.language_sitekick.locale')); echo $lang[0]; ?>';
</script>

<?php
echo $this->cell('App::navbar', [
    'languages'          => $languages,
    'activeLanguage'     => (!empty($activeLanguage)) ? $activeLanguage : [],
    'settings'           => $settings,
    'types'              => (!empty($types)) ? $types : [],
    'authUser'           => (!empty($authUser)) ? $authUser : null,
    'back_url'           => (!empty($back_url)) ? $back_url : $this->Url->build('/sitekick'),
    'front_url'          => (!empty($front_url)) ? $front_url : $this->request->here,
    'SitekickAreaActive' => (!empty($SitekickAreaActive)) ? $SitekickAreaActive : null
]);
?>