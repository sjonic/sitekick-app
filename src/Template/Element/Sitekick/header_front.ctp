<?php if(isset($isAuthorized)): ?>
	<meta name="apple-mobile-web-app-title" content="Sitekick">
	<meta name="msapplication-TileColor" content="#ff0000">
	<meta name="application-name" content="Sitekick">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="Sitekick">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">

    <?php $sitekickLanguage = (!empty($authUser['locale'])) ? substr($authUser['locale'], 0, 2) : 'nl' ?>

	<script>
		var sitekickUrl = '<?php echo $this->Url->build('/',true); ?>sitekick/';
		var websiteUrl = '<?php echo $this->Url->build('/',true); ?>';
		var redactorAdmin = <?php echo (!empty($settings['Base']['html_editor']) && $settings['Base']['html_editor'] == 1) ? 'true' : 'false' ?>;
		var redactorTables= <?php echo (!empty($settings['Base']['editor_tables']) && $settings['Base']['editor_tables'] == 1) ? 'true' : 'false' ?>;
		var redactorInlineImages= <?php echo (!empty($settings['Base']['editor_images']) && $settings['Base']['editor_images'] == 1) ? 'true' : 'false' ?>;
		var adminUser = <?php echo (isset($authUser['role']) && $authUser['role'] == "admin") ? 'true' : 'false'; ?>;
		var sitekickSettings = <?php echo json_encode($settings); ?>;
        var sitekickLanguage = '<?php echo $sitekickLanguage ?>';
        var redactorForms   = <?php echo (!empty($settings['Forms']['in_menu'])) ? 'true' : 'false'; ?>;
        var redactorButtonClass = '<?php echo (!empty($settings['Base']['button_class'])) ? $settings['Base']['button_class'] : '' ?>';
        var editorStyles = <?= ( !empty($settings['Base']['editor_custom_formats']) ) ? '['.$settings['Base']['editor_custom_formats'].']' : '[]' ?>;
	</script>
<?php endif; ?>

<?php
	echo $this->Html->css([
		'sitekick/icons',
		'sitekick/bootstrap.sitekickfront',
		'sitekick/navbar',
		'sitekick/switchbuttons',
		'sitekick/inline-edit',
		'sitekick/loaders.min.css',
		'../sitekick_library/sweetalert/sweet-alert',
		'Media.uploadwidget']);

	echo $this->Html->script([
		'../sitekick_library/sweetalert/sweet-alert',
		'sitekick/jquery-ui.min',
        'sitekick/sitekick',
		'sitekick/webapp',
		'sitekick/mustache',
		'sitekick/jquery.waitforimages.min',

		'Media.crop',
		'Media.jquery.ui.widget',
		'Media.jquery.iframe-transport',
		'Media.jquery.fileupload'
	], ['block' => 'scriptBottom']);


    // TinyMCE
    echo $this->Html->script('tinymce/tinymce.min.js', ['block' => 'scriptBottom']);
    echo $this->Html->script('tinymce/jquery.tinymce.min.js', ['block' => 'scriptBottom']);
    if( $sitekickLanguage != 'en' ){
        echo $this->Html->script('tinymce/langs/'.$sitekickLanguage.'.js', ['block' => 'scriptBottom']);
    }
    echo $this->Html->script('tinymce/sitekick.js', ['block' => 'scriptBottom']);
    echo $this->Html->css('sitekick/tinymce.css');

    echo $this->Html->script('sitekick/jquery.inline-edit', ['block' => 'scriptBottom']);


	echo $this->Html->script([
		'sitekick/jquery.inputs',
		'sitekick/jquery.nestable',
		'sitekick/jquery.mobile-events'

	], ['block' => 'scriptBottom']);

	$this->Html->scriptStart(array('block' => 'scriptBottom', 'inline' => false));
	echo 'try{Typekit.load();}catch(e){}';
	$this->Html->scriptEnd();

?>
<style>
	body {
		padding-top: 70px;
	}
</style>