<div id="updatesModal" class="modal fade _loading" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body _loading">

                <div id="updatemessages">

                    <?php
                    $ids = [];
                    foreach( $messages as $message ) { ?>
                        <div class="updatemessage">
                            <div class="message" data-id="<?php echo $message['id']; ?>">
                                <div class="well">
                                    <span class="icon-update"></span>
                                    <h3><?php echo __('Nieuwe update'); ?></h3>
                                    <div class="date">
                                        <?php echo __('Geplaatst op {0} om {1} uur', [$message['date']->format('d-m-Y'), $message['date']->format('H:i')]); ?>
                                    </div>
                                </div>
                                <div class="content">
                                    <?php echo $message['message']; ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $ids[] = $message['message_id'];
                    }
                    ?>

                </div>
                <div class="buttons">
                    <button class="btn ok"><?php echo __('Ik snap het'); ?></button>
                    <a href="#" class="skip"><?php echo __('Nu even niet'); ?></a>
                </div>
            </div>
        </div>

    </div>
</div>


<?php
echo $this->Html->script('sitekick/slick.min.js');
echo $this->Html->scriptBlock('$(document).ready(function() {

    $("#updatesModal").modal("show");    
    $("#updatesModal").on("shown.bs.modal", function (e) {
        
        $("#updatemessages").slick({
            dots: true,
            prevArrow: false,
            adaptiveHeight: true
        });    
        
        $("#updatesModal").removeClass("_loading");
    });  
    
    $("body").on("click", "#updatesModal .btn.ok", function() {   
    
        $.ajax({
            type: "post",
            data: { "ids" : "' . json_encode($ids) . '", "display" : "0" },  
            url: "' . $this->Url->build(["controller" => "UpdatemessagesUsers", "action" => "update", "plugin" => false ]) . '"
        });    
              
        $("#updatesModal").modal("hide"); 
        return false;
    });    
    
    $("body").on("click", "#updatesModal .skip", function() {  
        $.ajax({
            type: "post",
            data: { "ids" : "' . json_encode($ids) . '", "display" : "1" },  
            url: "' . $this->Url->build(["controller" => "UpdatemessagesUsers", "action" => "update", "plugin" => false ]) . '"
        });
                  
        $("#updatesModal").modal("hide"); 
        return false;
    });

})');
?>