<script>
	sweetAlert({
		title: "",
		text: "<?php echo __('Op dit moment is <strong>{0} {1}</strong> deze pagina aan het bewerken. Om er voor te zorgen dat jullie elkaars werk niet overschrijven, kan je deze pagina op dit moment niet bewerken. Wacht tot <strong>{0} {1}</strong> klaar is of tot de pagina automatisch wordt vrijgegeven over <strong>{2}</strong>', [$editing_user->firstname, $editing_user->lastname, $editing_togo]); ?>",
		type: "info",
		confirmButtonColor: "#2eb582",
		confirmButtonText: "<?php echo __('OK, sluiten'); ?>",
		closeOnConfirm: false,
        showCancelButton: true,
        cancelButtonText: "<?php echo __('Bewerk toch') ?>"
	}, function(isConfirmed){
	    // oh no, we are going to overwrite the warning
	    if( isConfirmed === false ){
            document.location.href = sitekickUrl + 'dashboard/overwriteSession/?url=<?= $this->request->url ?>';
        }
    });

	$('.main').append('<div class="editing" style="width:100%; height: 100%; z-index:2000; position: fixed; background: #fff; top:70px; opacity: 0.6">');
</script>