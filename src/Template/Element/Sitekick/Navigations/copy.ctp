
<div id="copyModal" class="modal fade" role="dialog" style="z-index: 999999999;">
    <div class="modal-dialog" style="z-index: 5000;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header text-center">
                <h3 class="modal-title" style="padding-left: 40px;margin-bottom: 0;"><span class="icon-reload" style="margin-top: 8px;"></span> Pagina kopiëren</h3>
            </div>
            <?php echo $this->Form->create(null, ['id' => 'copy-form', 'url' => ['plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'copy', 0]]); ?>
            <div class="modal-body">
                <?php echo $this->Form->input('title', ['label' => __('Geef een nieuwe paginatitel aan'), 'required' => true]); ?>

            </div>
            <div class="modal-footer">
                <a href="javascript:;" data-dismiss="modal"><?php echo __('Annuleren'); ?></a> &nbsp;
                <button type="submit" class="btn btn-primary"><?php echo __('Pagina kopiëren'); ?></button>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>

    </div>
</div>

<script>
    $(document).ready(function(){
       $('#copy-form').submit(function(){
            $('#copy-form .btn-primary').prop('disabled', true).html('<?php echo __('Bezig met opslaan...'); ?>');

       });
    });
</script>