<div class="formblock formblock-compact editor">
    <div class="modal-form">
        <?php
        echo $this->Form->create($setting);
        echo $this->Form->control('multilingual', [
            'options' => [
                0 => __('Nee'),
                1 => __('Ja')
            ],
            'tabindex' => '-1',
            'label' => __('Meertalig'),
        ]);
        echo $this->Form->control('is_public', [
            'options' => [
                0 => __('Nee'),
                1 => __('Ja')
            ],
            'default' => 1,
            'tabindex' => '-1',
            'label' => __('Klant kan instelling zien'),
        ]);
        echo $this->Form->control('plugin', [
            'options' => $settingsList,
            'tabindex' => '-1',
            'label' => __('Plugin'),
            'default' => $settings['Base']['theme'],
            'empty' => __('Maak een keuze')
        ]);
        echo $this->Form->control('name', [
            'label' => __('Unieke key'),
            'placeholder' => __('Voorbeeld: address_data'),
            'required',
        ]);
        echo $this->Form->control('description', [
            'label' => __('Omschrijving (zichtbaar voor klant)'),
            'placeholder' => __('Voorbeeld: Adresgegevens'),
            'required',
        ]);
        echo $this->Form->control('explanation', [
            'label' => __('Extra uitleg (optioneel)'),
            'placeholder' => __('Voorbeeld: Vul hier adresgegevens in')
        ]);
        echo $this->Form->control('value', [
            'label' => __('Waarde')
        ]);
        echo $this->Form->button(__('Opslaan'), ['class' => 'btn btn-primary btn-']);
        ?>
    </div>
</div>