<?php
	echo $this->Form->create($type);
	echo '<div class="formblock formblock-compact row">';
	echo $this->Form->input('content_type', ['label' => 'Type']);
    echo $this->Form->input('name_single_pre', ['label' => __('Naam enkelvoud voornaamwoord'), 'options' => [
        'deze' => 'Deze',
        'dit' => 'Dit'
    ]]);

	echo $this->Form->input('name_single', ['label' => __('Naam enkelvoud')]);
	echo $this->Form->input('name', ['label' => __('Naam meervoud')]);
    echo $this->Form->input('translations.en_US.name_single', ['label' => __('Naam enkelvoud Engels'), 'value' => isset($type->_translations['en_US']->name_single) ? $type->_translations['en_US']->name_single : '']);
    echo $this->Form->input('translations.en_US.name', ['label' => __('Naam meervoud Engels'), 'value' => isset($type->_translations['en_US']->name) ? $type->_translations['en_US']->name : '']);
	echo $this->Form->input('menu_slug', ['label' => __('Menu slug (bijvoorbeeld "product" of "product/:merk")')]);

	if( $languages->count() > 1 ){
	    foreach($languages as $l){
	        if( $l->locale != 'nl_NL' ){
	            echo $this->Form->input('translations.'.$l->locale.'.menu_slug', [
                    'value' => isset($type->_translations[$l->locale]->menu_slug) ? $type->_translations[$l->locale]->menu_slug : '',
                    'label' =>  __('Menu slug (bijvoorbeeld "product" of "product/:merk")') . ' ' . $l->own_label]);
            }
        }
    }

	echo $this->Form->input('icon', ['label' => __('Menu icoon')]);
    echo $this->Html->link(__('Kies een icoon'), 'javascript:;', [
        'onclick' => '$(".sitekick-icon-picker").fadeToggle();',
        'class' => 'btn-primary btn-small',
        'style' => 'font-size: 16px;'

    ]);

    ?>
    <div class="sitekick-icon-picker clearfix">
        <div class="col-md-1"><span class="icon-date"></span></div>
        <div class="col-md-1"><span class="icon-reload"></span></div>
        <div class="col-md-1"><span class="icon-loading"></span></div>
        <div class="col-md-1"><span class="icon-stats"></span></div>
        <div class="col-md-1"><span class="icon-arrow-right"></span></div>
        <div class="col-md-1"><span class="icon-check"></span></div>
        <div class="col-md-1"><span class="icon-check-2"></span></div>
        <div class="col-md-1"><span class="icon-cross"></span></div>
        <div class="col-md-1"><span class="icon-cross-2"></span></div>
        <div class="col-md-1"><span class="icon-delete"></span></div>
        <div class="col-md-1"><span class="icon-dropdown"></span></div>
        <div class="col-md-1"><span class="icon-envelop"></span></div>
        <div class="col-md-1"><span class="icon-form"></span></div>
        <div class="col-md-1"><span class="icon-heart"></span></div>
        <div class="col-md-1"><span class="icon-help"></span></div>
        <div class="col-md-1"><span class="icon-home"></span></div>
        <div class="col-md-1"><span class="icon-in-cloud"></span></div>
        <div class="col-md-1"><span class="icon-info"></span></div>
        <div class="col-md-1"><span class="icon-link"></span></div>
        <div class="col-md-1"><span class="icon-list"></span></div>
        <div class="col-md-1"><span class="icon-location"></span></div>
        <div class="col-md-1"><span class="icon-lock"></span></div>
        <div class="col-md-1"><span class="icon-lock-open"></span></div>
        <div class="col-md-1"><span class="icon-manage"></span></div>
        <div class="col-md-1"><span class="icon-menu-2"></span></div>
        <div class="col-md-1"><span class="icon-min"></span></div>
        <div class="col-md-1"><span class="icon-move"></span></div>
        <div class="col-md-1"><span class="icon-newsletter"></span></div>
        <div class="col-md-1"><span class="icon-order"></span></div>
        <div class="col-md-1"><span class="icon-painting"></span></div>
        <div class="col-md-1"><span class="icon-paintings"></span></div>
        <div class="col-md-1"><span class="icon-paper"></span></div>
        <div class="col-md-1"><span class="icon-pencil-2"></span></div>
        <div class="col-md-1"><span class="icon-phone"></span></div>
        <div class="col-md-1"><span class="icon-plus"></span></div>
        <div class="col-md-1"><span class="icon-reference"></span></div>
        <div class="col-md-1"><span class="icon-save"></span></div>
        <div class="col-md-1"><span class="icon-search"></span></div>
        <div class="col-md-1"><span class="icon-seo"></span></div>
        <div class="col-md-1"><span class="icon-seo-2"></span></div>
        <div class="col-md-1"><span class="icon-sitekick"></span></div>
        <div class="col-md-1"><span class="icon-statistics"></span></div>
        <div class="col-md-1"><span class="icon-team"></span></div>
        <div class="col-md-1"><span class="icon-thunder"></span></div>
        <div class="col-md-1"><span class="icon-web"></span></div>
        <div class="col-md-12 text-center">
            <?php echo $this->Html->link(__('Annuleren'), 'javascript:;', ['onclick' => '$(".sitekick-icon-picker").fadeToggle();']); ?>
        </div>
    </div>
    <?php
	echo $this->Form->input('overview', ['label' => __('Overzicht tonen'), 'options' => [
		'table' => __('Tabel overzicht'),
		'blocks' => __('Blokken overzicht')
	]]);
	echo $this->Form->input('sort', ['label' => __('Sorteer items op typeveld (laat dit veld leeg als de klant zelf kan sorteren)')]);
	echo $this->Form->input('active', [
		'type' => 'checkbox',
		'label' => [
			'text' => __('Actief'),
			'style' => 'float: left; margin-left: 0; margin-right: 20px;'
		],
		'class' => 'switch',
		'value'=> 1,
		'checked'=> isset($type->active) && $type->active == 0 ? false : true
	]);

    echo $this->Form->input('dashboard', [
        'type' => 'checkbox',
        'label' => [
            'text' => __('Tonen op dashboard'),
            'style' => 'float: left; margin-left: 0; margin-right: 20px;'
        ],
        'class' => 'switch',
        'value'=> 1,
        'checked'=> isset($type->dashboard) && $type->dashboard == 0 ? false : true
    ]);

    echo $this->Form->input('has_page', [
            'type' => 'checkbox',
            'label' => [
                    'text' => __('Items hebben eigen pagina'),
                    'style' => 'float: left; margin-left: 0; margin-right: 20px;'
            ],
            'class' => 'switch',
            'value'=> 1,
            'checked'=> isset($type->has_page) && $type->has_page == 0 ? false : true
    ]);

	echo $this->Form->input('use_cache', [
		'type' => 'checkbox',
		'label' => [
			'text' => __('Gebruik cache'),
			'style' => 'float: left; margin-left: 0; margin-right: 20px;'
		],
		'class' => 'switch',
		'value'=> 1,
		'checked'=> isset($type->use_cache) && $type->use_cache == 0 ? false : true
	]);

    echo $this->Form->input('use_headers', [
        'type' => 'checkbox',
        'label' => [
            'text' => __('Gebruik headers'),
            'style' => 'float: left; margin-left: 0; margin-right: 20px;'
        ],
        'class' => 'switch',
        'value'=> 1,
        'checked'=> (!empty($type->use_headers)) ? $type->use_headers : false
    ]);

	echo $this->Form->input('admin', [
		'type' => 'checkbox',
		'label' => [
			'text' => __('Alleen voor superadmin'),
			'style' => 'float: left; margin-left: 0; margin-right: 20px;'
		],
		'class' => 'switch',
		'value'=> 1,
		'checked'=> isset($type->admin) && $type->admin == 0 ? false : true
	]);

	echo $this->Form->input('in_sitemap', [
			'type' => 'checkbox',
			'label' => [
					'text' => __('Tonen in sitemap / link toevoegen'),
					'style' => 'float: left; margin-left: 0; margin-right: 20px;'
			],
			'class' => 'switch',
			'value'=> 1,
			'checked'=> isset($type->in_sitemap) && $type->in_sitemap == 0 ? false : true
	]);

    echo $this->Form->input('can_add', [
        'type' => 'checkbox',
        'label' => [
            'text' => __('Toevoegen toestaan'),
            'style' => 'float: left; margin-left: 0; margin-right: 20px;'
        ],
        'class' => 'switch',
        'value'=> 1,
        'checked'=> isset($type->can_add) && $type->can_add == 0 ? false : true
    ]);

	echo '</div>';

	echo $this->Form->submit(__('Content type opslaan'), ['class'=> 'btn btn-primary']);
	echo '&nbsp; ' . $this->Html->link(__('Annuleren'), ['action' => 'index']);
	echo $this->Form->end();
?>

<script>
    $('.sitekick-icon-picker .col-md-1').click(function(){
        $('#icon').val( $(this).find('span').attr('class') );
        $('.sitekick-icon-picker').fadeToggle();
    });
</script>
