<?php
echo $this->Form->create($typefield);
echo '<div class="formblock formblock-compact row">';

echo $this->Form->input('name', ['label' => __('Naam')]); 
echo $this->Form->input('description', ['type' => 'textarea', 'id' => 'input-description', 'label' => __('Uitleg')]);
echo $this->Form->input('translations.en_US.name', ['label' => __('Naam Engels'), 'value' => isset($typefield->_translations['en_US']->name) ? $typefield->_translations['en_US']->name : '']);
echo $this->Form->input('translations.en_US.description', ['type' => 'textarea', 'label' => __('Uitleg Engels'), 'value' => isset($typefield->_translations['en_US']->description) ? $typefield->_translations['en_US']->description : '']);
echo $this->Form->input('key', ['label' => __('Veldnaam')]);

$options = [];

foreach($fields as $field){
    $options[$field->fieldtype] = $field->name;
}
echo $this->Html->scriptBlock('var fields = ' . json_encode($fields->toArray()));

echo $this->Form->input('fieldtype', ['label' => __('Type'),'options' => $options]);

echo $this->Form->input('options', ['label' => __('Opties')]);
echo $this->Html->link(__('Instellingen bewerken'), 'javascript:;', ['class' => 'btn-primary btn-small btn-image-settings hidden']);

echo $this->Form->input('rank', ['label' => __('Positie')]);

echo $this->Form->input('required', [
    'type' => 'checkbox',
    'label' => [
        'text' => __('Verplicht'),
        'style' => 'float: left; margin-left: 0; margin-right: 20px;'
    ],
    'class' => 'switch',
    'value'=> 1,
    'checked'=> isset($typefield->required) && $typefield->required == 0 ? false : true
]);

echo $this->Form->input('overview', [
    'type' => 'checkbox',
    'label' => [
        'text' => __('Gebruik in overzicht'),
        'style' => 'float: left; margin-left: 0; margin-right: 20px;'
    ],
    'class' => 'switch',
    'value'=> 1,
    'checked'=> isset($typefield->overview) && $typefield->overview == 0 ? false : true
]);

echo $this->Form->input('multilingual', [
    'type' => 'checkbox',
    'label' => [
        'text' => __('Waarde is voor alle talen gelijk'),
        'style' => 'float: left; margin-left: 0; margin-right: 20px;'
    ],
    'class' => 'switch',
    'value'=> 1,
    'checked'=> isset($typefield->multilingual) && $typefield->multilingual == 0 ? false : true
]);

echo $this->Form->input('active', [
    'type' => 'checkbox',
    'label' => [
        'text' => __('Actief'),
        'style' => 'float: left; margin-left: 0; margin-right: 20px;'
    ],
    'class' => 'switch',
    'value'=> 1,
    'checked'=> isset($typefield->active) && $typefield->active == 0 ? false : true
]);


echo '</div>';



echo $this->Form->input('type_id', ['type' => 'hidden', 'value' => $type->id]);

echo $this->Form->submit(__('Veld opslaan'), ['class'=> 'btn btn-primary']);
echo $this->Form->end();
?>

<?php
echo $this->Element('Media.Sitekick/settings');
echo $this->Html->script('Media.library.js');
?>

<script>
    $(document).ready(function(){
        $(".switch").switchButton();
        $("#fieldtype").change(function(){
            var value = $("#fieldtype").val();
            if( value == "image" ){
                openImageSettings();
                $('.btn-image-settings').removeClass('hidden').click(openImageSettings);
            } else {
                $('.btn-image-settings').addClass('hidden');
            }

            // check for placeholder
            $.each(fields, function(key,data){
                if( data.fieldtype === value ){
                    $('#description').remove();
                    if( data.description ){
                        $('#options').after('<div class="small" style="clear:both;margin-top: 15px" id="description"><small>Bijvoorbeeld: ' + data.description + '</small></div>');
                    }
                }
            })

        });
        if( $('#fieldtype').val() == 'image' ){
            $('.btn-image-settings').removeClass('hidden').click(openImageSettings);
        }

        $("#setImageSettings").click(function(){
            parseImageSettingsToJson();
        });
    });

    function parseImageSettingsToJson() {
        var settings = {};
        $('.sitekick-modal-image-settings input').each(function(){
            if( $(this).attr('name') != '_method' ){
                if( $(this).attr('type') == 'checkbox' ){
                    settings[ $(this).attr('name') ] = $(this).is(':checked');
                } else {
                    settings[ $(this).attr('name') ] = $(this).val();
                }
            }
        });
        $('#options').val( JSON.stringify(settings) );
    }

    function openImageSettings() {

        if( $('#options').val() != "" ){
            var settings = $.parseJSON( $('#options').val() ); // current settings
        } else {
            var settings = {};
        }

        if( settings.cropper == 1 ){ $('#cropper').prop('checked', true); }
        if( settings.single == 1 ){ $('#single').prop('checked', true); }

        $('#cropper-width').val( (settings.cropper_width != "" ) ? settings.cropper_width : "" );
        $('#cropper-height').val( (settings.cropper_height != "" ) ? settings.cropper_height : "" );
        $('#thumbnail-width').val( (settings.thumbnail_width != "" ) ? settings.thumbnail_width : "" );
        $('#thumbnail-height').val( (settings.thumbnail_height != "" ) ? settings.thumbnail_height : "" );
        $('#cropper-background').val( (settings.cropper_background != "" ) ? settings.cropper_background : "" );
        $('#cropper-scale').val( (settings.cropper_scale != "" && typeof settings.cropper_scale != "undefined" ) ? settings.cropper_scale : 1 );

        $('#rounded').val( (settings.rounded != "" && typeof settings.rounded != "undefined" ) ? settings.rounded : 0 );
        $('#lazyload').val( (settings.lazyload != "" && typeof settings.lazyload != "undefined" ) ? settings.lazyload : 1 );

        $(".sitekick-modal-image-settings").modal();
    }
</script>