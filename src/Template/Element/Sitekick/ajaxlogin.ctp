
<div class="ajaxlogin_background"></div>
<div class="ajaxlogin">

	<div class="ajaxlogin_form">

		<?php echo $this->Form->create('',['class' => 'form-signin']) ?>
		<?php

		$email   = "";
		$checked = "";

		if(isset($_COOKIE['user'])) {
			$user  = json_decode( $_COOKIE['user'] );
			$email = $user->email;
		}
		?>

		<h2>
			<?php echo __('Even opnieuw inloggen...'); ?>
		</h2>

		<div class="ajaxlogin_error help-block rounded" style="display: none;">
            <?php echo __('E-mailadres en wachtwoord combinatie is onjuist.'); ?>
		</div>

		<?php echo $this->Form->input('email',['addon' => 'icon-envelop','label' => false,'placeholder' => __('E-mailadres'),'value'=> $email]); ?>
		<?php echo $this->Form->input('password',['addon' => 'icon-lock','label' => false,'placeholder' => __('Wachtwoord')]); ?>
        <?php
        $sitekickLanguage = $this->request->Session()->read('Config.language_sitekick');
        echo $this->Form->input('locale',['type' => "hidden",'value' => $sitekickLanguage->locale]);
        ?>
		<?php echo $this->Form->button(__('Aan de slag!'),['class' => 'btn btn-lg btn-primary btn-block']); ?>
		<?php echo $this->Form->end() ?>
	</div>
</div>

<script>
	$(".form-signin").submit(function(e) {

		e.preventDefault();
		$.ajax({
			url: sitekickUrl + 'users/ajaxlogin',
			type: 'post',
			dataType: 'json',
			data: $(".form-signin").serialize(),
			success: function (data) {

				if (data == 1) {
					$(".ajaxlogin_background").hide();
					$(".ajaxlogin").hide();
					$(".ajaxlogin_error").hide();
				}

				if (data == 0) {
					$(".ajaxlogin_error").show();
				}

			}
		});
	});
</script>