<meta name="apple-mobile-web-app-title" content="Sitekick">
<meta name="msapplication-TileColor" content="#ff0000">
<meta name="application-name" content="Sitekick">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="Sitekick">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<?php $sitekickLanguage = (!empty($authUser['locale'])) ? substr($authUser['locale'], 0, 2) : 'nl' ?>

<script>
	var sitekickUrl = '<?php echo $this->Url->build('/',true); ?>sitekick/';
	var websiteUrl = '<?php echo $this->Url->build('/',true); ?>';
	var definedLinks = '/sitemap/editor';
	var redactorAdmin = <?php echo (!empty($settings['Base']['html_editor']) && strip_tags($settings['Base']['html_editor']) == 1) ? 'true' : 'false' ?>;
    var redactorTables = <?php echo (!empty($settings['Base']['editor_tables']) && strip_tags($settings['Base']['editor_tables']) == 1) ? 'true' : 'false' ?>;
	var redactorInlineImages= <?php echo (!empty($settings['Base']['editor_images']) && strip_tags($settings['Base']['editor_images']) == 1) ? 'true' : 'false' ?>;
	var adminUser = <?php echo (!empty($authUser['superadmin']) && $authUser['superadmin'] == 1) ? 'true' : 'false'; ?>;
	var sitekickSettings = <?php echo json_encode($settings); ?>;
	var sitekickLanguage = '<?php echo $sitekickLanguage ?>';
    var redactorForms   = <?php echo (in_array('Forms', $plugins)) ? 'true' : 'false'; ?>;
    var redactorButtonClass = '<?php echo (!empty($settings['Base']['button_class'])) ? $settings['Base']['button_class'] : '' ?>';
    var editorStyles = <?= ( !empty($settings['Base']['editor_custom_formats']) ) ? '['.$settings['Base']['editor_custom_formats'].']' : '[]' ?>;
</script>

<?php
    if( $this->elementExists('js_include_translations') ) {
        echo $this->element('js_include_translations');
    }

	echo $this->Html->script('/sitekick_library/sweetalert/sweet-alert');

	echo $this->Html->css('sitekick/icons');
	echo $this->Html->css('sitekick/bootstrap');
	echo $this->Html->css('sitekick/bootstrap-sitekick');
	echo $this->Html->css('sitekick/navbar');
	echo $this->Html->css('sitekick/animate');
	echo $this->Html->css('sitekick/switchbuttons');
	echo $this->Html->css('sitekick/loaders.min');
	echo $this->Html->css('sitekick/flag-icon');
	echo $this->Html->css('sitekick/inline-edit');
    if( $this->request->session()->check('updatemessages') ) {
        echo $this->Html->css('sitekick/updatemessages');
    }

	echo $this->Html->css('/sitekick_library/labelauty/jquery-labelauty');
	echo $this->Html->css('/sitekick_library/datepicker/css/datepicker3');

	echo $this->Html->css('Media.uploadwidget');



	echo $this->Html->script('jquery.min');

	echo $this->Html->script('sitekick/bootstrap');
	echo $this->Html->script('sitekick/sitekick');

	echo $this->Html->script('sitekick/js_validator');
	echo $this->Html->script('sitekick/jquery.inputs');
	echo $this->Html->script('sitekick/jquery.quicksearch');
	echo $this->Html->script('sitekick/jquery.waitforimages.min');
	echo $this->Html->script('/sitekick_library/labelauty/jquery-labelauty');
	echo $this->Html->script('/sitekick_library/datepicker/js/bootstrap-datepicker');
	echo $this->Html->script('/sitekick_library/datepicker/js/locales/bootstrap-datepicker.nl.js');
	echo $this->Html->script('sitekick/webapp');
	echo $this->Html->script('sitekick/mustache');
	echo $this->Html->script('sitekick/loaders');
	echo $this->Html->script('sitekick/jquery.inline-edit');

	echo $this->Html->script([
		'Media.crop',
		'Media.jquery.ui.widget',
		'Media.jquery.iframe-transport',
		'Media.jquery.fileupload'
	], ['block' => 'scriptBottom']);

    // TinyMCE
    echo $this->Html->script('tinymce/tinymce.min.js');
    echo $this->Html->script('tinymce/jquery.tinymce.min.js');
    if( $sitekickLanguage != 'en' ){
        echo $this->Html->script('tinymce/langs/'.$sitekickLanguage.'.js');
    }
    echo $this->Html->script('tinymce/sitekick.js');
    echo $this->Html->css('sitekick/tinymce.css');

    // Sweet Alert
    echo $this->Html->css('/sitekick_library/sweetalert/sweet-alert');

	//Icons and startup images
	echo $this->Html->meta('apple-touch-icon-57x57.png','/img/sitekick/icons/apple-touch-icon-57x57.png',['type' => 'icon', 'rel' => 'apple-touch-icon','sizes' =>'57x57']);
	echo $this->Html->meta('apple-touch-icon-114x114.png','/img/sitekick/icons/apple-touch-icon-114x114.png',['type' => 'icon', 'rel' => 'apple-touch-icon','sizes' =>'114x114']);
	echo $this->Html->meta('apple-touch-icon-72x72.png','/img/sitekick/icons/apple-touch-icon-72x72.png',['type' => 'icon', 'rel' => 'apple-touch-icon','sizes' =>'72x72']);
	echo $this->Html->meta('apple-touch-icon-144x144.png','/img/sitekick/icons/apple-touch-icon-144x144.png',['type' => 'icon', 'rel' => 'apple-touch-icon','sizes' =>'144x144']);
	echo $this->Html->meta('apple-touch-icon-60x60.png','/img/sitekick/icons/apple-touch-icon-60x60.png',['type' => 'icon', 'rel' => 'apple-touch-icon','sizes' =>'60x60']);
	echo $this->Html->meta('apple-touch-icon-120x120.png','/img/sitekick/icons/apple-touch-icon-120x120.png',['type' => 'icon', 'rel' => 'apple-touch-icon','sizes' =>'120x120']);
	echo $this->Html->meta('apple-touch-icon-76x76.png','/img/sitekick/icons/apple-touch-icon-76x76.png',['type' => 'icon', 'rel' => 'apple-touch-icon','sizes' =>'76x76']);
	echo $this->Html->meta('apple-touch-icon-152x152.png','/img/sitekick/icons/apple-touch-icon-152x152.png',['type' => 'icon', 'rel' => 'apple-touch-icon','sizes' =>'152x152']);

	echo $this->Html->meta('1536x2008.png','/img/sitekick/startup/1536x2008.png',['type' => 'icon', 'rel' => 'apple-touch-startup-image','media' =>'screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 2)']);
	echo $this->Html->meta('1496x2048.png','/img/sitekick/startup/1496x2048.png',['type' => 'icon', 'rel' => 'apple-touch-startup-image','media' =>'screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 2)']);
	echo $this->Html->meta('768x1004.png','/img/sitekick/startup/768x1004.png',['type' => 'icon', 'rel' => 'apple-touch-startup-image','media' =>'(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 1)']);
	echo $this->Html->meta('748x1024.png','/img/sitekick/startup/748x1024.png',['type' => 'icon', 'rel' => 'apple-touch-startup-image','media' =>'(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 1)']);
	echo $this->Html->meta('640x1096.png','/img/sitekick/startup/640x1096.png',['type' => 'icon', 'rel' => 'apple-touch-startup-image','media' =>'(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)']);
	echo $this->Html->meta('640x920.png','/img/sitekick/startup/640x920.png',['type' => 'icon', 'rel' => 'apple-touch-startup-image','media' =>'(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)']);
	echo $this->Html->meta('320x460.png','/img/sitekick/startup/320x460.png',['type' => 'icon', 'rel' => 'apple-touch-startup-image','media' =>'(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 1)']);

	echo $this->Html->meta('favicon-196x196.png','/img/sitekick/icons/favicon-196x196.png',['type' => 'icon', 'rel' => 'icon','sizes' =>'196x196']);
	echo $this->Html->meta('favicon-160x160.png','/img/sitekick/icons/favicon-160x160.png',['type' => 'icon', 'rel' => 'icon','sizes' =>'160x160']);
	echo $this->Html->meta('favicon-96x96.png','/img/sitekick/icons/favicon-96x96.png',['type' => 'icon', 'rel' => 'icon','sizes' =>'96x96']);
	echo $this->Html->meta('favicon-16x16.png','/img/sitekick/icons/favicon-16x16.png',['type' => 'icon', 'rel' => 'icon','sizes' =>'16x16']);
	echo $this->Html->meta('favicon-32x32.png','/img/sitekick/icons/favicon-32x32.png',['type' => 'icon', 'rel' => 'icon','sizes' =>'32x32']);

	echo $this->Html->meta('msapplication-TileImage','/img/sitekick/icons/mstile-144x144.png',['type' => 'icon', 'name' => 'msapplication-TileImage', 'content'=> '/img/sitekick/icons/mstile-144x144.png']);

	echo $this->fetch('script');
	echo $this->fetch('css');

?>