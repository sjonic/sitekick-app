<?php
    if( ! isset($send)){
    echo $this->Form->create($contact, ['id' => 'contactform']);
    echo $this->Form->input('name', ['label' => __('Naam *')]);
    echo $this->Form->input('phone', ['label' => __('Telefoonnummer')]);
    echo $this->Form->input('email', ['label' => __('E-mailadres *')]);
    echo $this->Form->input('message', ['label' => __('Bericht *'),'type' => 'textarea']);
    echo $this->Form->button(__('Versturen'));
    echo $this->Form->end();
    }
?>

<?php if(isset($send)): ?>
       <h2><?php echo __('Bedankt'); ?></h2>
       <p><?php echo __('Wij nemen zo spoedig mogelijk contact met je op'); ?></p>


<?php endif; ?>