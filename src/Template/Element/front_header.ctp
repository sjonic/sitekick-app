<?php echo $this->Html->docType('html5'); ?>
<html lang="<?php echo (!empty($activeLanguage)) ? $activeLanguage->abbreviation : 'nl'; ?>">
<head>
    <title><?php if(isset($seo_title)) { echo $seo_title; }else{ echo ((!empty($meta_title) ) ? $meta_title . " | " : '').$settings['Base']['site_name']; }?></title>

    <?php
    echo $this->Html->charset();
    echo $this->Html->meta('viewport', 'width=device-width, initial-scale=1');
    if(isset($meta_description)) { echo $this->Html->meta('description', $meta_description); }

    // Indexation needed?
    if( isset($nofollow) || ( isset($settings['Base']['test_environment']) && $settings['Base']['test_environment'] == 1 && !isset($isAuthorized) ) ) {
        echo $this->Html->meta('robots', 'noindex, nofollow');
    }

    // Awesome images
    echo $this->Html->meta('icon', 'favicon.png', array('type' => 'image/png'));
    echo $this->Html->meta('apple-touch-icon', 'favicon.png');
    echo $this->Html->meta('og:image', $this->Url->build('logo.png', TRUE));

    // Required files for admin bar
    if( isset($authUser) ){
        echo $this->Element('/Sitekick/header_front');
    }

    // Include stylesheets
    // Optional: if there is an array with css elements
    // @param: $cssElements {array}
    $css = ['bootstrap.min.css', 'fancybox', 'main'];
    if( !empty($cssElements) ){ $css = array_merge($css, $cssElements); }
    echo $this->Html->css($css);

    // Load analytics
    if( !empty($settings['Base']['analytics']) ) { ?>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '<?php echo $settings['Base']['analytics']; ?>', 'auto');
            ga('set', 'anonymizeIp', true);
            ga('send', 'pageview');

        </script>
    <?php
    }

    // Load typekit
    if( !empty($settings['Base']['typekit']) ) {
        echo $this->Html->script('//use.typekit.net/' . $settings['Base']['typekit'] . '.js');
        echo $this->Html->scriptBlock('try{Typekit.load();}catch(e){}');
    }

    // Load humanizer script
    if( !empty($settings['Base']['human']) ){
        echo $this->Html->scriptBlock("var human = '".$settings['Base']['human']."'");
    }

    // Set some default footer JS
    $this->Html->script([
        'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js',
        'https://oss.maxcdn.com/respond/1.4.2/respond.min.js'
    ], ['block' => 'scriptBottom']);
    ?>
</head>
<body class="<?php if( isset($authUser)) echo 'sitekick'; ?>">
    <?php
    // Load test environment view
    echo $this->Element('testenv');

    // Sitekick bar, only show when logged in d'oh
    if(isset($authUser)){ echo $this->element('/Sitekick/navbar');}
    ?>
