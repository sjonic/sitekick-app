<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<meta name="description" content="">
	<meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <?php echo $this->fetch('css') ?>

	<title>Sitekick  - <?= $this->fetch("title") ?></title>
	<?php echo $this->element('/Sitekick/header'); ?>
</head>

<body id="sitekick" class="sitekick">
<?php if( !empty($imagick_error) ) {
    echo $this->element('Flash/imagick');
}
echo $this->Flash->render();
?>

<!-- FONTS -->
<?php
echo $this->fetch('scriptBottom');
echo $this->element('/Sitekick/navbar');
?>


<div id="container_dashboard">

	<div class="container-fluid">
		<div class="row">
			<?php echo $this->element('/Sitekick/sidebar'); ?>
			<?php //echo $this->element('/Sitekick/help'); ?>
			<div class="main col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 content-container <?php if(isset($nosidebar)): ?>fullscreen<?php endif; ?>" <?php if(isset($nosidebar)): ?>style="width: 100%; margin: 0px;"<?php endif; ?>>
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
	</div>
</div>


<?php echo $this->element('/Sitekick/ajaxlogin'); ?>

<?php if(isset($editing)) { echo $this->element('/Sitekick/editing'); } ?>

<?php
if( $this->request->session()->check('updatemessages') ) {
    echo $this->element('/Sitekick/updatemessages', ['messages' => $this->request->session()->read('updatemessages')]);
}
?>



</body>
</html>