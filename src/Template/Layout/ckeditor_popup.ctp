<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="noindex, nofollow">

    <title>Sitekick Popup</title>

    <?php
    echo $this->Html->css('sitekick/icons');
    echo $this->Html->css('sitekick/bootstrap');
    echo $this->Html->css('sitekick/bootstrap-sitekick');
    echo $this->Html->css('sitekick/navbar');
    echo $this->Html->css('sitekick/animate');
    echo $this->Html->css('sitekick/switchbuttons');
    echo $this->Html->css('sitekick/loaders.min');

    echo $this->Html->script('jquery.min');
    echo $this->Html->script('sitekick/bootstrap');

    echo $this->fetch('script');
    echo $this->fetch('css');
    ?>
</head>
<body>
    <?php echo $this->fetch('content'); ?>
</body>
</html>
