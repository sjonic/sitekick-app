<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Sitekick</title>
	<?php echo $this->element('/Sitekick/header'); ?>
</head>

<body class="body_login">


<div class="container">


	<?php echo $this->Flash->render(); ?>

	<div class="form-signin _error-table ">

		<p>
            <?php echo  $this->fetch('file'); ?>
			<?php echo  $this->fetch('content'); ?>
		</p>


		<?php echo $this->Html->image( '/img/sitekick/template/error_avatar.png', [ 'class' => 'img-circle' ] ); ?>

		<br/>
		<br/>

		<?php echo $this->Html->link('Terug naar het dashboard','/sitekick',['escape' => false,'class' => 'btn btn-lg btn-primary']); ?>

        <br/>
        <br/>

	</div>

</div>
<span class="call-login"><?php echo __('Hulp nodig? Bel 030 - 687 55 06.'); ?></span>
</body>
</html>

