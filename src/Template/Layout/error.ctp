<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<meta name="description" content="">
	<meta name="author" content="">

	<title>Sitekick</title>
	<?php echo $this->element('/Sitekick/header'); ?>
</head>

<body>

<div id="container_dashboard">

	<?php echo $this->Flash->render(); ?>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
	</div>
</div>

<!-- FONTS -->
<?php echo $this->fetch('scriptBottom'); ?>

</body>
</html>



