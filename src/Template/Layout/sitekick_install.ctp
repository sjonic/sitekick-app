<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sitekick</title>
    <?php echo $this->element('Sitekick/header'); ?>
    <meta name="robots" content="noindex, nofollow">
</head>

<script>var sitekickUrl = '<?php echo $this->Url->build('/',true); ?>sitekick/';</script>
<script>var websiteUrl = '<?php echo $this->Url->build('/',true); ?>';</script>

<body id="sitekick" class="body_login">


<div class="container">
    <div style="padding: 5rem; border-radius: 1rem; background: white; text-align: left; margin: 2rem auto;">
        <?php echo $this->Flash->render(); ?>
        <?php echo  $this->fetch('content'); ?>

        <div class="text-center">
            <br><br>
            <a href="/" class="btn btn-primary">Terug naar de website</a><br>
        </div>
    </div>

</div>


<script type="text/javascript" src="//use.typekit.net/rqw7htt.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


<?php echo $this->fetch('scriptBottom'); ?>
</body>
</html>