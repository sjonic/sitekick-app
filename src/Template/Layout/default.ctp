<?php echo $this->Html->docType('html5'); ?>

<html lang="nl">
<head>
	<title>
		<?php if(isset($seo_title)) { echo $seo_title; }else{ echo $meta_title . " | ".$settings['Base']['site_name']; }?>
	</title>

	<?php if(isset($nofollow) or isset($settings['Base']['test_environment'])): ?>
		<meta name="robots" content="noindex, nofollow">
	<?php endif;?>

	<?php echo $this->Html->charset(); ?>
	<?php echo $this->Html->meta('viewport', 'width=device-width, initial-scale=1'); ?>
	<?php if(isset($meta_description)) { echo $this->Html->meta('description', $meta_description); } ?>
	<?php echo $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']); ?>

	<?php
		if(isset($authUser)) echo $this->element('/Sitekick/header_front');

		echo $this->Html->css([
			'bootstrap',
			'global',
			'../fonts/font-awesome.min',
			'http://fonts.googleapis.com/css?family=Roboto:400,300,700',
			'http://fonts.googleapis.com/css?family=Roboto+Slab:300,400'
		]);

		echo $this->Html->script([
			'jquery.min',
			'jquery.touchSwipe.min',
			'jquery.scrolly.min',
			'jquery.mixitup.min',
			'global'
		]);

	?>
	<?php if(isset($settings["Base"]["analytics"]) && !empty($settings["Base"]["analytics"])): ?>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', '<?php echo $settings["Base"]["analytics"]; ?>', 'auto');
			ga('send', 'pageview');

		</script>
	<?php endif; ?>
</head>
<body>




	<!-- Sitekick admin bar -->
	<?php if(isset($authUser)) echo $this->element('/Sitekick/navbar'); ?>


	<header class="header container-fixed noSelect fixed absolute-fixed" role="banner">
		<div class="container">
			<div id="logo">
				<?php echo $this->Html->image('logo_geheel.svg', ['alt' => 'NOBEARS', 'id' => 'logo-full', 'url' => '/']); ?>
				<?php echo $this->Html->image('logo_s.svg', ['alt' => 'NOBEARS', 'id' => 'logo-s', 'url' => '/']); ?>
			</div>

			<nav class="main" role="navigation">
				<ul class="navigation">
					<?php if(isset($navigations)): ?>
					<?php foreach($navigations as $key => $navigation): ?>
					<li <?php if(!$navigation->display or !$navigation->active){ echo 'class="opacity"'; } ?>>
						<?php if($navigation->display && $navigation->active or (isset($isAuthorized) && $navigation->active)): ?>

                            <?php if($navigation->has_content): ?>
                                <?php echo $this->Html->link($navigation->name, ['controller' => 'Pagecontainers', 'action' => 'view', 'plugin' =>'Pages', $navigation->slug]); ?>
                            <?php else: ?>
                                <?php echo $this->Html->link($navigation->name, [''],['escape' => true,'onclick' => 'return false;']); ?>
                            <?php endif; ?>

							<?php if(isset($navigation->children) && !empty($navigation->children)): ?>

							<ul class="dd-list">
								<?php foreach($navigation->children as $child): ?>
									<?php if($child->display && $child->active or (isset($isAuthorized) && $child->active)): ?>
										<li <?php if(!$child->active){ echo 'class="opacity"'; } ?>>
                                            <?php if($child->has_content): ?>
                                                <?php echo $this->Html->link($child->name, ['controller' => 'Pagecontainers', 'action' => 'view', 'plugin' =>'Pages', $child->slug]); ?>
                                            <?php else: ?>
                                                <?php echo $this->Html->link($child->name, [''],['escape' => true,'onclick' => 'return false;']); ?>
                                            <?php endif; ?>
                                        </li>
									<?php endif; ?>
									<?php endforeach; ?>
							</ul>
							<?php endif; ?>
						<?php endif; ?>


					</li>
					<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</nav>


			<i class="fa fa-bars menu-trigger"></i>
		</div>
	</header>


	<main role="main">
		<?php echo $this->Flash->render(); ?>
		<?php echo $this->fetch('content'); ?>
	</main>


	<footer class="container-fluid">
		<div class="container"><?php echo __('Ontwikkeld door'); ?> <a href="https://nobears.nl" target="_blank">NOBEARS</a></div>
	</footer>

	<?php echo $this->Element('testenv'); ?>

<?php echo $this->fetch('scriptBottom'); ?>
</body>
</html>
