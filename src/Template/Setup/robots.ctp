User-agent: *
Disallow: /test
Disallow: /cgi-bin/
Disallow: /sitekick_library/
Disallow: /fonts/
Disallow: /easter/
Disallow: /pacman/
Disallow: /sitekick-forms/

<?php
if( !empty($extraSettings) ){
    foreach($extraSettings as $key=>$setting){
        echo "{$key}: {$setting}";
    }
} ?>

sitemap: <?= trim($this->Url->build('/', true), '/') ?>/sitemap.xml