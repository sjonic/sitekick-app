<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Console;

use Cake\Core\App;
use Cake\Core\Configure;
use Composer\Script\Event;

/**
 * Provides installation hooks for when this application is installed via
 * composer. Customize this class to suit your needs.
 */
class Installer
{

    /**
     * Does some routine installation tasks so people don't have to.
     *
     * @param Composer\Script\Event $event The composer event object.
     * @return void
     */
    public static function postInstall(Event $event)
    {
        $io = $event->getIO();

        $rootDir = dirname(dirname(__DIR__));
//        static::createAppConfig($rootDir, $io);
        static::setTmpPermissions($rootDir, $io);
//        static::setSecuritySalt($rootDir, $io);
        static::copySitekickAssets( $io );
    }

    /**
     * Run after composer update
     * @param Event $event
     */
    public static function postUpdate(Event $event)
    {
        $io = $event->getIO();
        static::copySitekickAssets( $io );
    }


    /**
     * Create the Config/app.php file if it does not exist.
     *
     * @param string $dir The application's root directory.
     * @param Composer\IO\IOInterface $io IO interface to write to console.
     * @return void
     */
    public static function createAppConfig($dir, $io)
    {
        $appConfig = $dir . '/config/app.php';
        $defaultConfig = $dir . '/config/app.default.php';
        if (!file_exists($appConfig) && file_exists($defaultConfig)) {
            copy($defaultConfig, $appConfig);
            $io->write('Created `config/app.php` file');
        }
    }

    /**
     * Set globally writable permissions on the tmp directory.
     *
     * This is not the most secure default, but it gets people up and running quickly.
     *
     * @param string $dir The application's root directory.
     * @param Composer\IO\IOInterface $io IO interface to write to console.
     * @return void
     */
    public static function setTmpPermissions($dir, $io)
    {
        // Change the permissions on a path and output the results.
        $changePerms = function ($path, $perms, $io) {
            // Get current permissions in decimal format so we can bitmask it.
            $currentPerms = octdec(substr(sprintf('%o', fileperms($path)), -4));
            if (($currentPerms & $perms) == $perms) {
                return;
            }

            $res = chmod($path, $currentPerms | $perms);
            if ($res) {
                $io->write('Permissions set on ' . $path);
            } else {
                $io->write('Failed to set permissions on ' . $path);
            }
        };

        $walker = function ($dir, $perms, $io) use (&$walker, $changePerms) {
            $files = array_diff(scandir($dir), ['.', '..']);
            foreach ($files as $file) {
                $path = $dir . '/' . $file;

                if (!is_dir($path)) {
                    continue;
                }

                $changePerms($path, $perms, $io);
                $walker($path, $perms, $io);
            }
        };

        $worldWritable = bindec('0000000111');
        $walker($dir . '/tmp', $worldWritable, $io);
        $changePerms($dir . '/tmp', $worldWritable, $io);
    }

    /**
     * Set the security.salt value in the application's config file.
     *
     * @param string $dir The application's root directory.
     * @param Composer\IO\IOInterface $io IO interface to write to console.
     * @return void
     */
    public static function setSecuritySalt($dir, $io)
    {
        $config = $dir . '/config/app.php';
        $content = file_get_contents($config);

        $newKey = hash('sha256', $dir . php_uname() . microtime(true));
        $content = str_replace('__SALT__', $newKey, $content, $count);

        if ($count == 0) {
            $io->write('No Security.salt placeholder to replace.');
            return;
        }

        $result = file_put_contents($config, $content);
        if ($result) {
            $io->write('Updated Security.salt value in /config/app.php');
            return;
        }
        $io->write('Unable to update Security.salt value.');
    }

    /**
     * Copy Sitekick assets to root
     * @param $io
     */
    public static function copySitekickAssets( $io )
    {

        $copy = function ($src, $dst) use (&$copy) {
            $dir = opendir($src);
            @mkdir($dst);
            while (false !== ($file = readdir($dir))) {
                if (($file != '.') && ($file != '..')) {
                    if (is_dir($src . '/' . $file)) {
                        $copy($src . '/' . $file, $dst . '/' . $file);
                    } else {
                        copy($src . '/' . $file, $dst . '/' . $file);
                    }
                }
            }
            closedir($dir);
        };

        $rootDir = dirname(dirname(__DIR__));
        if( strpos($rootDir, 'vendor') !== false ){
            $destinationPath = '/../../..';
        } else {
            $destinationPath = '';
        }

        // get user-plugin directory
        $pluginDirs = array_filter(glob($rootDir . '/../../../plugins/*', GLOB_ONLYDIR), 'is_dir');
        $activePluginDir = '';
        foreach($pluginDirs as $dir){
            if( strpos($dir, 'Bootstrap') === false &&  strpos($dir, 'Shrink') === false  ){
                $activePluginDir = $dir;
                break;
            }
        }

        $copy($rootDir . '/bin', $rootDir . $destinationPath .'/bin');
        $copy($rootDir . '/webroot', $rootDir . $destinationPath .'/webroot');
        $copy($rootDir . '/config', $rootDir . $destinationPath . '/config');
        $copy($rootDir . '/src', $rootDir . $destinationPath . '/src');
        $copy($rootDir . '/plugins', $rootDir . $destinationPath . '/plugins');

        // copy root files
        copy( $rootDir . '/index.php', $rootDir . $destinationPath . '/index.php' );
        copy( $rootDir . '/.gitignore', $rootDir . $destinationPath . '/.gitignore' );

        // check if there is a client htaccess file
        if( !empty($activePluginDir) && file_exists($activePluginDir . '/config/htaccess.txt') ) {
            copy($activePluginDir . '/config/htaccess.txt', $rootDir . $destinationPath .'/webroot/.htaccess');
            $io->write('Plugin htaccess copied to ' . $rootDir . $destinationPath .'/webroot/.htaccess');
        }

        copy($rootDir . '/.htaccess', $rootDir . $destinationPath . '/.htaccess');


        // copy gulp files
        copy( $rootDir . '/gulpfile.js', $rootDir . $destinationPath . '/gulpfile.js' );
        copy( $rootDir . '/package.json', $rootDir . $destinationPath . '/package.json' );
        copy( $rootDir . '/package-lock.json', $rootDir . $destinationPath . '/package-lock.json' );


        // copy favicon
        if( !empty($activePluginDir) ){
            if( file_exists($activePluginDir . '/webroot/favicon.ico') ){
                copy( $activePluginDir . '/webroot/favicon.ico', $rootDir . $destinationPath . '/webroot/favicon.ico' );
                $io->write('Favicon from plugin-webroot copied');
            } elseif( file_exists($activePluginDir . '/webroot/img/favicon.ico') ){
                copy( $activePluginDir . '/webroot/img/favicon.ico', $rootDir . $destinationPath . '/webroot/favicon.ico' );
                $io->write('Favicon from plugin-image-directory copied');
            } elseif( file_exists($activePluginDir . '/webroot/img/favicon/favicon.ico') ) {
                copy( $activePluginDir . '/webroot/img/favicon/favicon.ico', $rootDir . $destinationPath . '/webroot/favicon.ico' );
                $io->write('Favicon from plugin-image-favicon-directory copied');
            }
        }

        if( function_exists('opcache_reset') ){
            opcache_reset();
            $io->write('OPcache reset complete');
        }


        $io->write('Sitekick core written to ' . $rootDir);
    }

}
