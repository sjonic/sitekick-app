<?php
/**
 * Static functions can be used against the whole app
 */

/**
 * Get translation string
 * @param string $string
 * @return mixed|string
 */
function t( $string = '', array $arguments = [] )
{
    $locale = \Cake\I18n\I18n::getLocale();
    $table = \Cake\ORM\TableRegistry::getTableLocator()->get('Translations');

    $entity = $table->find()->select(['id', 'translated_value'])->where(['locale' => $locale, 'original_value' => $string])->first();

    // value doesn't exist yet in the db
    if( empty($entity) ){
        $entity = $table->newEntity(['locale' => $locale, 'original_value' => $string]);
        $table->save($entity);
    }

    // Check if there is translation otherwise just return original string
    $translationValue = $string;
    if( !empty($entity->translated_value) ){
        $translationValue =  $entity->translated_value;
    }

    // Check if we need to replace stuff within the string
    if( !empty($arguments) ){
        foreach( $arguments as $key => $value ){
            // @todo replace with plurals for example {0,plural,=0{No records found} =1{Found 1 record} other{Found # records}}
            $translationValue = str_replace('{'.$key.'}', $value, $translationValue);
        }
    }

    return $translationValue;
}