<?php
/**
 * Command to build Sitekick Cache
 */
namespace App\Command;

use App\Controller\Component\SitekickCacheComponent;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Controller\ComponentRegistry;

class SitekickCacheShell extends Command
{
    /**
     * Set options
     * @param ConsoleOptionParser $parser
     * @return ConsoleOptionParser
     */
    protected function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser
            ->addOption('domain', [
                'help' => 'Set domain to access pages',
                'default' => 'http://sitekick.test/'
            ]);

        return $parser;
    }

    /**
     * Execute shell command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $sitekickCache = new SitekickCacheComponent(new ComponentRegistry());
        $sitekickCache->setIo($io);
        $sitekickCache->setDomain($args->getOption('domain'));
        $sitekickCache->build();
        $io->out('---- DONE BUILDING ----');
    }

}