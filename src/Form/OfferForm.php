<?php
namespace App\Form;

use Cake\Core\Configure;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use Cake\Network\Email\Email;

class OfferForm extends Form
{

	public $fieldsToValidate = ['name', 'message', 'email','address','city','zipcode','phone'];

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('name', 'string')
            ->addField('email', ['type' => 'string'])
            ->addField('phone', ['type' => 'string'])
            ->addField('address', ['type' => 'string'])
            ->addField('zipcode', ['type' => 'string'])
            ->addField('city', ['type' => 'string'])
            ->addField('phone', ['type' => 'string'])
            ->addField('message', ['type' => 'text']);
    }

    protected function _buildValidator(Validator $validator)
    {
		foreach($this->fieldsToValidate as $field){
			$validator->add($field,  'length', ['rule' => ['notBlank'], 'message' => 'Dit veld is verplicht']);
			if( $field == 'email' ){
				$validator->add('email', 'format', ['rule' => 'email', 'message' => 'Ongeldig e-mailadres']);
			}
		}
        return $validator;
    }

    protected function _execute(array $data)
    {

	    $to = [];
		if(Configure::read("mail_to")){
			$arrTo = explode(",", Configure::read("mail_to"));
			foreach($arrTo as $receiver){
				$arrReceiver = explode("#",$receiver);
				$to[$arrReceiver[1]] = $arrReceiver[0];
			}
		}

	    $cc = [];
	    if(Configure::read("mail_cc")){
		    $arrTo = explode(",", Configure::read("mail_cc"));
		    foreach($arrTo as $receiver){
			    $arrReceiver = explode("#",$receiver);
			    $cc[$arrReceiver[1] ] = $arrReceiver[0];
		    }
	    }

	    $bcc = [];
	    if(Configure::read("mail_bcc")) {
		    $arrTo = explode( ",", Configure::read( "mail_bcc" ) );
		    foreach ( $arrTo as $receiver ) {
			    $arrReceiver = explode( "#", $receiver );
			    $bcc[$arrReceiver[1]] = $arrReceiver[0];
		    }
	    }

        //send e-mail to new user to setup password
        $email = new Email();
		$settings = json_decode(Configure::read("settings"), true);
		$data['settings'] = $settings;

        $email->emailFormat("html")
            ->transport("Sitekick")
            ->from(Configure::read("mail_from_email"), Configure::read("mail_from_name"))
	        ->to($to)
            ->subject('Offerte aanvraag')
            ->viewVars($data)
            ->template( (!empty($settings['Base']['theme'])) ? $settings['Base']['theme'] . '.offer' : 'offer');


	    if(!empty($cc)){
		    $email->cc($cc);
	    }


	    if(!empty($bcc)){
		    $email->bcc($bcc);
	    }

        $email->send();

        // Send an email.
        return true;
    }
}