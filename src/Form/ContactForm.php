<?php
namespace App\Form;

use Cake\Core\Configure;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use Cake\Mailer\Email;

class ContactForm extends Form
{

	public $fieldsToValidate = ['name', 'message', 'email'];
	public $subject = 'Contactformulier';
	public $template = 'contact';
	public $attachments = [];


    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('name', 'string')
            ->addField('companyname', ['type' => 'string'])
            ->addField('email', ['type' => 'string'])
            ->addField('phone', ['type' => 'string'])
            ->addField('message', ['type' => 'text']);
    }

    protected function _buildValidator(Validator $validator)
    {
		foreach($this->fieldsToValidate as $field){
			$validator->add($field,  'length', ['rule' => ['notBlank'], 'message' => 'Dit veld is verplicht']);
			if( $field == 'email' ){
				$validator->add('email', 'format', ['rule' => 'email', 'message' => 'Ongeldig e-mailadres']);
			}
		}
        return $validator;
    }

	/**
     * @param array $data
     * @return bool
     */
    protected function _execute(array $data)
    {
	    $to = [];
		if(Configure::read("mail_to")){
			$arrTo = explode(",", Configure::read("mail_to"));
			foreach($arrTo as $receiver){
				$arrReceiver = explode("#",$receiver);
				if( !empty($arrReceiver[1]) ){
				    $to[$arrReceiver[1]] = $arrReceiver[0];
                } else {
				    $to[$arrReceiver[0]] = $arrReceiver[0];
                }
			}
		}

	    $cc = [];
	    if(Configure::read("mail_cc")){
		    $arrTo = explode(",", Configure::read("mail_cc"));
		    foreach($arrTo as $receiver){
			    $arrReceiver = explode("#",$receiver);
			    $cc[$arrReceiver[1] ] = $arrReceiver[0];
		    }
	    }

	    $bcc = [];
	    if(Configure::read("mail_bcc")) {
		    $arrTo = explode( ",", Configure::read( "mail_bcc" ) );
		    foreach ( $arrTo as $receiver ) {
			    $arrReceiver = explode( "#", $receiver );
			    $bcc[$arrReceiver[1]] = $arrReceiver[0];
		    }
	    }

        //send e-mail to new user to setup password
        $email = new Email();
		$settings = json_decode(Configure::read("settings"), true);
		$data['settings'] = $settings;

        $email->emailFormat("html")
            ->transport("Sitekick")
            ->from(Configure::read("mail_from_email"), Configure::read("mail_from_name"))
	        ->to($to)
            ->subject($this->subject)
            ->viewVars($data)
            ->template( (!empty($settings['Base']['theme'])) ? $settings['Base']['theme'] . '.' . $this->template : $this->template);


	    if(!empty($cc)){
		    $email->addCc($cc);
	    }


	    if(!empty($bcc)){
		    $email->addBcc($bcc);
	    }

		// add attachments
	    if( !empty($this->attachments) ){
	        $email->setAttachments($this->attachments);
        }
		
        $email->send();

        // Send an email.
        return true;
    }
}