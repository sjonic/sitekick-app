<?php

namespace App\Mailer\Transport;

use Cake\Core\Configure;
use Cake\Mailer\AbstractTransport;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;


class SitekickTransport extends AbstractTransport
{

    protected $apiUrl = 'http://api.sjonic.nl/';
//    protected $apiUrl = 'http://api.sjonic.nl/test/';

    /**
     * @param Email $email
     * @return string|null
     */
    public function send(Email $email)
    {
        // get view vars
        $vars = $email->getViewVars();

        //from
        $from = $email->getFrom();
        $from_email = key($from);
        $from_name = $from[key($from)];

        $attachments = $email->getAttachments();
        $encodedAttachments = [];

        $settings = json_decode(Configure::read('settings'));
        $appName = (!empty($settings->Base->theme)) ? $settings->Base->theme : env('APP_NAME', 'Sitekick');

        if (is_array($attachments)) {
            foreach ($attachments as $filename => $filedata) {

                $file = array();
                $file['type'] = $filedata['mimetype'];
                $file['name'] = $filename;
                $file['content'] = base64_encode(@file_get_contents($filedata['file']));
                $encodedAttachments[] = $file;

            }
        }

        $mails = TableRegistry::get('Mails');

        # Get existing mail
        if (isset($email->viewVars['mail_id'])) {
            $mailEntity = $mails->get($email->viewVars['mail_id']);
        } else {
            $mailEntity = $mails->newEntity();
        }

        # Save to database
        $mailData = [
            'mail_to' => json_encode($email->getTo()),
            'to_email' => json_encode($email->getTo()),
            'to_name' => json_encode($email->getTo()),
            'cc' => json_encode($email->getCc()),
            'bcc' => json_encode($email->getBcc()),
            'reply_to' => $email->getReplyTo(),
            'from_name' => $from_name,
            'from_email' => $from_email,
            'subject' => $email->getOriginalSubject(),
            'message' => $email->message("html"),
            'type' => "mail",
            'raw_data' => json_encode($vars),
        ];

        $mails->patchEntity($mailEntity, $mailData);
        $mails->save($mailEntity);

        $sitekickData = [
            'to' => $email->getTo(),
            'cc' => $email->getCc(),
            'bcc' => $email->getBcc(),
            'reply_to' => $email->getReplyTo(),
            'from_name' => $from_name,
            'from_email' => $from_email,
            'subject' => $email->getSubject(),
            'message' => $email->message("html"),
            'attachments' => $encodedAttachments,
            'mail_id' => $mailEntity->id,
            'mandrill' => $appName
        ];


        // set Sitekick-Forms ID
        if (!empty($vars['forminfo']['id'])) {
            $mailData['form_id'] = $vars['forminfo']['id'];
        }

        $sitekickApiConnection = new \Cake\Http\Client();
        $response = $sitekickApiConnection->post($this->apiUrl . "mandrill/send/838909cfa25823873dd2e0089321a2af/mandrill", ['data' => json_encode($sitekickData)]);

        if ($response->isOk()) {
            $mailData['sent_to_api'] = 1;
            $mailData['mandrill_id'] = $response->getStringBody();
            $mailEntity = $mails->patchEntity($mailEntity, $mailData);
            $mails->save($mailEntity);

            return $response->getStringBody();
        } else {
            $message = "Het is niet gelukt om een mail vanuit " . $appName . " te versturen.";
            mail("development@nobears.nl", "Sitekick - " . $appName . ": Mail versturen mislukt", $message);
            // send to slack
            exec('curl -X POST -H \'Content-type: application/json\' --data \'{"text":"' . $message . '"}\' https://hooks.slack.com/services/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');
        }

        return null;
    }
}