<?php
namespace App\Controller;

use App\Controller\AppController;

class MinifyController extends AppController {

    /**
     * Create a single minified file
     * @param string $request
     */
    public function index( $request = '' )
    {
        require ROOT . '/vendor/autoload.php';
        $_GET['f'] = str_replace('f=', '', $request);
        $configDir = dirname(dirname(__DIR__)) . DS . 'config'. DS .'Minify';
        $app = new \Minify\App( $configDir );
        $app->runServer();
        exit;
    }
}