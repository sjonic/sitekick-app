<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use App\Utility\Browser;

class NextGenComponent extends Component {

    public $nextGenConfiguration = [
        'Edge' => [
            'minVersion' => 18,
            'extension' => 'webp'
        ],
        'Firefox' => [
            'minVersion' => 65,
            'extension' => 'webp'
        ],
        'Chrome' => [
            'minVersion' => 32,
            'extension' => 'webp',
        ],
        'Safari' => [
            'minVersion' => 5,
            'extension' => 'jp2',
        ],
        'Opera' => [
            'minVersion' => 19,
            'extension' => 'webp',
        ],
        'Android' => [
            'minVersion' => 4,
            'extension' => 'webp',
        ],

        /* iphone specific */
        'iPhone' => [
            'Safari' => [
                'minVersion' => 5,
                'extension' => 'jp2',
            ],
            'Chrome' => [
                'minVersion' => 32,
                'extension' => 'jp2',
            ],
        ]

    ];

    public $browserName = '';
    public $browserVersion = '';
    public $browserPlatform = '';

    public function initialize( array $config = [] )
    {
        parent::initialize($config);

        $browserCheck           = new Browser();
        $this->browserName      = $browserCheck->getBrowser();
        $this->browserVersion   = (float)$browserCheck->getVersion();
        $this->browserPlatform   = $browserCheck->getPlatform();
    }

    public function getBrowserName(){ return $this->browserName; }
    public function getBrowserVersion(){ return $this->browserVersion; }
    public function getBrowserPlatform(){ return $this->browserPlatform; }

    /**
     * Check if browser is compatible for next-gen images
     * @return bool
     */
    public function isCompatible()
    {
        // check if there is a configuration for a platform specific
        if( !empty($this->nextGenConfiguration[$this->browserPlatform][$this->browserName]) ){
            return (!empty($this->nextGenConfiguration[$this->browserPlatform][$this->browserName]['minVersion']) && $this->nextGenConfiguration[$this->browserPlatform][$this->browserName]['minVersion'] < $this->browserVersion);
        } else {
            return (!empty($this->nextGenConfiguration[$this->browserName]['minVersion']) && $this->nextGenConfiguration[$this->browserName]['minVersion'] < $this->browserVersion);
        }
    }

    /**
     * Get next-gen extension
     * @return |null
     */
    public function getExtension()
    {
        if( $this->isCompatible() ){
            if( !empty($this->nextGenConfiguration[$this->browserPlatform][$this->browserName]['extension']) ){
                return $this->nextGenConfiguration[$this->browserPlatform][$this->browserName]['extension'];
            } else {
                return $this->nextGenConfiguration[$this->browserName]['extension'];
            }
        }
        return null;
    }


}
