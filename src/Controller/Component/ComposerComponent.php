<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Model;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;


class ComposerComponent extends Component {


    /**
     * Get versions from composer
     *
     * @return array
     */
    public function getVersions() {
        $versions = [];
        if( file_exists( ROOT . '/composer.lock' ) ){
            $composerInfo = file_get_contents(ROOT . '/composer.lock');
            if( !empty($composerInfo) ){
                $composerInfo = json_decode($composerInfo);
                if( !empty($composerInfo->packages) ){
                    foreach($composerInfo->packages as $package){
                        if( strpos($package->name, 'sitekick') !== false ){
                            $versions[$package->name] = $package->version;
                        }
                    }
                }
            }
        }

        return $versions;
    }
	
}