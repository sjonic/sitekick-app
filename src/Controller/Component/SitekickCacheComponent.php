<?php

namespace App\Controller\Component;

use Cake\Cache\Cache;
use Cake\Console\ConsoleIo;
use Cake\Controller\Component;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Http\Client;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Utility\Xml;


class SitekickCacheComponent extends Component
{

    protected $io;
    protected $domain;

    /**
     * Return cache
     */
    public function view()
    {
        $request = Component::getController()->getRequest();
        if ($request->is(['post', 'put'])) {
            return false;
        }

        $cacheFile = new File(CACHE . "static/" . $this->getPageFileName(), false);
        if ($cacheFile->exists()) {
            echo $cacheFile->read();
            exit;
        }
    }

    /**
     * Write cache file
     * @param $content
     */

    public function write($content)
    {
        $request = Component::getController()->getRequest();

        if (!$request->isAjax()) {

            $search = array(
                '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
                '/[^\S ]+\</s',  // strip whitespaces before tags, except space
                '/(\s)+/s'       // shorten multiple whitespace sequences
            );

            $replace = array(
                '>',
                '<',
                '\\1'
            );

            // turn off JIT-stacklimit
            ini_set('pcre.jit', false);
            //  Removes multi-line comments and does not create
            //  a blank line, also treats white spaces/tabs
            $newContent = preg_replace('#^\s*//.+$#m', "", $content);
            if ($newContent !== null) {
                $content = $newContent;
            }
            // Strip all blank spaces and new lines
            $newContent = preg_replace($search, $replace, $content);
            if ($newContent !== null) {
                $content = $newContent;
            }
            // Add cache written copyright
            $now = new \DateTime('now', new \DateTimeZone('Europe/Amsterdam'));
            $content .= "<!-- Cache written by Sitekick (Powered by NOBEARS) on " . $now->format('Y-m-d H:i:s') . ". -->";


            $cacheFile = new File(CACHE . "static/" . $this->getPageFileName(), true);
            $cacheFile->write($content);
            $cacheFile->close();
        }
    }

    /**
     * Get page filename
     * @return string
     */
    public function getPageFileName()
    {

        $request = Component::getController()->getRequest();

        // Generate filename based on URL
        if (empty($request->url)) {
            $fileName = "index";
        } else {
            $fileName = str_replace("/", "-", $request->url);
        }
        if ($request->getQuery()) {

            $queryArray = [];
            foreach ($request->getQuery() as $k => $v) {

                if (is_array($v)) {
                    $v = implode("-", $v);
                }

                $queryArray[$k] = $k . "-" . $v;
            }

            $query = implode("-", $queryArray);
            $fileName = $fileName . "-" . $query;
        }

        $fileName = str_replace("--", "-", $fileName);
        $fileName = strtolower($fileName);
        $fileName = Text::slug($fileName);

        // Check for next-gen images support
        $nextGenComponent = new NextGenComponent(new \Cake\Controller\ComponentRegistry(), []);
        $browserParts = [
            $nextGenComponent->getBrowserPlatform(),
            $nextGenComponent->getBrowserName(),
        ];
        $browserParts[] = ($nextGenComponent->isCompatible()) ? $nextGenComponent->getExtension() : 'no_support';
        $browser = strtolower('_' . implode("_", $browserParts));

        // Check for type of device
        if ($request->isMobile() && $request->isTablet() == false) {
            $device = "_mobile";
        } elseif ($request->isMobile()) {
            $device = "_tablet";
        } else {
            $device = "";
        }

        $fileName = $fileName . $browser . $device . ".html";

        return $fileName;
    }

    /**
     * Delete file from cache
     * @param null $file
     */
    public function delete($file = null)
    {

        Cache::clear(false);

        $cacheFile = new File(CACHE . "static/" . $file, false);
        if ($cacheFile->exists()) {
            $cacheFile->delete();
        }

    }

    /**
     * Clear cache
     * @param boolean $force - if set to true the disable_cache_clear setting will be ignored
     */

    public function clear($force = false)
    {
        $disableCacheClear = (!empty(Component::getController()->settings['Base']['disable_cache_clear']));
        if ($force === true || !$disableCacheClear) {

            Cache::clear(false);

            $cacheFolder = new Folder(CACHE . "static");
            $cacheFolder->delete();

            $cacheFolder = new Folder(CACHE . "models");
            $cacheFolder->delete();

            $cacheFolder = new Folder(CACHE . "persistent");
            $cacheFolder->delete();

        }


    }

    /**
     * Set console interface
     * @param ConsoleIo $io
     */
    public function setIo(ConsoleIo $io)
    {
        $this->io = $io;
    }

    /**
     * Set domain to write cache for
     * @param string $domain
     */
    public function setDomain(string $domain = '')
    {
        $this->domain = $domain;
    }

    public function build()
    {

        // get sitemap URLs
        $sitemap = $this->getSitemap($this->domain);
        $ch = curl_init();

        $browsers = $this->getLatestBrowsers();

        foreach ($sitemap as $url) {
            foreach ($browsers as $name => $userAgent) {

                // Send to output interface if it exists
                if ($this->io) {
                    $this->io->out("Storing {$url} for {$name}");
                }

                // Run curl command to write file for specific browser
                curl_setopt_array($ch, array(
                    CURLOPT_URL => $url,
                    CURLOPT_USERAGENT => $userAgent,
                    CURLOPT_ENCODING => 'gzip, deflate',
                    CURLOPT_RESOLVE => ['sitekick.test:80:127.0.0.1'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTPHEADER => array(
                        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                        'Accept-Language: en-US,en;q=0.5',
                        'Connection: keep-alive',
                        'Upgrade-Insecure-Requests: 1',
                    ),
                ));
                curl_exec($ch);
            }
        }
        curl_close($ch);

        // write log
        $logFile = new File(TMP . 'cache/sitekick_cache_log.txt', true);
        $now = new \DateTime('now', new \DateTimeZone('Europe/Amsterdam'));
        $logFile->write($now->format('Y-m-d H:i:s'));

    }

    /**
     * Get URLs from website sitemap
     * @param string $domain
     * @return array
     */
    public function getSitemap(string $domain = '')
    {
        $urls = [];
        $sitemap = Xml::toArray(Xml::build(file_get_contents(Router::url($domain . 'sitemap.xml', true))));
        if (!empty($sitemap['urlset']['url'])) {
            foreach ($sitemap['urlset']['url'] as $url) {
                $subSitemap = Xml::toArray(Xml::build(file_get_contents($url['loc'])));
                if (!empty($subSitemap['urlset']['url'][0])) {
                    foreach ($subSitemap['urlset']['url'] as $pageUrl) {
                        $urls[$pageUrl['loc']] = $pageUrl['loc'];
                    }
                } else {
                    $urls[$subSitemap['urlset']['url']['loc']] = $subSitemap['urlset']['url']['loc'];
                }
            }
        }

        return $urls;
    }

    /**
     * Get latest browser agents for each browser on desktop and mobile
     * @doc see: https://www.whatismybrowser.com/guides/the-latest-user-agent/
     * @return array
     */
    public function getLatestBrowsers()
    {
        $return = [];
        $client = new Client();
        $response = $client->get('http://api.sjonic.nl/browsers/all.json?key=838909cfa25823873dd2e0089321a2af');
        if ($response->isOk()) {
            $data = $response->getJson();

            // build a one-way array
            if (!empty($data)) {
                foreach ($data as $browser) {
                    if (!empty($browser['user_agents_list'])) {
                        foreach ($browser['user_agents_list'] as $groupName => $userAgents) {
                            foreach ($userAgents as $userAgent) {
                                $return[$browser['name'] . '_' . $browser['type'] . '_' . $groupName] = $userAgent;
                            }
                        }
                    }
                }
            }
        }

        return $return;
    }

    /**
     * Get last cache build date
     * @return \DateTime|false
     */
    public function getLastBuild()
    {
        if( file_exists(TMP . 'cache/sitekick_cache_log.txt') ){
            $file = new File(TMP . 'cache/sitekick_cache_log.txt');
            return date_create_from_format('Y-m-d H:i:s', $file->read());
        } else {
            return false;
        }

    }
}