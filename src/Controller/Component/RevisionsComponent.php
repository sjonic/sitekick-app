<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Model;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;


class RevisionsComponent extends Component {


	/**
	 * Find all revisions 
	 * @params $plugin, $controller, $action, $foreign_key
	 *
	 * @return $revisions
	 */
	public function find($plugin, $controller, $action, $foreign_key, $additional, $lang) {
		
		$model = TableRegistry::get('Revisions');
		$revisions = $model->find();
        $revisions->where(['plugin' => $plugin, 'controller' => $controller, 'foreign_key' => $foreign_key]);
        if( $additional == true ) {
            $revisions->andWhere(['additional' => $additional]);
        }
        if( $lang == true ) {
            $revisions->andWhere(['lang' => $lang]);
        }
        $revisions->join([
            'table' => 'users',
            'alias' => 'user',
            'type' => 'LEFT',
            'conditions' => 'user.id = Revisions.user_id',
        ]);
        $revisions->order(['date desc']);
        $revisions->select(['id', 'user_id', 'url', 'date', 'user.firstname', 'user.lastname']);

		if( !empty($revisions) ) {
			$revisions = $revisions->toArray();
		}
		return $revisions;
	}


	/**
	 * Add new revision
	 * @params $user_id, $plugin, $controller, $action, $foreign_key, $url, $postdata
	 *
	 * @return void
	 */
	public function add($user_id, $plugin, $controller, $action, $foreign_key, $additional, $url, $postdata, $method) {

	    if( strtolower($action) == 'add' ) {

	        # Update URL
	        $url = 'sitekick/' . strtolower($plugin) . '/edit/' . $foreign_key;

	        # Register ID in data
            if( strtolower($plugin) == 'pages' ) {
                $postdata['Navigations']['id'] = $foreign_key;
            }
            else {
                $postdata[ucfirst($plugin)]['id'] = $foreign_key;
            }

        }

		$model = TableRegistry::get('Revisions');

		$revision 					= $model->newEntity();
		$revisions['user_id'] 		= $user_id;
        $revisions['lang'] 		    = $this->request->session()->read('Config.language_sitekick.locale');
		$revisions['plugin'] 		= $plugin;
		$revisions['controller'] 	= $controller;
		$revisions['action'] 		= $action;
		$revisions['foreign_key'] 	= $foreign_key;
        $revisions['additional'] 	= $additional;
        $revisions['url'] 			= $url;
		$revisions['postdata'] 		= serialize($postdata);
        $revisions['method'] 		= $method;
        $revision = $model->patchEntity($revision, $revisions);
		$model->save($revision);	

		return true;
	}
	
}