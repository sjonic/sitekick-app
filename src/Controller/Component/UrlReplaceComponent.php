<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Model;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;


class UrlReplaceComponent extends Component {


	/**
	 * Replace urls in content by new url
	 * @param $oldUrl
	 * @param $newUrl
	 *
	 * @return $count
	 */
	public function replace($oldUrl, $newUrl) {

		$count = 0;
		if( $oldUrl != $newUrl && !empty($oldUrl) && !empty($this->request->getSession()->read('Config.language_sitekick.locale')) ) {

            $locale = $this->request->getSession()->read('Config.language_sitekick.locale');

			$model = TableRegistry::get('i18n');

			# Get all records where old url was being used
			$items = $model->find()
				->where(['content LIKE' => '%' . $oldUrl . '%'])		
				->andWhere(['field IS NOT' => 'slug'])
				->andWhere(['field IS NOT' => 'name'])
                ->andWhere(['locale' => $locale]);
				
			foreach($items as $item){
				
				# Find and replace hyperlinks
				$re = '/<a href=\\"([^\\"]*)' . preg_quote($oldUrl, '/') . '\\">(.*)<\\/a>/iU';
				$str = $item->content;

				preg_match_all($re, $str, $matches);
				if(!empty($matches[0])) {
					
					$content = str_replace($matches[1][0] . $oldUrl, $matches[1][0] . $newUrl, $item->content);
					$id = $item->id;
						
					# update old url with new url
					$query = $model->query();
					$query->update()
						->set(['content' => $content])
						->where(['id' => $id])
						->execute();
                    
                    $count++;
					
				}
			}

			
			# add to redirects
			if( $oldUrl != $newUrl ){
				$redirectsTable   = TableRegistry::getTableLocator()->get("Redirects");
				$check = $redirectsTable->find()->where(['from_url' => $oldUrl, 'to_url' => $newUrl])->first();
				if( empty($check) ){
				    $redirect = $redirectsTable->newEntity(['from_url' => $oldUrl, 'to_url' => $newUrl]);
				    $redirectsTable->save($redirect);
                }
			}

		}
		return $count;
	}

    /**
     * Delete redirects from a specific page
     *
     * @param $slug
     */
    public function cleanRedirect( $slug )
    {
        $redirectsTable   = TableRegistry::get("Redirects");
        if( $entities = $redirectsTable->findByFromUrl( $slug ) ){
            foreach($entities as $entity){
                $redirectsTable->delete($entity);
            }
        }
    }
	
	/**
	 * Find items with given internal link
	 * @param $url
	 *
	 * @return bool
	 */
	public function findInternalLinks($url) {

		$model = TableRegistry::get('i18n');

		# Get all records where old url was being used
		$items = $model->find()
			->where(['content LIKE' => '%' . $url . '%'])		
			->andWhere(['field IS NOT' => 'slug'])
			->andWhere(['field IS NOT' => 'name']);			
				

		$pages = array();					
		foreach($items as $item){	
				
			# Find hyperlinks
			$re = '/<a href=\\"([^\\"]*)' . $url . '\\">(.*)<\\/a>/iU'; 
			$str = $item->content; 
			preg_match_all($re, $str, $matches);
			if(!empty($matches[0])) {
						
				# Get URL of page where hyperlink is used		
				if($item->model == 'PagecontainerBlockItems') {	
				
					$BlockItems     = TableRegistry::get('Pages.PagecontainerBlockItems');
					$Blocks         = TableRegistry::get('Pages.PagecontainerBlocks');
					$Containers     = TableRegistry::get('Pages.Pagecontainers');
					$Items          = TableRegistry::get('Navigations');

					if( $elem = $BlockItems->findById($item->foreign_key)->first() ){
						if( $elem = $Blocks->findById($elem->pagecontainer_block_id)->first() ){
							if( $elem = $Containers->findById($elem->pagecontainer_id)->first() ){
								if( $elem = $Items->findById($elem->navigation_id)->first() ){
									$elem->plugin = 'Pages';
									$pages[] = array(
										'name' => $elem->name,
										'url' => Router::url(array('prefix'=>'sitekick', 'plugin'=>'Pages', 'controller'=>'Pagecontainers', 'action'=>'edit', $elem->id), true)
									);
								}
							}
						}
					}
				}			
			}
		}
		
		return $pages;

	}

	public function cleanUrl($url) {		
		$url = strtolower($url);
		$url = str_replace("'", '', $url);    
		$url = str_replace('%20', '-', $url); 
		$url = str_replace('&', '-', $url);		
		$url = str_replace(':', '_', $url);
		$url = str_replace(' ', '-', $url);      
		$url = preg_replace('~[^-a-z/0-9_]+~', '', $url);
		$url = preg_replace('!--+!', '-', $url);
		
		return $url;	
	}			
}