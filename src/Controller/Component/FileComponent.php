<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Model;
use Cake\ORM\TableRegistry;
use Imagick;

class FileComponent extends Component
{

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    /**
     * Upload file in files/plugin directory with validation options
     * @param $file
     * @param $options
     *
     * @return bool
     * @throws \ImagickException
     */
    public function upload($file, $options = null)
    {
        $files = TableRegistry::get('Files');

        //set error default on false
        $error = false;

        $plugin = "media";
        if (isset($options["plugin"])) {
            $plugin = $options["plugin"];
        }

        // create original directory
        $dirOriginal = new Folder(WWW_ROOT . 'files/original/', true, 0755);

        //if validate is given validate file types
        if (isset($options['validate'])) {
            $error = true;
            foreach ($options['validate'] as $validateKey => $validateValue) {
                if (strpos($file["type"], $validateValue)) {
                    $error = false;
                }
            }
        }

        //no errors so upload file
        if (!$error) {

            $uploadFile = new File($file["tmp_name"]);

            if (!empty($uploadFile->name)) {

                $filename = $this->rename($file['name']);
                $filename = $this->validateFilename($dirOriginal->path, $filename);
                if (isset($options['filename'])) {
                    $filename = $options["filename"];
                }

                $uploadFile->copy($dirOriginal->path . "/" . $filename);


                // create interlaced
                if (in_array(pathinfo($filename, PATHINFO_EXTENSION), ['jpg', 'jpeg'])) {
                    $image = new Imagick($dirOriginal->path . "/" . $filename);
                    $image->setInterlaceScheme(Imagick::INTERLACE_PLANE);

                    // Rotate image acording to image EXIF rotation
                    $orientation = $image->getImageOrientation();

                    switch ($orientation) {
                        case imagick::ORIENTATION_BOTTOMRIGHT:
                            $image->rotateimage("#000", 180); // rotate 180 degrees
                            break;

                        case imagick::ORIENTATION_BOTTOMLEFT :
                            $image->rotateimage("#000", 180);
                            $image->flopImage();
                            break;

                        case imagick::ORIENTATION_RIGHTTOP:
                            $image->rotateimage("#000", 90); // rotate 90 degrees CW
                            break;

                        case imagick::ORIENTATION_RIGHTBOTTOM:
                            $image->rotateimage("#000", -90);
                            $image->flopImage();
                            break;

                        case imagick::ORIENTATION_TOPRIGHT:
                            $image->flopImage();
                            break;

                        case imagick::ORIENTATION_LEFTBOTTOM:
                            $image->rotateimage("#000", -90); // rotate 90 degrees CCW
                            break;

                        case imagick::ORIENTATION_LEFTTOP:
                            $image->rotateimage("#000", 90);
                            $image->flopImage();
                            break;
                    }

                    // Change image orientation metadata
                    $image->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
                    $image->stripImage();
                    $image->writeImage($dirOriginal->path . DS . $filename);
                    $src = $dirOriginal->path . DS . $filename;
                    $this->compressImage($src);
                }

                $dirOriginal->chmod($dirOriginal->path, 0777);

                $alt = $filename;
                if (isset($options['alt'])) {
                    $alt = $options["alt"];
                }

                $type = 'image';
                if (!in_array(pathinfo($filename, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'gif', 'svg', 'tiff', 'png'])) {
                    $type = 'file';
                }
                if (isset($options['type'])) {
                    $type = $options["type"];
                }

                $data = '';
                if (isset($options['options'])) {
                    $data = $options['options'];
                }

                $parent_id = null;
                if (isset($options['id'])) {
                    $parent_id = $options['id'];
                }

                $locale = NULL;
                if(isset($options['locale'])) {
                    $locale = $options['locale'];
                }

                // upload default thumbnail
                if (in_array(pathinfo($filename, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'gif'])) {
                    $uploadsThumbnailDir = new Folder(WWW_ROOT . 'files/' . $plugin . '/thumbnails/', true, 0755);
                    $image = new Imagick($dirOriginal->path . "/" . $filename);
                    $image->cropThumbnailImage(250, 250);
                    $image->writeImage($uploadsThumbnailDir->path . "/" . $filename);
                    $this->compressImage($uploadsThumbnailDir->path . "/" . $filename);
                }

                $dbFile = $files->newEntity([
                    'file' => $filename,
                    'filesize' => @filesize(WWW_ROOT . 'files/original/' . $filename),
                    'plugin' => $plugin,
                    'parent_id' => $parent_id,
                    'type_field_id' => (!empty($options['type_field_id'])) ? $options['type_field_id'] : null,
                    'alt' => $alt,
                    'type' => $type,
                    'options' => $data,
                    'user_id' => $this->getController()->Auth->user('id'),
                    'session_id' => session_id(),
                    'locale' => $locale
                ]);

                $files->save($dbFile);

                // try to delete the nextgen images
                $this->deleteNextGen($dbFile);

                return $dbFile;

            }

            return false;


        }

        return false;

    }


    /**
     * Delete a file
     * If there is no record by the filename
     * Delete it from the server too
     *
     * @param string $plugin
     * @param $id
     * @param boolean $force
     * @return boolean
     */
    public function delete($plugin = "media", $id, $force = false)
    {

        $files = TableRegistry::get('Files');
        $dir = new Folder(WWW_ROOT . DS . 'files');

        $where = [
            'Files.id' => $id
        ];
        if ($plugin != '') {
            $where['plugin'] = $plugin;
        }



        if ($file = $files->find()->where($where)->first()) {

            $this->deleteNextGen( $file );


            // force to delete a file, also delete the connections
            if( $force === true ){

                $filesGroupByFilename = $files->findByFile($file->file);
                if( $filesGroupByFilename && $filesGroupByFilename->count() == 1 ){
                    foreach($filesGroupByFilename as $file){
                        if ($files->delete($file)) {
                            if ($filesInDir = $dir->findRecursive($file->file)) {
                                foreach ($filesInDir as $f) {
                                    $f = new File($f);
                                    $f->delete();
                                    $f->close();
                                }
                            }
                        }
                    }
                }

                // Disconnect image from plugin
                $files->delete($file);


            } else if (strtolower($file->plugin) == 'media' || $plugin == null) {

                // remove from DB
                if ($files->delete($file)) {

                    // check if there is another row with the same filename,
                    // if so we cannot delete the file from the server
                    $filesCheck = $files->findByFile($file->file)->count();

                    if ($filesCheck == 0) {
                        // remove from server
                        if ($filesInDir = $dir->findRecursive($file->file)) {
                            foreach ($filesInDir as $f) {
                                $f = new File($f);
                                $f->delete();
                                $f->close();
                            }
                        }
                    }
                } else {
                    return false;
                }
            } else {
                // Disconnect image from plugin
                $file->plugin = 'media';
                $file->parent_id = NULL;

                $files->save($file);
            }
            return true;

        }

        return false;
    }


    /**
     * Validate and convert a filename
     * @param $path
     * @param $filename
     * @param null $number
     *
     * @return mixed|string
     */
    public function validateFilename($path, $filename, $number = null)
    {

        //convert filename lowercase and without space
        $filename = strtolower($filename);
        $filename = str_replace(" ", "-", $filename);

        //check if exists else run same funtion again with number added
        if (file_exists($path . "/" . $filename)) {

            $arrExt = explode(".", $filename);
            $arrExt = array_reverse($arrExt);
            $ext = $arrExt[0];

            $number++;

            $filename = str_replace("." . $ext, "", $filename);
            $filename = str_replace("-" . ($number - 1), "", $filename);
            $filename = $filename . "-" . $number . "." . $ext;

            $filename = $this->validateFilename($path, $filename, $number);

        }

        return $filename;
    }

    /**
     * Rename a file method
     *
     * @param string $filename
     * @return mixed|string
     */
    public function rename($filename = '')
    {
        $filename = preg_replace('/^-+|-+$/', '', strtolower(preg_replace('/[^a-zA-Z0-9\.]+/', '-', $filename)));
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        if (str_replace(pathinfo($filename, PATHINFO_EXTENSION), '', $filename) == '.') {
            $filename = 'no_filename.' . $extension;
        }
        return $filename;
    }

    /**
     * Delete pending images by session_id
     * A pending image is an image wich have been uploaded but the page is never saved
     */
    public function cleanPendingImages()
    {

        $table = TableRegistry::get('Files');
        $table->deleteAll([
            'parent_id IS' => 0,
            'plugin NOT IN ("redactor", "media")'

        ]);
    }

    /**
     * Compress image
     * @param $path
     */
    public function compressImage($path)
    {
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        if( in_array($ext, ['gif', 'png']) ){
            exec('convert '.$path.' -strip '.$path);
        } else {
            exec('convert '.$path.' -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG ' . $path);
        }
    }

    /**
     * Delete next gen images
     * @param $file
     */
    public function deleteNextGen( $file )
    {
        if( !empty($file->file) ){
            $dir = new Folder(WWW_ROOT . DS . 'files');
            foreach(['webp','jp2'] as $nextgenExt) {
                $res  = $dir->findRecursive($file->file . '.' . $nextgenExt);
                if( !empty($res) ){
                    foreach ($res as $f) {
                        @unlink($f);
                    }
                }
            }
        }
    }

}