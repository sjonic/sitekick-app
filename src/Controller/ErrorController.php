<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Network\Response;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Seo\Controller\Sitekick\LinkController;

class ErrorController extends AppController
{
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->loadComponent('RequestHandler');

        $db = ConnectionManager::get('default');
        $collection = $db->getSchemaCollection();
        $tables = $collection->listTables();

        if( count($tables) == 0 ) {
            header("Location:" . Router::url(['controller' => 'Setup', 'action' => 'install', 'plugin' => false]));
            exit();
        }


    }

    /**
     * Render error messages
     *
     * @param Event $event
     * @return \Cake\Http\Response|Response|void|null
     */
    public function beforeRender(Event $event)
    {

        $statusCode = $event->getSubject()->getResponse()->getStatusCode();
        if( empty($statusCode) ){
            $statusCode = 500;
        }

        // Register redirects table
        $request        = Router::getRequest();
        $redirects      = TableRegistry::getTableLocator()->get('Redirects');
        $linkController = new LinkController();

        // Check if redirect exists, than log to DB and redirect
        if($statusCode > 400 && $statusCode < 500 && !empty($linkController)) {

            if ($redirect = $redirects->findByFromUrl($request->getPath())->first()) {
                if ($redirect->from_url != $redirect->to_url) {

                    // Redirect exists, log to DB
                    $linkController->logRedirect($request, $redirect);

                    // Set the redirect headers
                    if (strpos($redirect->to_url, 'http') !== false) {
                        $url = $redirect->to_url;
                    } else {
                        $url = Router::url('/' . $redirect->to_url, true);
                    }

                    $response = new Response();
                    $response->withHeader('Location', $url);
                    $response->withStatus($redirect->type);
                    return $response;

                }
            }

            // Save page to database
            $exception = $event->getSubject()->viewVars['error'];
            if( empty($exception) ){
                $exception = new \Exception();
            }
            $linkController->logBrokenLinks($request, $exception);

        // handle all other errors
        } else {

            $exception = $event->getSubject()->viewVars['error'];
            if( empty($exception) ){
                $exception = new \Exception();
            }

            //save it to the database
            $errors = TableRegistry::getTableLocator()->get('Errors');
            $error = $errors->newEntity([
                'message' => $exception->getMessage(),
                'file'    => $exception->getFile(),
                'line'    => $exception->getLine()
            ]);

            $errors->save($error);

            //server vars for error log
            $serverVars = '';
            $whitelist = ['HTTP_USER_AGENT', 'HTTP_REFERER', 'HTTP_ACCEPT_LANGUAGE', 'SERVER_NAME', 'REMOTE_ADDR', 'REQUEST_METHOD', 'REQUEST_URI', 'APP_NAME'];
            foreach($_SERVER as $varKey => $varValue){
                if( in_array($varKey, $whitelist ) ) {
                    $serverVars .= '<strong>' . $varKey . ':</strong>' . $varValue . "<br>";
                }
            }

            $errorMessage = '';
            $errorMessage .= 'Er is een fout opgetreden bij <strong>"http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '"</strong><br><br>';
            $errorMessage .= '<strong>Foutmelding:</strong><br><br>' . $exception->getMessage() . '<br><br>';
            $errorMessage .= '<strong>Bestand:</strong><br><br>' . $exception->getFile() . '<br><br>';
            $errorMessage .= '<strong>Regel:</strong><br><br>' . $exception->getLine() . '<br><br>';
            $errorMessage .= '<strong>Server:</strong><br><br>'.$serverVars.'<br><br>';
            $errorMessage .= '<strong>Backtrace:</strong><br><br>'.nl2br($exception->getTraceAsString());

            if ( Configure::read('debug') == false ) {
                $email = new Email();
                $email->setFrom('dev@sjonic.nl')
                    ->setEmailFormat('html')
                    ->setTo('dev@sjonic.nl')
                    ->setSubject('Sitekick error melding [' . $_SERVER["SERVER_NAME"] . ']')
                    ->setTransport('Sitekick')
                    ->send($errorMessage);
            }
        }

    }
}