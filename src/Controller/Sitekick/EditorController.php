<?php
namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\Core\Configure;

class EditorController extends AppController {

    /**
     * Link method
     */
    public function link()
    {
        $this->viewBuilder()->setLayout('ajax');

        $settings = json_decode(Configure::read('settings'));

        $classAddon = (!empty($settings->Base->button_class)) ? $settings->Base->button_class : '';
        $classes = [
            '' => __('Geen'),
            'button ' . $classAddon  => __('Button')
        ];
        $extraClasses = (!empty($settings->Base->button_classes)) ? json_decode($settings->Base->button_classes, true) : '';
        if( !empty($extraClasses) ){
            $classes = array_merge($classes, $extraClasses);
        }

        $this->set(compact('classes'));
    }

}