<?php
namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\I18n\I18n;

class NavigationsController extends AppController {


	/**
	 * Load components
	 * @var array
	 */
	public $components = ['SitekickCache'];


    public function index() {

		//reset front referral
	    $session = $this->request->session();
	    $session->delete("front_referral");


	    $this->Navigations->addBehavior('Tree');
//	    $this->Navigations->recover();

        # Set locale data
        $this->Navigations->locale($this->request->session()->read('Config.language_sitekick.locale'));
        $navigations = $this->Navigations->find('threaded',['order' => 'lft','conditions' => ['temp' => 0]])->find('translations');

		$this->set('navigations', $navigations);

    }  
    
    /**
     * Save new ranking
     */	
    public function ranking() {

	    $this->viewBuilder()->layout('ajax');

        if ($this->request->is('post')) {   
            
			$JSON = $this->request->data['ranking'];
			$cleanJSON = json_decode($JSON,true);
						
			$this->newTree = array();
			$previousLeft = 0;
			foreach($cleanJSON as $key => $value) {
				$previousLeft = $this->recurseTree($cleanJSON[$key], $previousLeft, 0);
			}
			
            foreach($this->newTree as $value) {
		
				if(isset($value['id'])) {
			
					$navigation = $this->Navigations->get($value['id']);  
					
					$data['Navigations']['id'] = $value['id'];
					$data['Navigations']['parent_id'] = $value['parent_id'];
					$data['Navigations']['lft'] = $value['lft'];
					$data['Navigations']['rght'] = $value['rght'];					

					$this->Navigations->patchEntity($navigation, $data);
					$this->Navigations->save($navigation);

				}		
            }
			
        }

		//clear cache
		$this->SitekickCache->clear();

        $this->render(false);
        
    }

    /**
     * Update navigation data with ajax
     */
    public function update() {

	    $this->viewBuilder()->setLayout('ajax');

        # Set locale data
        I18n::setLocale( $this->request->getSession()->read('Config.language_sitekick.locale') );

        if ($this->request->is(['post'])) {            
            $navigation = $this->Navigations->get($this->request->getData('data.Navigations.id'));
            $this->Navigations->patchEntity($navigation, $this->request->getData('data'));
            $this->Navigations->save($navigation);

	        //clear cache
	        $this->SitekickCache->clear();
        }

        # Set locale User
        I18n::setLocale( $this->request->getSession()->read('Auth.User.locale') );

        $this->render(false);
        
    }    
	
    /**
     * Set lft, rght and parent_id
     */
	private function recurseTree($structure, $previousLeft, $parent) {	
				
		$indexed = array();                           
		$indexed['id'] = $structure['id'];     
		$indexed['parent_id'] = $parent;
		$indexed['lft'] = $previousLeft + 1;

		$lastRight = $indexed['lft'];

		$i_count = 0;
		if (isset($structure['children'])) {
			$parent = $structure['id'];
			foreach ($structure['children'] as $a_child) {
				$lastRight = $this->recurseTree($structure['children'][$i_count],$lastRight, $parent);
				$i_count++;
			}
		}

		$indexed['rght'] = $lastRight + 1;     	// Set Right
	
		array_push($this->newTree, $indexed);   // Push onto stack
		
		return $indexed['rght'];       
	}		
	
}
?>