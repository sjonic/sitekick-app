<?php

namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Ifsnop\Mysqldump as IMysqldump;

class InfoController extends AppController
{
    public $components = ['Flash', 'RequestHandler', 'Composer'];

    /**
     * Overview
     */
    public function index()
    {
        $info = [];

        $info['PHP Versie'] = phpversion();
        $info['Imagick'] = extension_loaded('imagick');
        $info['Upload size'] = ini_get("upload_max_filesize");
        $info['Post size'] = ini_get('post_max_size');
        $info['Memory limit'] = ini_get('memory_limit');

        $info['ImagickAllowedFormats'] = '';
        if ($info['Imagick']) {
            $info['ImagickAllowedFormats'] = implode(' ', \Imagick::queryFormats('*'));
        }

        $this->set('info', $info);


        $this->set('versions', $this->Composer->getVersions());
        $this->set('migrations', $this->getMigrations());

    }

    public function runPatches()
    {
        $this->viewBuilder()->setLayout('ajax');
        $response = [];
        if ($this->request->getData('ids')) {
            $ids = $this->request->getData('ids');
            $this->loadModel('Migrations');

            $connection = ConnectionManager::get('default');

            foreach ($ids as $id) {
                $migration = $this->Migrations->get($id);

                if (strtolower($migration->plugin) != 'base') {
                    $path = Plugin::path(ucfirst($migration->plugin));
                } else {
                    $path = ROOT . DS;
                }

                $return = [
                    'file' => $migration->file,
                    'plugin' => $migration->plugin,
                    'path' => $path,
                ];

                if (file_exists($path . '/config/Schema/' . $migration->file)) {

                    $patch = file_get_contents($path . 'config/Schema/' . $migration->file);
                    try {
                        $connection->execute($patch);
                        $return['patch_response'] = "Executed";
                        $migration->executed = 1;
                        $this->Migrations->save($migration);

                    } catch (\PDOException $error) {
                        $return['patch_response'] = $error->getMessage();
                    }

                } else {
                    $return['error'] = 'Kan het bestand niet (meer) vinden: ' . $path . 'config/Schema/' . $migration->file;
                }

                $response[] = $return;
            }


        }

        $this->set('response', $response);
    }

    /**
     * Download MySQL dump for current Sitekick state
     * @return \Cake\Http\Response|null
     * @throws \Exception
     */
    public function dump()
    {
        $tables = ConnectionManager::get('default')->getSchemaCollection()->listTables();
        $blackListTables = ['seo_broken_links', 'seo_redirect_logs', 'seo_referrers', 'sessions', 'errors', 'mailing_lists', 'mails', 'orders', 'revisions', 'redirects', 'pageviews'];

        // remove black-listed tables
        foreach ($tables as $key => $table) {
            if (in_array($table, $blackListTables)) {
                unset($tables[$key]);
            }
        }

        // Make sure tmp folder exists
        $tmpFolder = new Folder(TMP, true);
        $filePath = TMP . '/dump.sql';

        $dump = new IMysqldump\Mysqldump('mysql:host=localhost;dbname=' . env('DATABASE_NAME'), env('DATABASE_USERNAME'), env('DATABASE_PASSWORD'), [
            'add-drop-table' => true,
            'default-character-set' => IMysqldump\Mysqldump::UTF8MB4,
            'no-data' => $blackListTables
        ]);
        $dump->start($filePath);

        // Force specific COLLATE + ENGINE
        $file = new File($filePath);
        $str = $file->read();
        $str = str_replace('COLLATE=utf8mb4_0900_ai_ci', 'COLLATE=utf8mb4_bin', $str);
        $str = str_replace('ENGINE=MyISAM', 'ENGINE=InnoDB', $str);
        $file->write($str);
        $file->close();

        // Download file
        $this->response->file($filePath, array('download' => true, 'name' => time() . '_full_dump.sql'));
        return $this->response;
    }

    /**
     * Cache overview
     */
    public function cache()
    {

    }

    /**
     * Empty sitekick cache
     */
    public function emptycache()
    {

        $this->autoRender = false;

        $this->SitekickCache->clear();

        $this->Flash->success(__('Cache is geleegd'));

        return $this->redirect(['plugin' => false, 'controller' => 'info', 'action' => 'index']);

    }


    /**
     * Get migrations
     *
     * @return mixed
     */
    protected function getMigrations()
    {
        $this->loadModel('Migrations');
        return $this->Migrations->find()->order(['id' => 'desc']);
    }

}