<?php
namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\Network\Session;

class FieldsController extends AppController
{

	public function index()
	{
		$fields = $this->Fields->find();
		$this->set('overview', $fields);
	}

	public function add()
	{
		$field = $this->Fields->newEntity();
		$this->set('field', $field);

		if( $this->request->is(['post', 'put']) ){
			$this->Fields->patchEntity($field, $this->request->data());
			if( $this->Fields->save($field) ){
				$this->Flash->success(__('Veld wijziging opgeslagen'));
				$this->Redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('Kan veld niet opslaan'));
			}
		}
	}

	public function edit($id)
	{
		$field = $this->Fields->get($id);

		if( $this->request->is(['post', 'put']) ){
			$this->Fields->patchEntity($field, $this->request->data());
			if( $this->Fields->save($field) ){
				$this->Flash->success(__('Veld wijziging opgeslagen'));
				$this->Redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('Kan veld niet opslaan'));
			}
		}

		$this->set('field', $field);
	}

	public function delete($id){
		if( $field = $this->Fields->get($id) ){
			if( $this->Fields->delete($field) ){
				$this->Flash->success(__('Veld is verwijderd'));
				$this->Redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('Veld kan op dit moment niet verwijderd worden.'));
			}
		}
	}
}