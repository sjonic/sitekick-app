<?php
namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\Network\Session;

class ErrorsController extends AppController
{


	public $components = ['Flash', 'RequestHandler'];

	/**
	 * Overview
	 */
	public function index()
	{
		$errors = $this->Errors->find('all');
		$this->set('errors', $errors);
	}



	/**
	 * Empty errorlog
	 */
	public function deleteAll(){

		$this->autoRender = false;

		//clear errorlog
		$this->Errors->deleteAll([]);

		$this->Flash->success( __('Errorlog is geleegd'));

		return $this->redirect( [ 'plugin' => false, 'controller' => 'errors', 'action' => 'index' ] );

	}

}