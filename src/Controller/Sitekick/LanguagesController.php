<?php
namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\Network\Session;

class LanguagesController extends AppController
{

	public function index()
	{
		$languages = $this->Languages->find();
		$this->set('languagesOverview', $languages);
	}

	public function add()
	{
		$language = $this->Languages->newEntity();
		$this->set('language', $language);

		if( $this->request->is(['post', 'put']) ){
			$this->Languages->patchEntity($language, $this->request->data());
			if( $this->Languages->save($language) ){
				$this->Flash->success(__('Taalwijziging opgeslagen'));
				$this->Redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('Kan de taal niet opslaan'));
			}
		}
	}

	public function edit($id)
	{
		$language = $this->Languages->get($id);

		if( $this->request->is(['post', 'put']) ){
			$this->Languages->patchEntity($language, $this->request->data());
			if( $this->Languages->save($language) ){
				$this->Flash->success(__('Taalwijziging opgeslagen'));
				$this->Redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('Kan de taal niet opslaan'));
			}
		}

		$this->set('language', $language);
	}

	public function delete($id){
		if( $language = $this->Languages->get($id) ){
			if( $this->Languages->delete($language) ){
				$this->Flash->success(__('De taal is verwijderd'));
				$this->Redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('De taal kan op dit moment niet verwijderd worden.'));
			}
		}
	}

	/**
	 * Set an active current language to the session
	 * @param $langIDorAbbreviation
	 * @return bool (or redirect)
	 */
	public function set_current_language( $langIDorAbbreviation )
	{
		if( is_numeric($langIDorAbbreviation) ){
			$language = $this->Languages->get($langIDorAbbreviation);
		} else {
			$language = $this->Languages->find('all')->where(['abbreviation'=>$langIDorAbbreviation])->first();
		}

		// nothing found, return false
		if( !$language ){
			return FALSE;
		}

		// set it to the session and redirect
		$this->request->session()->write('Config.language_sitekick', $language);

		$this->redirect( $this->referer() );
	}
}