<?php

namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\I18n\I18n;

class TypesController extends AppController
{

    /**
     * Overview
     */
    public function index()
    {
        $this->Types->locale('nl_NL');
        $types = $this->Types->find();
        $this->set('typesOverview', $types);
    }


    /**
     * Add new content type
     */
    public function add()
    {
        $this->Types->locale($this->request->getSession()->read('Config.language_sitekick.locale'));
        $type = $this->Types->newEntity();
        $this->set('type', $type);

        if ($this->request->is(['post', 'put'])) {

            $this->Types->patchEntity($type, $this->request->data());
            if ($this->Types->save($type)) {

                if (isset($this->request->data['translations']) && !empty($this->request->data['translations'])) {
                    foreach ($this->request->data['translations'] as $key => $value) {
                        $this->Types->patchEntity($type->translation($key), $value);
                        $this->Types->save($type);
                    }
                }

                $this->Flash->success(__('Content type wijziging opgeslagen'));
                $this->Redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Kan content type niet opslaan'));
            }
        }
    }


    /**
     * Edit content type
     * @param $id
     *
     */
    public function edit($id)
    {
        I18n::setLocale($this->request->getSession()->read('Auth.User.locale'));

        $this->Types->locale($this->request->getSession()->read('Config.language_sitekick.locale'));
        $type = $this->Types->find('translations')->where(['Types.id' => $id])->first();

        if ($this->request->is(['post', 'put'])) {

            //set formdata and old contenttype in variables
            $formData = $this->request->data();
            $oldContentType = $type->content_type;

            $this->Types->patchEntity($type->translation($this->request->getSession()->read('Config.language_sitekick.locale')), $formData);
            //$this->Types->patchEntity($type, $formData);

            if ($this->Types->save($type)) {

                if (isset($this->request->data['translations']) && !empty($this->request->data['translations'])) {
                    foreach ($this->request->data['translations'] as $key => $value) {
                        $this->Types->patchEntity($type->translation($key), $value);
                        $this->Types->save($type);
                    }
                }

                //if name of type is changed move all items to new typename
                if ($oldContentType != $formData['content_type']) {

                    $this->loadModel('Items.Items');

                    $items = $this->Items->findByType($oldContentType);
                    foreach ($items as $item) {
                        $this->Items->patchEntity($item, ['type' => $formData['content_type']]);
                        $this->Items->save($item);
                    }

                    $this->Flash->success(__('Content type wijziging opgeslagen <br><strong>LET OP: heb je type naam gewijzigd?<br> Pas dit ook aan in je frontend!</strong>'), ['params' => ['timer' => 0]]);

                } else {
                    $this->Flash->success(__('Content type wijziging opgeslagen'));
                }

                $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Kan content type niet opslaan'));
            }
        }

        $this->set('languages', $this->Languages->find());

        $this->set('type', $type);
    }


    /**
     * Get alle fields for content types
     * @param $id
     */
    public function fields($id)
    {
        //get content type with fields
        I18n::locale('nl_NL');
        $fields = $this->Types->get($id, ['contain' => ['TypeFields']]);
        $this->set(compact('fields'));

        # Set locale User
        I18n::locale($this->request->session()->read('Auth.User.locale'));
    }


    /**
     * Add file to content type
     * @param $id
     */
    public function addfield($id)
    {

        //set type
        $type = $this->Types->get($id);
        $this->set('type', $type);

        //all fields to view
        $this->loadModel('Fields');
        $fields = $this->Fields->find('all');
        $this->set(compact('fields'));

        $this->loadModel('TypeFields');
        $this->TypeFields->locale('nl_NL');
        $typefield = $this->TypeFields->newEntity();
        $this->set('typefield', $typefield);

        // get latest order
        $newrank = $this->TypeFields->find()->select(['rank' => '`rank` + 1'])->where(['type_id' => $id])->last();
        $this->set('newrank', $newrank);

        if ($this->request->is(['post', 'put'])) {

            $this->TypeFields->patchEntity($typefield->translation('nl_NL'), $this->request->data());

            if ($typefield = $this->TypeFields->save($typefield->translation('nl_NL'))) {

                if (isset($this->request->data['translations']) && !empty($this->request->data['translations'])) {

                    $typefield = $this->TypeFields->get($typefield->id);

                    foreach ($this->request->data['translations'] as $key => $value) {
                        $this->TypeFields->patchEntity($typefield->translation($key), $value);
                        $this->TypeFields->save($typefield);
                    }
                }

                $this->Flash->success(__('Veld wijziging opgeslagen'));
                $this->Redirect(['action' => 'fields', $id]);
            } else {
                $this->Flash->error(__('Kon veld niet opslaan'));
            }
        }
    }


    /**
     * Edit field from content type
     * @param $id
     */
    public function editfield($id)
    {

        //all fields to view
        $this->loadModel('Fields');

        $fields = $this->Fields->find('all');
        $this->set(compact('fields'));

        $this->loadModel('TypeFields');
        $this->TypeFields->locale('nl_NL');
        $typefield = $this->TypeFields->find()->find('translations')->where(['TypeFields.id' => $id])->first();
        $this->set('typefield', $typefield);

        //set type
        $type = $this->Types->get($typefield->type_id);
        $this->set('type', $type);

        if ($this->request->is(['post', 'put'])) {

            if ($this->request->data('options') != '') {
                $this->request->data['options'] = trim($this->request->data['options']);
            }
            $this->TypeFields->patchEntity($typefield->translation('nl_NL'), $this->request->data());

            if ($this->TypeFields->save($typefield)) {

                if (isset($this->request->data['translations']) && !empty($this->request->data['translations'])) {

                    foreach ($this->request->data['translations'] as $key => $value) {
                        $this->TypeFields->patchEntity($typefield->translation($key), $value);
                        $this->TypeFields->save($typefield);
                    }
                }

                $this->Flash->success(__('Veld wijziging opgeslagen'));
                $this->Redirect(['action' => 'fields', $typefield->type_id]);
            } else {
                $this->Flash->error(__('Kon veld niet opslaan'));
            }
        }
    }

    /**
     * Update field ranking/ordering
     */
    public function saveFieldOrder()
    {
        $this->request->allowMethod(['post', 'put']);
        $this->autoRender = false;
        $this->loadModel('TypeFields');

        foreach ($this->request->data('field') as $rank => $id) {
            if ($field = $this->TypeFields->findById($id)->first()) {
                $field->rank = $rank;
                $this->TypeFields->save($field);
            }
        }
    }

    /**
     * Update field
     * @param $id
     */
    public function updateField($id)
    {

        $this->autoRender = false;
        $this->loadModel('TypeFields');

        $typefield = $this->TypeFields->get($id);
        $this->TypeFields->patchEntity($typefield, $this->request->data);
        $this->TypeFields->save($typefield);

    }

    /**
     * Delete field from content type
     * @param $id
     */
    public function deletefield($id)
    {

        $this->loadModel('TypeFields');

        $typefield = $this->TypeFields->get($id);
        $this->TypeFields->delete($typefield);

        $this->Flash->success(__('Veld verwijderd'));
        $this->Redirect(['action' => 'fields', $typefield->type_id]);
    }


    /**
     * Delete content type
     * @param $id
     */
    public function delete($id)
    {
        if ($type = $this->Types->get($id)) {
            if ($this->Types->delete($type)) {
                $this->Flash->success(__('Content type is verwijderd'));
                $this->Redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Content type kan op dit moment niet verwijderd worden.'));
            }
        }
    }
}