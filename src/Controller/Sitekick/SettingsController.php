<?php

namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\Error\NotFoundException;
use Cake\Http\Exception\MethodNotAllowedException;
use Cake\Http\Response;

class SettingsController extends AppController
{

    /**
     * Settings overview
     */
    public function index()
    {

        # Set locale data
        $this->Settings->locale($this->request->getSession()->read('Config.language_sitekick.locale'));

        $systemsettings = $this->Settings->find('all', array('order' => 'plugin ASC'))->find('translations');

        if (!$this->Auth->user('superadmin')) {
            $systemsettings->where(['is_public' => 1]);
        }

        $this->set('systemsettings', $systemsettings);
        if ($this->Auth->user('superadmin')) {
            $this->render('index_admin');
        }
    }

    /**
     * Add setting method
     * @return Response|null
     */
    public function add()
    {
        if (!$this->Auth->user('superadmin')) {
            throw new MethodNotAllowedException('Auth is not valid.');
        }

        $setting = $this->Settings->newEntity();
        if ($this->request->is(['post'])) {
            $this->Settings->patchEntity($setting, $this->request->getData());
            if ($this->Settings->save($setting)) {
                $this->Flash->success(__('Instelling is toegevoegd.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Instelling is kan niet toegevoegd worden.'));
            }
        }

        $this->set('settingsList', array_combine(array_keys($this->settings), array_keys($this->settings)));
        $this->set('setting', $setting);
    }

    /**
     * Edit  user
     */
    public function edit($id = null)
    {

        if (!$id) {
            throw new NotFoundException(__('Ongeldige instelling'));
        }
        if (!$this->Auth->user('superadmin')) {
            throw new MethodNotAllowedException('Auth is not valid.');
        }

        # Set locale data
        $this->Settings->locale($this->request->getSession()->read('Config.language_sitekick.locale'));

        $setting = $this->Settings->get($id);

        if ($this->request->is(['post', 'put'])) {
            $this->Settings->patchEntity($setting, $this->request->getData());
            if ($this->Settings->save($setting)) {
                $this->Flash->success(__('Instellingen opgeslagen'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Kon instellingen niet opslaan'));
        }

        $this->set('settingsList', array_combine(array_keys($this->settings), array_keys($this->settings)));
        $this->set('setting', $setting);
    }

    /**
     * Delete setting
     * @param null $id
     * @return Response|null
     */
    public function delete($id = null)
    {
        if (!$this->Auth->user('superadmin')) {
            throw new MethodNotAllowedException('Auth is not valid.');
        }

        $setting = $this->Settings->get($id);
        $this->Settings->delete($setting);
        $this->Flash->success(__('Instelling is verwijderd.'));
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Check active edit sessions
     *
     */
    public function editcheck()
    {

        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');

        echo json_encode(['allowed' => 1]);

    }


    /**
     * Inline edit saving
     * @param null $id
     * @param null $field
     * @param null $editor
     */
    public function inline_edit($id = null, $field = null, $editor = null)
    {

        $this->viewBuilder()->setLayout('ajax');

        if (!$id) {
            throw new NotFoundException('Ongeldige data');
        }

        //get content
        $id = str_replace("-" . $field, "", $id);

        # Set locale data
        $this->Settings->locale($this->request->getSession()->read('Config.language_sitekick.locale'));

        $setting = $this->Settings->get($id);
        $this->set('content', $setting->value);

        if ($this->request->is(['post', 'put'])) {
            if (!$this->request->getData('cancel')) {
                $setting = $this->Settings->patchEntity($setting, ['value' => $this->request->getData('data')]);
                if ($this->Settings->save($setting)) {

                    // check for multilingual
                    if ($setting->multilingual == 0) {
                        $langs = $this->request->getSession()->read('sitelanguages');
                        foreach ($langs as $lang) {
                            $setting->translation($lang->locale)->set(['value' => $this->request->getData('data')], ['guard' => false]);
                            $this->Settings->save($setting);
                        }
                    }

                    //clear cache
                    $this->SitekickCache->clear();
                    $this->set('field', $field);
                    $this->set('content', $this->request->getData('data'));
                }
            }

            $this->render('inline_view');
        }

        $this->set('content', $setting->value);
        $this->set('editor', $editor);
        $this->set('field', $field);

    }
}