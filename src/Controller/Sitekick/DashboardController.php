<?php
namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Http\Client;
use Cake\Network\Email\Email;
use Cake\Cache\Cache;
use Cake\Event\Event;

class DashboardController extends AppController {

    /**
     * @param Event $event
     * @return \Cake\Network\Response|null|void
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // we allow the index because it is our main page. And we don't want a default not-logged-in message
        $this->Auth->allow(['index']);
    }


	/**
	 * Overview
	 */
	public function index() {

	    // check if we are logged in, otherwise redirect without a warning message
        if( !$this->Auth->user() ){
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }

		//get settings
		$settings  = json_decode(Configure::read('settings'));

        // get item types
        $this->loadModel('Types');
        $this->set('itemTypes', $this->Types->find()->where(['dashboard' => 1]));

	}

    /**
     * Overwrite session
     * @return \Cake\Http\Response|null
     */
	public function overwriteSession()
    {
        $this->loadModel('Sessions');
        $this->autoRender = false;

        if( !$this->request->getQuery('url') ){
            return $this->redirect($this->referer());
        }

        $session = $this->Sessions->findByUrl($this->request->getQuery('url'))->first();
        if( !empty($session) ){
            $this->Sessions->delete($session);
        }

        return $this->redirect($this->request->getQuery('url'));
    }

	public function pacman(){

	}
}