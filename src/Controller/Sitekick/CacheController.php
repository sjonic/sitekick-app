<?php

namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\Cache\Cache;
use Cake\Http\Response;
use Cake\Routing\Router;
use Exception;

class CacheController extends AppController
{
    public $components = ['SitekickCache'];

    /**
     * Overview
     */
    public function index()
    {
        // Check for files
        $files = [];
        $dir = ROOT . '/tmp/cache/static';
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file != "." && $file != "..") {
                        $files[] = $file;
                    }

                }
                closedir($dh);
            }
        }
        $this->set('files', $files);

        // check if there is a timestamp for when last log was written
        $this->loadComponent('SitekickCache');
        $lastBuildLog = $this->SitekickCache->getLastBuild();
        $this->set('lastBuildLog', $lastBuildLog);
    }

    /**
     * Rebuild Sitekick Cache
     * @return Response|null
     * @throws Exception
     */
    public function rebuild()
    {
        if ($this->request->is('ajax')) {
            session_write_close();
            $this->disableAutoRender();
        }

        $this->loadComponent('SitekickCache');
        $this->SitekickCache->setDomain(Router::url('/', true));
        $this->SitekickCache->build();
        $this->Flash->success(__('Cache is opnieuw opgebouwd voor de gehele website.'));

        if (!$this->request->is('ajax')) {
            return $this->redirect($this->referer());
        } else {
            echo 'success';
        }

    }

    /**
     * Empty sitekick cache
     */
    public function clear()
    {

        $this->autoRender = false;

        //clear sitekick cache
        $this->SitekickCache->clear(true);

        //clear cake cache
        Cache::clear();
        $this->Flash->success('Cache is geleegd');

        return $this->redirect(['plugin' => false, 'controller' => 'cache', 'action' => 'index']);

    }


    public function delete($file)
    {

        $this->autoRender = false;

        $this->SitekickCache->delete($file);

        $this->Flash->success(__('{0} is verwijderd', [$file]));

        return $this->redirect(['plugin' => false, 'controller' => 'cache', 'action' => 'index']);


    }


}