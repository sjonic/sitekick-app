<?php
namespace App\Controller\Sitekick;

use App\Controller\AppController;

class UpdatemessagesUsersController extends AppController {


    /**
     * Update status
     */
    public function update() {

        $this->viewBuilder()->layout('ajax');

        if ($this->request->is('post')) {

            if( $this->request->data('display') == 0 ) {

                $ids = json_decode($this->request->data('ids'));

                $this->UpdatemessagesUsers->updateAll(
                    [
                        'display' => $this->request->data('display')
                    ],
                    [
                        'message_id IN' => $ids,
                        'user_id' => $this->Auth->user('id')
                    ]
                );

            }

            $this->request->session()->delete('updatemessages');
        }

        $this->render(false);

    }

}
?>