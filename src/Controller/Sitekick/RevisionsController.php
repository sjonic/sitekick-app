<?php
namespace App\Controller\Sitekick;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Http\Client;
use Cake\Network\Email\Email;
use Cake\Cache\Cache;

class RevisionsController extends AppController {

	/**
	 * Get revision by id
     * @param $id
     *
	 */
	public function view($id = null) {

	    if( $id ) {

            $revision = $this->Revisions->find()
                 ->join([
                    'table' => 'users',
                    'alias' => 'user',
                    'type' => 'LEFT',
                    'conditions' => 'user.id = Revisions.user_id',
                ])
                ->where(['Revisions.id' => $id])
				->select(['id', 'user_id', 'plugin', 'controller', 'action', 'foreign_key', 'url', 'postdata', 'method', 'date', 'user.firstname', 'user.lastname'])
                ->first();

            if( !empty($revision) ) {
                $this->set('revision', $revision);
            }
        }
	}


}