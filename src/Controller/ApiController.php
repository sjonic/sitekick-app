<?php
namespace App\Controller;

use App\Model\Entity\Setting;
use App\Model\Table\SettingsTable;
use Cake\Core\Configure;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Core\Plugin;
use Cake\Core\App;
use Cake\Cache\Cache;


class ApiController extends AppController {

	public $components = ['RequestHandler','SitekickCache'];

	/**
	 * Index
	 * @return \Cake\Network\Response|void
	 */
	public function index() {
		$this->autoRender = false;
	}


	/**
	 * Return json with verions of base and plugins
	 */
	public function versions($key = null) {

		$this->autoRender = false;

		if($key == "1234"){

			$versionsArray = array();

			$plugins = Plugin::loaded();

			$this->set( 'plugins', $plugins );

			//get base version
			$appBaseVersionFile = new File( ROOT . '/config/version.txt' );

			if ( file_exists( $appBaseVersionFile->path ) ) {
				$versionsArray["base"] = $appBaseVersionFile->read();
			}

			//get plugin versions
			foreach ( $plugins as $plugin ) {

				//run init file
				$pluginVersionFile = new File( ROOT . '/plugins/' . $plugin . '/config/version.txt' );
				if ( file_exists( $pluginVersionFile->path ) ) {
					$versionsArray[strtolower($plugin)] =  $pluginVersionFile->read();
				}
			}

			//output json
			echo json_encode($versionsArray);
		}
	}

	/**
	 * Return json with all settings of site
	 *
	 */
	public function settings($key = null) {

		$this->autoRender = false;

		if($key == "1234"){

			$this->loadModel('Settings');
			$settings = $this->Settings->find('all',array('order'=> 'plugin DESC'));

			$settingsArray = array();

			foreach($settings as $setting){
				$settingsArray[$setting->plugin][$setting->name] = $setting->value;
			}

			echo json_encode($settingsArray);
		}
	}


	/**
	 * Clear complete cache
	 */
	public function clearcache($key = null) {

		$this->autoRender = false;

		if($key == "1234"){

			//clear sitekick cache
			$this->SitekickCache->clear();

			//clear cake cache
			Cache::clear();

			echo 1;
		}
	}


	/**
	 * Check session
	 */
	public function session(){

		$this->autoRender = false;

		//session_destroy();

		if(isset($_SESSION['Auth'])){
			echo 1;
		}else{
			echo 0;
		}
	}
}