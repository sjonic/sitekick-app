<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use DateTime;
use DateTimeZone;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Components this controller uses.
     *
     * Component names should not include the `Component` suffix. Components
     * declared in subclasses will be merged with components declared here.
     *
     * @var array
     */
    public $components = [
        'Flash',
        'Cookie',
        'File',
        'UrlReplace',
        'SitekickCache',
        'Auth' => [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email']
                ]
            ],
            'loginRedirect' => [
                'controller' => 'dashboard',
                'action' => 'index',
                'plugin' => false
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login',
                'plugin' => false
            ],
            'unauthorizedRedirect' => [
                'controller' => 'users',
                'action' => 'login',
                'plugin' => false
            ],
            'loginAction' => [
                'controller' => 'users',
                'action' => 'login',
                'plugin' => false
            ],
            'authorize' => ['Controller']
        ],
        'RequestHandler',
    ];


    /**
     * Load helpers
     * @var array
     */
    public $helpers = [
        'isAllowed',
        'Url',
        'Inline',
        'Shrink.Shrink',
        'Form' => [
            'className' => 'Bootstrap.Form'
        ]
    ];

    /**
     * schema query
     * @var string
     */
    public $query = '';

    /**
     * Active language
     * @var
     */
    public $activeLanguage;
    public $tables;
    public $settings;

    public function initialize()
    {
        $db = ConnectionManager::get('default');
        $collection = $db->getSchemaCollection();
        $this->tables = $collection->listTables();

        parent::initialize();

    }

    /**
     * Before filters
     *
     * @param Event $event
     * @return \Cake\Network\Response|null|void
     * @throws \Exception
     */
    public function beforeFilter(Event $event)
    {
        //Loaded plugins to view
        $plugins = Plugin::loaded();

        //if url ends with trailing slash remove and set 301 redirect
        $requestUrl = $_SERVER['REQUEST_URI'];
        $trailing = substr($requestUrl, -1);
        $skipTrailcheck = 0;

        //skip trailing slash check for plugins
        foreach ($plugins as $plugin) {
            if (strpos($this->request->here('true'), strtolower("webroot/" . $plugin))) {
                $skipTrailcheck = 1;
            }
        }

        if ($skipTrailcheck == 0 && $trailing == "/" && strlen($requestUrl) > 1 && reset($this->request->params['pass']) != "home" && strpos($this->request->here('true'), "sitekick") == 0 && strpos($this->request->here('true'), "api") == 0 && strpos($this->request->here('true'), "webroot") == 0 && !$this->request->is(['post', 'put'])) {
            $redirectUrl = rtrim($requestUrl, "/");
            header('location:' . $redirectUrl, false, 301);
            exit;
        }


        //remove core plugins (bootstrap forms/debugkit)
        foreach ($plugins as $pkey => $pval) {
            if (in_array($pval, ["DebugKit", "Bootstrap"])) {
                unset($plugins[$pkey]);
            }
        }


        // check redirects
        $this->checkRedirects();

        // get available languages
        $this->setLanguage();
        $this->loadLanguages();

        //load settings
        $this->loadSettings($plugins);

        // set pageview
        $blacklistIps = (!empty($this->settings['Base']['blacklist_ips'])) ? explode(';', $this->settings['Base']['blacklist_ips']) : [];
        $blacklistIps = array_merge(['127.0.0.1'], $blacklistIps);
        if (in_array('pageviews', $this->tables)
            && $this->request->getParam('prefix') !== 'sitekick'
            && !$this->request->is('ajax')
            && !$this->request->getParam('requested')
            && strpos($this->request->getUri()->getPath(), '.') === false
            && session_id()
            && !in_array($_SERVER['REMOTE_ADDR'], $blacklistIps)
            && !preg_match('/bot|crawl|slurp|spider|mediapartners/i', ((!empty($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : ''))
        ) {
            $this->loadModel('Pageviews');
            $entity = $this->Pageviews->newEntity([
                'date_created' => new DateTime(),
                'ipaddress' => md5($_SERVER['REMOTE_ADDR']),
                'session_id' => session_id(),
                'page' => $this->request->getUri()->getPath(),
            ]);
            $this->Pageviews->save($entity);
        }

        //return cache only when exist an not logged in
        if (!$this->Auth->isAuthorized() && Configure::read('sitekick_cache') != 0) {
            $this->SitekickCache->view();
        }

        // turn on debug mode
        $this->setDebugMode();


        //get available content types
        $this->loadTypes();

        //if user set all user permissions to view
        if ($this->Auth->isAuthorized()) {

            $authUser = $this->Auth->user();

            if ($this->request->getQuery('show_message')) {
                $this->loadModel('Updatemessages');
                $updatemessages = $this->Updatemessages->findById($this->request->getQuery('show_message'))->toArray();
                $this->request->getSession()->write('updatemessages', $updatemessages);
            }

            //get avatar
            if ($this->request->getSession()->check('useravatar') == false) {
                $this->loadModel("Files");
                $aUser = $this->Files->find('all', ['conditions' => ['plugin' => 'Users', 'parent_id' => $authUser['id']]])->first();

                if (isset($aUser->file)) {
                    $authUser['avatar'] = $aUser->file;
                    $this->request->getSession()->write('useravatar', $aUser->file);
                } else {
                    $authUser['avatar'] = '';
                    $this->request->getSession()->write('useravatar', '');
                }
            } else {
                $authUser['avatar'] = $this->request->getSession()->read('useravatar');
            }

            //set admin in config
            if ($authUser['role'] == "admin") {
                Configure::write("admin", 1);
            }

            $this->set(compact('authUser'));

            if ($this->request->getSession()->check('userspermissions') == false) {
                $this->loadModel('UsersPermissions');
                $pluginPermissions = $this->UsersPermissions->findAllByUserId($authUser['id']);
                $this->request->getSession()->write('userspermissions', $pluginPermissions->toArray());
            } else {
                $pluginPermissions = $this->request->getSession()->read('userspermissions');
            }
            $this->set(compact('pluginPermissions'));
        }

        //allow everything except admin prefix (need to be logged in for that)
        if ($this->request->getParam('prefix') !== 'sitekick') {
            $this->Auth->allow();
        } else {
            //set backend layout
            $this->viewBuilder()->setLayout('sitekick');
            $this->Auth->deny();
        }

        //always allow certain routes
        if (
            $this->request->params['action'] == "ajaxlogin" or
            $this->request->params['action'] == "login" or
            $this->request->params['action'] == "logout" or
            $this->request->params['action'] == "setpassword" or
            $this->request->params['action'] == "forgot" or
            $this->request->params['action'] == "forgot_send" or
            $this->request->params['action'] == "getavatar" or
            $this->request->params['action'] == "nobearslogin" or
            $this->request->params['action'] == "nobearsloginreturn"
        ) {
            $this->Auth->allow();
        }

        //display no sidebar
        if (isset($_COOKIE["nosidebar"])) {
            $this->set('nosidebar', 1);
        }

        //set referral only if not set already
        $session = $this->request->getSession();
        if (!strpos($this->request->here('true'), "sitekick") && !strpos($this->request->here('true'), "api") && !strpos($this->request->here('true'), "files") && !strpos($this->request->here('true'), "json")) {
            $session->write("front_referral", $this->request->here('true'));
        }

        if ($this->request->getParam('prefix') !== 'sitekick') {
            $this->set('isFront', 1);
        }

        //set navigations
        $locale = $this->request->getSession()->read('Config.language');
        I18n::setLocale(((!empty($locale)) ? $locale->locale : 'nl_NL'));

        $this->loadModel('Navigations');
        $navigations = $this->getNavigations();
        $this->set('navigations', $navigations);

        //set if home
        if ($this->request->here(false) == "/") {
            $this->set('isHome', 1);
        }

        // If we are in Sitekick - set the correct Sitekick-user-locale
        if ($this->request->getParam('prefix') === 'sitekick') {
            I18n::setLocale( ($this->Auth->user('locale')) ? $this->Auth->user('locale') : 'nl_NL');
        }
    }

    /**
     * Recursive get navigations
     * @param int $parent_id
     * @param int $level
     * @return mixed
     */
    protected function getNavigations($parent_id = 0, $level = 0)
    {
        $navigationDepth = (!empty($this->settings['Pages']['menu_levels'])) ? $this->settings['Pages']['menu_levels'] : 2;
        $navigations = $this->Navigations->find()
            ->where(['parent_id' => $parent_id])
            ->where(['OR' => [
                'Navigations.active' => true,
                $this->Navigations->translationField('active') => true,
            ]])
            ->where(['OR' => [
                'Navigations.display' => true,
                $this->Navigations->translationField('display') => true
            ]])
            ->group('Navigations.id')
            ->order('lft')
            ->cache('navigations_'.$parent_id . $level .'_' . strtolower(I18n::getLocale()))
            ->toArray();

        if( $level <  $navigationDepth && !empty($navigations)){
            foreach($navigations as $navigation){
                $navigation->children = $this->getNavigations($navigation->id, $level+1);
            }
        }

        return $navigations;
    }

    /**
     * Before rendering
     *
     * @param Event $event
     * @throws \Exception
     */
    public function beforeRender(Event $event)
    {

        // Site prefix for cache files
        $f = Plugin::path($this->settings['Base']['theme']) . DS . 'webroot' . DS . 'js' . DS . 'default.js';
        if( file_exists($f) ){
            Configure::write('Shrink.prefix', 'shrink_'.filemtime($f).'_');
        } else {
            $f = Plugin::path($this->settings['Base']['theme']) . DS . 'webroot' . DS . 'js' . DS . 'main.js';
            if( file_exists($f) ){
                Configure::write('Shrink.prefix', 'shrink_'.filemtime($f).'_');
            }
        }

        //all edit actions need to be logged in sessions table, this to check if there are double edit actions.
        //also set warning if session with same url from other user is active
        $authUser = $this->Auth->user();
        $this->loadModel("Sessions");
        $this->loadModel("Users");

        $sessions = $this->Sessions->find('all');
        foreach ($sessions as $session) {

            $start_date = $session->date;
            $since_start = $start_date->diff(new DateTime('now', new DateTimeZone('Europe/Amsterdam')));

            if ($since_start->d > 0 && $since_start->h >= 5) {
                $this->Sessions->delete($session);
            }
        }

        //delete all own sessions
        if (!strpos($this->request->here('true'), "api") && !strpos($this->request->here('true'), "media")) {
            if( !empty($authUser) ){
                $this->Sessions->deleteAll(['user_id' => $authUser['id']]);
            }
        }
        if ($this->request->params['action'] == "edit") {

            //check if session with same url with other user exists then set session warning
            if ($editsession = $this->Sessions->find()->where(['url' => $this->request->url, 'user_id !=' => $authUser['id']])->first()) {

                $editing = $this->Sessions->find('all', ['url' => $this->request->url, 'user_id !=' => $authUser['id']])->first();
                $editing_user = $this->Users->get($editing->user_id);


                $datetime = new Time($editsession->date, 'Europe/Amsterdam');
                $datetime = $datetime->modify("+5 hours");
                $datetime = $datetime->format('d-m-Y H:i:s');

                $modifiedTime = new Time($datetime);
                $now = new Time();

                // editing time is over
                if ($modifiedTime < $now) {
                    $this->Sessions->delete($editing);
                } else {
                    $this->set("editing_togo", $modifiedTime->timeAgoInWords());
                    $this->set("editing", $editing);
                    $this->set("editing_user", $editing_user);
                }
            } else {
                $data = [
                    'user_id' => $authUser['id'],
                    'url' => $this->request->url
                ];

                $sessionEntity = $this->Sessions->newEntity($data);
                $this->Sessions->save($sessionEntity);
            }
        }


        //delete all pending images if in sitekick and action not edit or add
        if (isset($this->request->params['prefix']) && isset($this->request->params['action']) && $this->request->params['prefix'] == "sitekick" && in_array($this->request->params['action'], ["index"])) {
            $this->File->cleanPendingImages();
        }

        //set current active plugin
        $this->set('active_plugin', $this->request->params['plugin']);

        //get all routes
        $namedRoutes = Router::routes();

        //set front url
        $frontUrl = "/";

        if (empty($this->viewVars['frontUrl'])) {
            foreach ($namedRoutes as $nameRoute) {
                if (isset($nameRoute->options['_name'])) {
                    if ($nameRoute->options['_name'] == strtolower($this->request->params['plugin']) . '_front_' . $this->request->params['action']) {
                        $frontUrl = Router::url(['_name' => strtolower($this->request->params['plugin']) . '_front_' . $this->request->params['action']]);
                    }
                }
            }
        } else {
            $frontUrl = $this->viewVars['frontUrl'];
        }

        $addonFrontUrl = "";
        if (!empty($this->viewVars['slug']) && $this->viewVars['slug'] != "home") {
            $addonFrontUrl = $this->viewVars['slug'];
        }


        //get type settings and check if front url is disabled
        if (isset($this->request->params['type'])) {
            $this->loadModel('Types');
            $type = $this->Types->findByContentType($this->request->params['type'])->first();

            if (isset($type) && $type->has_page == 0) {
                $frontUrl = "/";
                $addonFrontUrl = "";
            }
        }


        $this->set('front_url', $frontUrl . $addonFrontUrl);

        //set back url
        $settings = (array)json_decode(Configure::read("settings"));

        $backUrl = "/sitekick/" . strtolower($this->request->params['plugin']);

        if (isset($settings[$this->request->params['plugin']]->slug) && $this->request->params['plugin'] != "Pages") {
            $backUrl = "/sitekick/" . $settings[$this->request->params['plugin']]->slug;
        }
        if (!empty($event->subject()->viewVars['navigation']->type)) {
            $backUrl .= "/" . $event->subject()->viewVars['navigation']->type;
        }

        $addonBackUrl = "";
        if (isset($this->viewVars['itemid'])) {
            $addonBackUrl = "/edit/" . $this->viewVars['itemid'];
        }

        $this->set('back_url', $backUrl . $addonBackUrl);

        if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'sitekick') {
            $this->set('SitekickAreaActive', true);
        } else {
            $this->set('SitekickAreaActive', false);
        }

        //set currentUrl
        $this->set("currentUrl", $this->request->here());

        # Set cookies
        $this->set('cookies', $this->Cookie->read('Sitekick'));

        # Tablet detect
        $detector = new \Detection\MobileDetect();
        if ($detector->isTablet()) {
            $this->isTablet = true;
        } else {
            $this->isTablet = false;
        }
        $this->set('isTablet', $this->isTablet);
    }

    /**
     * Check redirects
     */
    protected function checkRedirects(){

        $redirects  = TableRegistry::getTableLocator()->get('Redirects');

        if($redirect = $redirects->findByFromUrl($this->request->url)->first()){
            if($redirect->from_url != $redirect->to_url) {

                // Redirect exists, log to DB
                $this->loadModel('SeoRedirectLogs');
                $entity = $this->SeoRedirectLogs->newEntity([
                    'redirect_id' => $redirect->id,
                    'ip' => $this->request->clientIp(),
                    'created' => time()
                ]);
                $this->SeoRedirectLogs->save($entity);

                // Set the redirect headers
                if( strpos($redirect->to_url, 'http') !== false ){
                    $url = $redirect->to_url;
                } else {
                    $url = Router::url($redirect->to_url, true);
                }

                header('Location: ' . $url, '', $redirect->type);
                exit;
            }
        }

        // check wildcard redirects
        $wildcardRedirects = $redirects->find()->where(['from_url LIKE' => '%*%']);
        foreach($wildcardRedirects as $wildcardRedirect){
            $matches = null;
            $regex = str_replace('*', '(.*)', $wildcardRedirect->from_url);
            $regex = str_replace('/', '\/', $regex);
            $regex = '/^'. $regex . '$/';
            $matchesFound = preg_match_all($regex, $this->request->url, $matches);

            // redirect found
            if( !empty($matchesFound) ){
                $toUrl = $wildcardRedirect->to_url;
                foreach($matches as $key=>$match){
                    if( $key > 0 ){
                        $toUrl = str_replace('$' . $key, $match[0], $toUrl);
                    }
                }

                // Redirect exists, log to DB
                $this->loadModel('SeoRedirectLogs');
                $entity = $this->SeoRedirectLogs->newEntity([
                    'redirect_id' => $wildcardRedirect->id,
                    'ip' => $this->request->clientIp(),
                    'created' => time()
                ]);
                $this->SeoRedirectLogs->save($entity);

                header('Location: ' . $toUrl, '', $wildcardRedirect->type);
                exit;

            }
        }
    }

    /**
     * Is user authorized to visit action
     * @param $authUser
     *
     * @return bool
     */
    public function isAuthorized($authUser) {

        // superadmin and admin can access every action
        if(($authUser['superadmin'] == 1) or $authUser['role']  == "admin"){
            $this->set('isAuthorized',true);
            return true;
        }

        //other users
        if (isset($authUser['role'])) {

            $this->set('isAuthorized',true);

            //always allow certain routes
            if(
                $this->request->params['action'] == "login" or
                $this->request->params['action'] == "logout" or
                $this->request->params['action'] == "setpassword" or
                $this->request->params['action'] == "forgot" or
                $this->request->params['action'] == "forgot_send" or
                $this->request->params['action'] == "getavatar"
            ){
                return true;
            }

            if($this->request->params['plugin'] == "Users" && $this->request->params['action'] == "edit"){
                return true;
            }


            if($this->request->params['plugin'] == "Media"){
                return true;
            }

            //if user is admin check for plugin permissions
            if($authUser['role'] != 'admin' && ! empty($this->request->params['plugin'])){

                $this->loadModel('UsersPermissions');
                $pluginPermission = $this->UsersPermissions->findByUserIdAndPlugin($authUser['id'], $this->request->params['plugin'])->first();

                if(isset($this->request->params['type']) && $this->request->params['plugin'] == 'Items'){
                    $pluginPermission = $this->UsersPermissions->findByUserIdAndPlugin($authUser['id'], $this->request->params['type'])->first();
                }

                if( ! isset($pluginPermission)){
                    return false;
                }

                if( isset($pluginPermission) && empty($pluginPermission->allow)){
                    return false;
                }
            }

            $this->set('isAuthorized',true);
            return true;
        }

        return false;
    }



    /**
     * Init app schema's
     */
    public function initAppSchemas(){

        //always run app init file
        $appSchemaInitFile = new File(ROOT.'/config/Schema/init.sql');
        $db = ConnectionManager::get('default');
        $db->execute($appSchemaInitFile->read());

        //migrations array for checks
        $this->loadModel('Migrations');
        $migrations      = $this->Migrations->findAllByPlugin('base');
        $migrationsArray = array();

        foreach($migrations as $migration) {
            $migrationsArray[] = $migration->file;
        }

        //run other update files
        $appSchemaUpdateFiles = new Folder(ROOT.'/config/Schema/');
        foreach($appSchemaUpdateFiles->find(".*") as $schemaUpdateFile){

            if( ! in_array($schemaUpdateFile, $migrationsArray)){


                $appSchemaUpdateFile = new File( ROOT . '/config/Schema/' . $schemaUpdateFile );
                $db = ConnectionManager::get( 'default' );

                //set query to run
                $this->query = $appSchemaUpdateFile->read();

                try {
                    $db->execute( $this->query );
                    $executed = 1;
                } catch (\Exception $e) {
                    $executed = 0;
                }

                $data = [
                    'plugin'   => 'base',
                    'file'     => $schemaUpdateFile,
                    'executed' => $executed
                ];

                $migrationEntity = $this->Migrations->newEntity($data);
                $this->Migrations->save($migrationEntity);

                //clear all cache
                $this->SitekickCache->clear();
            }
        }
    }


    /**
     * Init plugin schema's
     */
    public function initPluginSchemas(){

        //Loaded plugins to view
        $plugins = Plugin::loaded();

        //load migrations model
        $this->loadModel('Migrations');

        //init plugin database schema's
        foreach($plugins as $plugin){

            //migrations array for checks
            $migrations      = $this->Migrations->findAllByPlugin(strtolower($plugin));
            $migrationsArray = array();

            foreach($migrations as $migration) {
                $migrationsArray[] = $migration->file;
            }


            if( ! in_array("init.sql", $migrationsArray)) {


                //run init file
                $pluginSchemaInitFile = new File( ROOT . '/plugins/' . $plugin . '/config/Schema/init.sql' );

                if ( file_exists( $pluginSchemaInitFile->path ) ) {

                    $db = ConnectionManager::get( 'default' );

                    //set query to run
                    $this->query = $pluginSchemaInitFile->read();

                    try {
                        $db->execute( $this->query );
                        $executed = 1;
                    } catch (\Exception $e) {
                        $executed = 0;
                    }

                    $data = [
                        'plugin'   => strtolower($plugin),
                        'file'     => 'init.sql',
                        'executed' => $executed
                    ];

                    $migrationEntity = $this->Migrations->newEntity($data);
                    $this->Migrations->save($migrationEntity);

                    $migrationsArray[] = 'init.sql';
                }
            }


            //run other update files
            $pluginSchemaUpdateFiles = new Folder(ROOT.'/plugins/' . $plugin . '/config/Schema/');
            foreach($pluginSchemaUpdateFiles->find(".*") as $schemaUpdateFile){
                if( !in_array($schemaUpdateFile, $migrationsArray) ) {
                    $pluginSchemaUpdateFile = new File( ROOT . '/plugins/' . $plugin . '/config/Schema/' . $schemaUpdateFile );

                    $db = ConnectionManager::get( 'default' );

                    //set query to run
                    $this->query = $pluginSchemaUpdateFile->read();

                    try {
                        $db->execute( $this->query );
                        $executed = 1;
                    } catch (\Exception $e) {
                        $executed = 0;
                    }

                    $data = [
                        'plugin'   => strtolower($plugin),
                        'file'     => $schemaUpdateFile,
                        'executed' => $executed
                    ];


                    $migrationEntity = $this->Migrations->newEntity($data);
                    $this->Migrations->save($migrationEntity);

                    //clear all cache
                    $this->SitekickCache->clear();
                }
            }

        }
    }


    /**
     * Load settings
     */
    public function loadSettings($plugins){

        $this->loadModel('Settings');
        $viewSettings = array();

        $basesettingsSessionName = 'basesettings_' . I18n::getLocale();
        $viewsettingsSessionName = 'viewsettings' . I18n::getLocale();

        if( $this->request->getSession()->check($basesettingsSessionName) == false || $this->Auth->user() ) {
            $basesettings = $this->Settings->findAllByPlugin('Base');
            $this->request->getSession()->write($basesettingsSessionName, $basesettings->toArray());
        }
        else {
            $basesettings = $this->request->getSession()->read($basesettingsSessionName);
        }

        $settingsArray = array();
        foreach($basesettings as $setting){
            $settingsArray[$setting->name] = $setting->value;
            Configure::write($setting->name, $setting->value);
        }

        $viewSettings['Base'] = $settingsArray;

        //if theme isset use theme and load theme plugin
        if(isset($viewSettings['Base']['theme'])){
            $this->viewBuilder()->setTheme($viewSettings['Base']['theme']);
        }

        if( $this->request->getSession()->check($viewsettingsSessionName) == false || $this->Auth->user() ) {

            //load plugins settings to view
            foreach($plugins as $plugin){

                // Get locale for front-end website if we are at the front-end for the theme-settings.
                if( $plugin === $viewSettings['Base']['theme'] && $this->request->getParam('prefix') !== 'sitekick' ){
                    $this->Settings->locale($this->activeLanguage->locale);
                } else {
                    $this->Settings->locale($this->request->getSession()->read('Auth.User.locale'));
                }
                $settings = $this->Settings->findAllByPlugin($plugin);

                $settingsArray = array();
                foreach($settings as $setting){
                    if( is_numeric($setting->value) ){
                        $settingsArray[$setting->name] = (float)$setting->value;
                    } else {
                        $settingsArray[$setting->name] = $setting->value;
                    }
                }

                $viewSettings[$plugin] = $settingsArray;
            }
            uasort($viewSettings, [$this, "sortSettings"]);

        }
        else {
            $viewSettings = $this->request->getSession()->read($viewsettingsSessionName);
        }

        $this->set("settings", $viewSettings);
        $this->settings = $viewSettings;
        Configure::write("settings",json_encode($viewSettings));

        //set new order to plugins array
        $plugins = array();
        foreach($viewSettings as $viewSettingKey => $viewSettingValue){
            if($viewSettingKey != "Base"){
                $plugins[] = $viewSettingKey;
            }
        }

        $this->set('plugins', $plugins);
    }

    /***
     * Sort settings by "menu_order" field
     * @param $a
     * @param $b
     * @return bool
     */
    protected function sortSettings($a,$b) {
        if( empty($a['menu_order']) ) $a['menu_order'] = 999;
        if( empty($b['menu_order']) ) $b['menu_order'] = 999;
        return (int)$a['menu_order']>(int)$b['menu_order'];
    }



    /**
     * Load active types
     */
    function loadTypes(){

        $this->loadModel('Types');

        if( $this->request->getSession()->check('types') == false || $this->Auth->user() ) {
            // get all types
            $types = $this->Types->find('all')->where(['active'=>1]);

            if( !empty($types) ) {
                $this->request->getSession()->write('types', $types->toArray());
            }
        }
        else {
            $types = $this->request->getSession()->read('types');
        }
        $this->set('types', $types);
    }

    /**
     * Load active languages
     */
    function loadLanguages(){
        $this->loadModel('Languages');

        if( $this->request->getSession()->check('sitelanguages') == false || $this->Auth->user() ) {
            // get all languages
            $languages = $this->Languages->find('all')->where(['active'=>1]);

            if( !empty($languages) ) {
                $this->request->getSession()->write('sitelanguages', $languages->toArray());
            }
        }
        else {
            $languages = $this->request->getSession()->read('sitelanguages');
        }
        $this->set('languages', $languages);

    }

    /**
     * Set active language
     */
    function setLanguage() {

        $this->loadModel( 'Sitekick.Languages' );

        //get session alnguage based on front or backend
        $sessionLanguage = $this->request->session()->read( 'Config.language' );

        if($this->request->getParam('prefix') == "sitekick" && $this->request->getParam('action') != "inline_edit"){
            $sessionLanguage = $this->request->session()->read('Config.language_sitekick');
        }

        // no prefix found but we are in front-view, set default
        if ( $this->request->getParam('lang') == ''
            && $this->request->getParam( 'prefix' ) == ''
            && in_array( $this->request->getParam( 'action' ), [
                'view',
                'sitemap'
            ] )
        )  {
            $languages = $this->Languages->find( 'all' )->where( [ 'active' => 1 ] );
            if ( $languages->count() > 1 ) {
                $firstLanguage = $languages->first();
                if( !empty($sessionLanguage) ){
                    $firstLanguage = $sessionLanguage;
                }
                $url  = Router::url( '/', true ) . $firstLanguage->abbreviation . '/' . $this->request->url;
                header( 'location:' . $url, '', 301 );
                exit;
            }
        }


        // set locale based on request
        if ( ! empty( $this->request->params['lang'] ) ) {

            $locale = $this->Languages->find()->where(['abbreviation' => $this->request->params['lang'], 'active' => 1 ])->first();

            // no request available, lets set the language based on a session choice
        } elseif ( $sessionLanguage ) {
            $locale = $this->Languages->findById( $sessionLanguage->id )->first();
        }

        // get (and set) active language currently set
        if ( empty( $locale ) || $locale->active == 0 ) {
            $locale = $this->Languages->find( 'all' )->where( [ 'active' => 1 ] )->order( [ 'id' => 'asc' ] )->first();
        }


        //save session based on front or backend
        if($this->request->getParam('prefix') == "sitekick" && $this->request->getParam('action') != "inline_edit") {
            $this->request->getSession()->write( 'Config.language_sitekick', $locale );
        }else{
            $this->request->getSession()->write( 'Config.language', $locale );
        }


        if( isset($this->request->params['prefix']) && $this->request->getParam('prefix') == 'sitekick' ) {
            if( $this->request->getSession()->check('Auth') && $this->request->session()->read('Auth') ) {

                $auth = $this->request->getSession()->read('Auth.User');

                if( isset($auth['locale']) && !empty($auth['locale']) ) {
                    I18n::locale( $auth['locale'] );
                    setlocale( LC_ALL, $auth['locale'] );
                    ini_set( 'intl.default_locale', $auth['locale'] );
                }
                else {

                    $this->redirect(['controller' => 'Users', 'action' => 'logout', 'redirect_url' => Router::url('/sitekick')]);
                }
            }
        }
        else {
            I18n::locale( $locale->locale );
            setlocale( LC_ALL, $locale->locale );
            ini_set( 'intl.default_locale', $locale->locale );
        }

        $this->activeLanguage = $locale;
        $this->set( 'activeLanguage', $locale );

    }

    /**
     * Sitemap
     */
    public function sitemap()
    {
        // get different settings
        $settingsdata   = json_decode(Configure::read("settings"));
        $l              = TableRegistry::get('Languages');
        $sitemap        = [];

        // catch extension - if it is HTML we get different data
        // do request actions to the sitemap actions and get the JSON sitemap data
        if( $this->request->param('_ext') == 'html' || $this->request->param('_ext') == '' ){

            foreach($settingsdata as $plugin => $setting) {
                if (isset($setting->sitemap) && $setting->sitemap == 1) {
                    $url = (($l->find()->where(['active'=>1])->count() > 1) ? '/' . $this->activeLanguage->abbreviation : '') . "/".lcfirst($plugin)."/sitemap.json";
                    $url = Router::url($url);
                    if( $callback = $this->requestAction( $url ) ){

                        $items = json_decode($callback, TRUE);
                        if(!empty($items)) {
                            $sitemap[ $plugin ] = $items;
                        }
                    }
                }
            }


            // sitemap XML version
        } else {
            $activeLanguages = $l->find()->where(['active'=>1]);

            foreach($activeLanguages as $language){
                foreach($settingsdata as $plugin => $setting) {
                    if(isset($setting->sitemap) && $setting->sitemap == 1) {
                        $pluginName = lcfirst($plugin);
                        $url = "/{$language->abbreviation}/{$pluginName}/sitemap.xml";
                        $sitemap[]['loc'] = Router::url($url, [
                            '_full '    => true,
                        ]);
                    }
                }
            }
            // render special XML-template
            $this->set('sitemap', $sitemap);
        }

        $this->set([
            'sitemap' => $sitemap,
        ]);
    }


    /**
     * Sitemap editor
     */
    public function sitemapeditor() {

        //get correct locale
        $locale = $this->request->session()->read('Config.language');
        if(isset($_SERVER['HTTP_REFERER'])) {
            $arrRef = explode( "/", $_SERVER['HTTP_REFERER'] );
            if ( in_array( "sitekick", $arrRef ) ) {
                $locale = $this->request->session()->read( 'Config.language_sitekick' );
            }
        }

        // get different settings
        $settingsdata   = json_decode(Configure::read("settings"));
        $l              = TableRegistry::get('Languages');
        $sitemap        = [];



        foreach($settingsdata as $plugin => $setting) {
            if($setting) {
                if ( isset( $setting->sitemap ) && $setting->sitemap == 1 ) {
                    $url = ( ( $l->find()->where( [ 'active' => 1 ] )->count() > 1 ) ? '/' . $locale->abbreviation : '' ) . "/" . lcfirst( $plugin ) . "/sitemap.json";
                    $url = Router::url( $url, true );
                    if ( $callback = $this->curl_get_file_contents( $url ) ) {
                        $items = json_decode( $callback, true );
                        if(!empty($items)) {
                            $sitemap[ $plugin ] = json_decode( $callback, true );
                        }
                    }
                }
            }
        }

        // format for tinyMCE
        $formattedData = [];
        if( !empty($sitemap) ){
            foreach($sitemap as $plugin=>$urls){

                if( !empty($urls) ){
                    $parsedUrls = [];
                    foreach($urls as $urlGroup){
                        if( !empty($urlGroup) && is_array($urlGroup) ){
                            foreach($urlGroup as $url){
                                $parsedUrls[] = [
                                    'title' => $url['title'],
                                    'value' => $url['loc']
                                ];
                            }
                        }
                    }

                    $formattedData[] = [
                        'title' => $plugin,
                        'menu' => $parsedUrls
                    ];
                }
            }
        }

        $this->set([
            'sitemap' => $formattedData
        ]);


    }


    /**
     * Sort multi array on field value
     * @param $data
     * @param $field
     *
     * @return mixed
     */
    function settingsSort($a, $b)
    {
        if( empty($a['menu_order']) ) $a['menu_order'] = 0;
        if( empty($b['menu_order']) ) $b['menu_order'] = 0;
        return strcmp($a['menu_order'], $b['menu_order']);
    }

    /**
     * Get file content with cURL
     * @param string $url
     * @return bool|string
     */
    protected function curl_get_file_contents( $url = '' )
    {
        if( empty($url) ) return '';
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $url);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }

    /**
     * Set debug mode according to setting
     */
    protected function setDebugMode()
    {
        $this->loadModel('Settings');
        $setting = $this->Settings->find()->select(['value'])->where(['name' => 'debug'])->first();
        if( !empty($setting) ){
            Configure::write('debug', $setting->value);
        }
    }


    /**
     * Return empty grey placeholder with correct size
     */
    public function placeholder(){
        die(__('Bestand niet gevonden'));
    }

}