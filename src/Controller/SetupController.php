<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Datasource\ConnectionManager;
use Cake\Core\Plugin;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Core\Configure;
use Cake\Network\Http\Client;
use Cake\Network\Email\Email;
use Cake\Cache\Cache;
use Cake\Utility\Text;
use Cake\View\View;
use Cake\View\ViewBuilder;

class SetupController extends Controller {

    protected $db;
    protected $firstInstall = false;
    public $messages = [];

    /**
     * Install Sitekick
     */
    public function install()
    {
        $this->viewBuilder()->setLayout('sitekick_install');
        ini_set('max_execution_time', 300);

        $messages = [];

        $this->db = ConnectionManager::get('default');
        $tables = $this->db->getSchemaCollection()->listTables();
        if( empty($tables) ) $this->firstInstall = true;

        //create and modify database tables
        $this->initAppSchemas();
        $this->initPluginSchemas();


        // create robots.txt file
        $this->installRobotsFile();

        // set environment data within settings
        if( $this->firstInstall ){
            $this->setThemeSettings();
            $this->messages[] = '<strong>' . env('APP_NAME') . '</strong> implemented into settings';
        }

        // clear cache
        $this->request->getSession()->destroy();

        $this->set('seo_title', 'Sitekick Installatie');
        $this->set('messages', $this->messages);
    }


    /**
     * Init app schema's
     */
    public function initAppSchemas()
    {
        //always run app init file
        $appSchemaInitFile = new File(ROOT.'/config/Schema/init.sql');
        $db = ConnectionManager::get('default');
        $db->execute($appSchemaInitFile->read());


        //migrations array for checks
        $this->loadModel('Migrations');
        $migrations      = $this->Migrations->findAllByPlugin('base');
        $migrationsArray = array();

        foreach($migrations as $migration) {
            $migrationsArray[] = $migration->file;
        }

        //run other update files
        $appSchemaUpdateFiles = new Folder(ROOT.'/config/Schema/');
        foreach($appSchemaUpdateFiles->find(".*", true) as $schemaUpdateFile){

            if( ! in_array($schemaUpdateFile, $migrationsArray)){


                $appSchemaUpdateFile = new File( ROOT . '/config/Schema/' . $schemaUpdateFile );
                $db = ConnectionManager::get( 'default' );

                //set query to run
                $this->query = $appSchemaUpdateFile->read();

                try {
                    $db->execute( $this->query );
                    $executed = 1;
                } catch (\Exception $e) {
                    $executed = 0;
                    $error = $e;
                }

                $data = [
                    'plugin'   => 'base',
                    'file'     => $schemaUpdateFile,
                    'executed' => $executed
                ];

                $this->messages[] = $schemaUpdateFile . ' - ' . (($executed === 1) ? 'executed' : 'NOT executed <div class="hidden">'.$error.'</div>');

                $migrationEntity = $this->Migrations->newEntity($data);
                $this->Migrations->save($migrationEntity);

            }
        }
    }


    /**
     * Init plugin schema's
     */
    public function initPluginSchemas()
    {
        $this->loadModel('Migrations');

        //Loaded plugins to view
        $plugins = Plugin::loaded();

        if( file_exists(CONFIG . 'sitekick_install_modules.json') ){

            $sitekickPlugins = json_decode(file_get_contents(CONFIG . 'sitekick_install_modules.json'));

            // Run every default Sitekick plugin first
            foreach($sitekickPlugins->plugins as $plugin){
                $this->runPluginSqlPatches( $plugin );
            }

            // Run other plugins
            foreach($plugins as $plugin){
                if( !in_array($plugin, $sitekickPlugins->plugins) ) {
                    $this->runPluginSqlPatches( $plugin );
                }
            }
        }
    }

    /**
     * Run SQL patches within a plugin
     * @param $plugin
     */
    public function runPluginSqlPatches( $plugin )
    {
        $migrationsHistory = $this->Migrations->find('list', [
            'keyField' => 'file',
            'valueField' => 'file'
        ])->where(['plugin' => (strtolower($plugin))])->toArray();
        if( Plugin::loaded($plugin) ) {
            $pluginPath = Plugin::path($plugin);

            // first run init.sql
            if (!in_array("init.sql", $migrationsHistory)) {
                $initFile = $pluginPath . 'config/Schema/init.sql';
                if (file_exists($initFile)) {
                    $response = $this->runSqlPatchFile($plugin, $initFile);
                    $migrationsHistory[] = 'init.sql';
                    $this->messages[] = $initFile . ' - ' . (($response['executed'] === 1) ? 'executed' : 'NOT executed <div class="text-muted">' . $response['message'] . '</div>');
                }
            }

            // run other patches
            $pluginSchemaUpdateFiles = new Folder($pluginPath . 'config/Schema/');
            foreach ($pluginSchemaUpdateFiles->find(".*", true) as $schemaUpdateFile) {
                if (!in_array($schemaUpdateFile, $migrationsHistory)) {
                    $response = $this->runSqlPatchFile($plugin, $pluginSchemaUpdateFiles->path . $schemaUpdateFile);
                    $this->messages[] = $pluginSchemaUpdateFiles->path . $schemaUpdateFile . ' - ' . (($response['executed'] === 1) ? 'executed' : 'NOT executed <div class="text-muted">' . $response['message'] . '</div>');
                }
            }
        }
    }

    /**
     * Run SQL patch file
     * @param $plugin
     * @param $file
     * @return array
     */
    public function runSqlPatchFile( $plugin, $file )
    {
        $queryFile = new File( $file );
        $query = $queryFile->read();
        $error = '';
        try {
            $this->db->execute( $query );
            $executed = 1;
        } catch (\Exception $e) {
            $executed = 0;
            $error = $e->getMessage();
        }
        $migrationEntity = $this->Migrations->newEntity([
            'plugin'   => strtolower($plugin),
            'file'     => $queryFile->name,
            'executed' => $executed
        ]);
        $this->Migrations->save($migrationEntity);
        return [
            'executed' => $executed,
            'message' => $error
        ];
    }

    /**
     * Built robots.txt file
     */
    public function installRobotsFile()
    {
        $extraRobotsSettings = [];

        $this->loadModel('Settings');
        $setting = $this->Settings->find()->where(['name' => 'robots'])->first();

        if( !empty($setting) ){
            $extraRobotsSettings = json_decode($setting->value, true);
        }

        $builder = new ViewBuilder();
        $builder->setLayout('ajax');
        $builder->setTemplate('/Setup/robots');
        $view = $builder->build([
            'extraSettings' => $extraRobotsSettings
        ]);

        $robotsFile = new File( WWW_ROOT . 'robots.txt', true );
        $robotsFile->write($view->render());
        $robotsFile->close();

        return true;
    }

    public function setThemeSettings()
    {
        $theme = env('APP_NAME', '');
        $this->loadModel('Settings');

        // Set test environment
        $setting = $this->Settings->findByName('test_environment')->first();
        $setting->value = 1;
        $this->Settings->save($setting);

        // Set correct theme
        $setting = $this->Settings->findByName('theme')->first();
        $setting->value = $theme;
        $this->Settings->save($setting);

        // Set site name
        $setting = $this->Settings->findByName('site_name')->first();
        $setting->value = $theme;
        $this->Settings->save($setting);

        // Set default from name
        $setting = $this->Settings->findByName('mail_from_name')->first();
        $setting->value = $theme;
        $this->Settings->save($setting);

        // Set default from name
        $setting = $this->Settings->findByName('mail_from_email')->first();
        $setting->value = 'noreply@sjonic.nl';
        $this->Settings->save($setting);

        // Generate a humanizer string
        $setting = $this->Settings->findByName('human')->first();
        $setting->value = Text::uuid();
        $this->Settings->save($setting);

    }
}