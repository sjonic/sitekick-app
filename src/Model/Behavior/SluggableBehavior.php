<?php 
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Utility\Inflector;

class SluggableBehavior extends Behavior {

	public function initialize(array $config) {
		// Some initialization code here
	}
	
	public function slug($value) {
		return Inflector::slug($value, $this->_config['replacement'] = "-");
	}	

}
?>