<?php
namespace App\Model\Table;

use Cake\Controller\Component\AuthComponent;
use Cake\Datasource\EntityInterface;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Filesystem\Folder;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Navigations Model
 */
class NavigationsTable extends Table {


/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->setTable('navigations');
		$this->setDisplayField('name');
		$this->setPrimaryKey('id');
		$this->addBehavior('Timestamp');
		$this->addBehavior('Sluggable');
		
		$this->belongsTo('ParentNavigations', [
			'alias' => 'ParentNavigations',
			'className' => 'Navigations',
			'foreignKey' => 'parent_id',
			'sort' => ['lft asc']
		]);

		$this->hasMany('ChildNavigations', [
			'alias' => 'ChildNavigations',
			'className' => 'Navigations',
			'foreignKey' => 'parent_id',
			'sort' => ['lft asc']
		]);

        $this->hasMany('Children', [
//            'alias' => 'ChildNavigations',
            'className' => 'Navigations',
            'foreignKey' => 'parent_id',
            'sort' => ['lft asc']
        ]);

		$this->hasMany('Pagecontainers', [
			'alias' => 'Pagecontainers',
			'foreignKey' => 'navigation_id',
            'className' => 'Pages.Pagecontainers',
            'dependent' => true,
			'cascadeCallbacks' => true,
			'sort' => ['rank']
		]);

		$db = ConnectionManager::get('default');
        $tables = $db->getSchemaCollection()->listTables();
		if( in_array('headers', $tables) ) {
			$this->hasMany('HeadersNavigations', [
                'className'         => 'Headers.HeadersNavigations',
				'propertyName'      => 'headers',
                'dependent'         => true,
                'cascadeCallbacks'  => true
			]);
		}

		// turn on translations
		$this->addBehavior('Translate', ['fields' => [
			'name',
			'display',
			'cache',
			'active',
			'has_content',
			'seo_title',
			'seo_description',
			'seo_index',
			'slug'
		]]);
	}


	/**
	 * Validation
	 * @param Validator $validator
	 *
	 * @return Validator
	 */
	public function validationDefault(Validator $validator) {
		$validator
			->notEmpty('name','Je bent vergeten een naam in te vullen');
		return $validator;
	}

	/**
	 * @param Event $event
	 */
	public function beforeSave( Event $event ) {

		$entity = $event->data['entity'];


		//strip last - in slug
		if(isset($entity->slug)) {
			$newSlug = trim($entity->slug, "\0\t\n\x0B\r- ");
			$entity->slug = $newSlug;

			if( !empty($entity->_i18n) ){
				foreach($entity->_i18n as $key=>$translation){
					if( $translation->field == 'slug' ){
						$entity->_i18n[$key]->content = trim($entity->_i18n[$key]->content, "\0\t\n\x0B\r- ");
					}
				}
			}

		}

	}

    /** 
     * Get the next inserted ID
     * @return string $futureID
     */
	public function futureID() {

        $futureID = "";

		$conn = ConnectionManager::get('default');
		$data = $conn->query("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '".$conn->config()['database']."' AND TABLE_NAME = 'navigations'");

		foreach ($data as $key => $d) {
			$futureID = $d['AUTO_INCREMENT'];
		}

		return $futureID;
	}

	public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        // log that item has been deleted
        Log::info('Navigation "'.$entity->name.'" (#'.$entity->id.') has been updated by user "' . Router::getRequest()->getSession()->read('Auth.User.email') . '"', ['scope' => ['sitekick']]);
    }

	public function afterDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
	    // log that item has been deleted
        Log::info('Navigation "'.$entity->name.'" (#'.$entity->id.') has been deleted by user "' . Router::getRequest()->getSession()->read('Auth.User.email') . '"', ['scope' => ['sitekick']]);
    }
}
