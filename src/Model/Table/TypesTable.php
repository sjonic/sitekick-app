<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class TypesTable extends Table {


	public function initialize(array $config) {

		$this->hasMany('TypeFields', [
			'alias'      => 'TypeFields',
			'className'  => 'TypeFields',
			'foreignKey' => 'type_id',
			'sort'       => ['rank' => 'ASC']
		]);

        // turn on translations
        $this->addBehavior('Translate', ['fields' => [
            'name',
            'name_single',
            'menu_slug'
        ]]);

	}

	public function beforeFind( $options, $query )
    {
        if( $this->getSchema()->getColumn('rank') ){
            $query->order([$this->getAlias() . '.rank' => 'ASC']);
        }
    }
}