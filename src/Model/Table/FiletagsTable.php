<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * File tags
 * Class FiletagsTable
 * @package App\Model\Table
 */
class FiletagsTable extends Table
{
    /**
     * Initialize method
     * @param array $config
     */
    public function initialize(array $config)
    {
        $this->setDisplayField('name');
        $this->belongsToMany('Files');
    }

    /**
     * Create validation
     * \
     * @param Validator $validator
     * @return Validator|\Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->add('name',
            ['unique' => [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __d('Media', 'Deze mapnaam wordt al gebruikt')]
            ]
        )->notEmpty('name');

        return $validator;
    }
    /**
     * Before save alter entity
     * @param $event
     * @param $entity
     * @param $options
     * @throws \Exception
     */
    public function beforeSave( $event, $entity, $options )
    {
        if( empty($entity->date_created) ){
            $entity->date_created = new \DateTime();
        }
    }
}