<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Collection\Collection;

/**
 * Files table
 * Class FilesTable
 * @package App\Model\Table
 */
class FilesTable extends Table
{

    public function initialize(array $config)
    {

        // turn on translations
        $this->addBehavior('Translate', ['fields' => [
            'alt',
            'title'
        ]]);

        $this->belongsToMany('Filetags', [
            'joinTable' => 'files_filetags',
        ]);
    }


    /**
     * Find all with query and use map for adding rank
     * @param $query
     * @param array $options
     *
     * @return mixed
     */
    public function findSorted($query, $options = [])
    {

        if (!empty($options)) {
            foreach ($options as $key => $option) {
                if (strpos($key, 'locale') !== false) {
                    $query->where([
                        'OR' => [
                            'Files.locale' => $option,
                            '(Files.locale IS NULL OR Files.locale = "")'
                        ]
                    ]);
                }
            }
        }

        return $query->formatResults(function ($results) {

            $sortResults = $results->map(function ($row) {
                $options = json_decode($row->options);
                if (isset($options->order)) {
                    $row['rank'] = $options->order;
                }

                if (!isset($options->order)) {
                    $row['rank'] = 99;
                }

                return $row;
            });

            $sortResults = new Collection($sortResults);
            $sortResults = $sortResults->sortBy('rank', 'ASC');

            return $sortResults;
        });
    }


    public function validationDefault(Validator $validator)
    {
        $validator->add('file', 'unique', [
            'rule' => 'validateUnique',
            'provider' => 'table',
            'message' => 'De gekozen bestandsnaam bestaat al',
            'on' => 'update'
        ]);

        return $validator;
    }

    public function beforeFind($event, $query, $options, $primary)
    {
        if (!empty($options['locale'])) {

            $query->where([
                'OR' => [
                    'Files.locale' => $options['locale'],
                    '(Files.locale IS NULL OR Files.locale = "")'
                ]
            ]);
        }

        $query->order(['CONVERT(LEFT(options, 14), BINARY) ASC']);

        return $query;
    }
}