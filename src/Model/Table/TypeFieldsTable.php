<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Typefields Model
 */
class TypeFieldsTable extends Table {


/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {

		$this->table('type_fields');
		$this->displayField('name');
		$this->primaryKey('id');

		$this->belongsTo('Types', [
			'alias' => 'Types',
			'className' => 'Types',
			'foreignKey' => 'type_id'
		]);

        // turn on translations
        $this->addBehavior('Translate', ['fields' => [
            'name', 'description'
        ]]);

	}

	/**
	 * Default ranking
	 * @param $event
	 * @param $query
	 * @param $options
	 * @param $primary
	 * @return mixed
	 */
	public function beforeFind( $event, $query, $options, $primary )
	{
		return $query->order(['rank' => 'asc']);
	}
}
