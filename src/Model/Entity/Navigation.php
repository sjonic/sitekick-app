<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Behavior\Translate\TranslateTrait;

/**
 * Navigation Entity.
 */
class Navigation extends Entity {

	use TranslateTrait;

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'parent_id' => true,
		'lft' => true,
		'rght' => true,
		'name' => true,
		'display' => true,
		'disable_header' => true,
		'active' => true,
		'has_content' => true,
		'template' => true,
		'autofill' => true,
		'rank' => true,
		'seo_title' => true,
		'seo_description' => true,
		'seo_index' => true,
		'slug' => true,
		'plugin' => true,
		'parent_navigation' => true,
		'child_navigations' => true,
		'pagecontainers' => true,
		'front_edit_title' => true,
		'headers' => true,
		'headers_navigations' => true,
		'cache' => true,
        'class' => true,
		'_joinData' => true

	];
	
    /**
     * Get parent name
     *
     * @return float|int
     */
    protected function _getParentName()
    {
		$table  = TableRegistry::get('Navigations');
		$parent = $table->find()->where(['Navigations.id' => $this->parent_id])->first();
		if( isset($parent) && !empty($parent) )
			$name = $parent->name;
		else 
			$name = "";
		
        return $name;
    }

    /**
     * Get parent
     *
     * @return array
     */
    protected function _getParent()
    {
        $table = TableRegistry::get('Navigations');
        $parent = $table->find()->where(['Navigations.id' => $this->parent_id])->first();
        if (!empty($parent)){
            return $parent;
        }
        return [];
    }


}
