<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\Filesystem\Folder;
use Imagick;


class File extends Entity
{
    use TranslateTrait;

    protected $_virtual = ['origin_page', 'url', 'thumbnail', 'cropped'];
    protected $_accessible = ['*' => true];

    /**
     * Get original file URL
     *
     * @return string
     */
    public function _getUrl()
    {
        if (file_exists(WWW_ROOT . 'files/original/' . $this->file)) {
            return Router::url("/", true) . 'files/original/' . $this->file;
        } else {
            return Router::url("/", true) . 'files/' . $this->plugin . '/' . $this->file;
        }
    }

    /**
     * Get thumbnail url
     *
     * @return string
     */
    public function _getThumbnail()
    {
        // get file locations
        if (file_exists(WWW_ROOT . 'files/' . $this->plugin . '/thumbnails/' . $this->file)) {
            return Router::url("/", true) . 'files/' . $this->plugin . '/thumbnails/' . $this->file;
        } elseif (file_exists(WWW_ROOT . 'files/original/' . $this->file)) {
            return Router::url("/", true) . 'files/original/' . $this->file;
        } else {
            return Router::url("/", true) . 'files/' . $this->plugin . '/' . $this->file;
        }
    }


    /**
     * Get cropped URL according to the specific settings
     *
     * @return string
     */
    public function _getCropped()
    {
        $settings = json_decode(Configure::read('settings'), true);
        $plugin = ucfirst($this->plugin);


        // first check if there are crop settings within an image
        $imageSettings = json_decode($this->options);
        if (!empty($imageSettings->cropper_width)) {
            $cropFolder = $imageSettings->cropper_width . 'x' . $imageSettings->cropper_height;
            // image contains cropped settings
        } elseif (!empty($imageSettings->width)) {
            $cropFolder = $imageSettings->width . 'x' . $imageSettings->height;
            // otherwise check for general crop settings
        } else {
            if (isset($settings[$plugin]) && !empty($settings[$plugin]['image_width'])) {
                $cropFolder = $settings[$plugin]['image_width'] . 'x' . $settings[$plugin]['image_height'];
            } elseif (isset($settings[$plugin]) && !empty($settings[$plugin]['cropper_width'])) {
                $cropFolder = $settings[$plugin]['cropper_width'] . 'x' . $settings[$plugin]['cropper_height'];
            } else {
                $cropFolder = '/';
                foreach ($settings as $key => $values) {
                    // for some stupid reason the plugin name must be in plural form. So we just add this stupid "s"...
                    if (!empty($values->content_model) && strtolower($values->content_model) == strtolower($plugin) . 's' && !empty($values->image_width)) {
                        $cropFolder = $values->image_width . 'x' . $values->image_height;
                        break;
                    }
                }
            }
        }

        // and as last we have to check if the image exist in that folder otherwise just return an other item
        $url = 'files/' . $this->plugin . '/cropped/';
        if (file_exists(WWW_ROOT . $url . $cropFolder . '/' . $this->file)) {
            return Router::url("/", true) . $url . $cropFolder . '/' . $this->file;
        } elseif (file_exists(WWW_ROOT . $url . '/' . $this->file)) {
            return Router::url("/", true) . $url . $this->file;
        } else {
            return Router::url("/", true) . 'files/original/' . $this->file;
        }


    }


    /**
     * Get cropped responsive image URL according to the specific settings
     *
     * @return string
     */
    protected function _getMobile(){ return $this->_getResponsive('mobile'); }
    protected function _getTablet(){ return $this->_getResponsive('tablet'); }
    protected function _getResponsive( $type = "mobile" )
    {
        $croppedURL = $this->_getCropped();
        $parts = explode('/',$croppedURL);
        $file = $type . '_' . end($parts);

        $parts[count($parts)-1] = $file;
        $checkURL = implode('/', $parts);

        // make a path from the url
        $checkPath = str_replace( Router::url("/", true) , '', $checkURL);

        if( file_exists( WWW_ROOT . $checkPath ) ){
            return $checkURL;
        }

        return $croppedURL;
    }

    /**
     * Get original size in MB
     *
     * @return string
     */
    public function _getSize()
    {
        if (file_exists(WWW_ROOT . 'files/original/' . $this->file)) {
            $path = WWW_ROOT . 'files/original/' . $this->file;
        } else {
            $path = WWW_ROOT . 'files/' . $this->plugin . '/' . $this->file;
        }

        return number_format((@filesize($path) / 1024 / 1024), 2);
    }

    /**
     * Get file extension
     *
     * @return string
     */
    public function _getFiletype()
    {
        return strtolower(pathinfo($this->file, PATHINFO_EXTENSION));
    }

    /**
     * Get origin page for a specific file.
     * This can be a page / news or item location
     *
     * @return item object | null
     */
    public function _getOriginPage()
    {
        switch (strtoupper($this->plugin)) {
            case "ITEMFIELDS":
                return $this->_getItemByItemField($this->parent_id);
                break;
            case "ITEMCONTAINERBLOCKITEM":
                return $this->_getItemByItemBlockItem($this->parent_id);
                break;
            case "PAGECONTAINERBLOCKITEM":
                return $this->_getItemByPageBlockItem($this->parent_id);
                break;
            case "NEWS":
                return $this->_getItemByNews($this->parent_id);
                break;
            case "MAILINGS":
                return $this->_getItemByMailings($this->parent_id);
                break;
            case "USERS":
                return $this->_getItemByUsers($this->parent_id);
                break;
            case "REDACTOR":
                return $this->_getItemByRedactor($this->file);
                break;
            case "HEADERS":
                return $this->_getItemByHeaders($this->parent_id);
                break;
        }

        return null;
    }

    /**
     * Get item according to an item field
     *
     * @param $id
     * @return null
     */
    protected function _getItemByItemField($id)
    {
        $ItemFields = TableRegistry::get('Items.ItemFields');
        $Items = TableRegistry::get('Items.Items');

        if ($item = $ItemFields->findById($id)->first()) {
            if ($item = $Items->findById($item->item_id)->first()) {
                $item->plugin = 'Items';
                return $item;
            }
        }

        return null;
    }

    /**
     * Get item according to the item block
     *
     * @param $id
     * @return null
     */
    protected function _getItemByItemBlockItem($id)
    {
        $BlockItems = TableRegistry::get('Items.ItemcontainerBlockItems');
        $Blocks = TableRegistry::get('Items.ItemcontainerBlocks');
        $Containers = TableRegistry::get('Items.Itemcontainers');
        $Items = TableRegistry::get('Items.Items');

        if ($item = $BlockItems->findById($id)->first()) {
            if ($item = $Blocks->findById($item->itemcontainer_block_id)->first()) {
                if ($item = $Containers->findById($item->itemcontainer_id)) {
                    if ($item = $Items->findById($item->item_id)) {
                        $item = $item->first();
                        $item->plugin = 'Items';
                        return $item;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Get item according to a page block
     *
     * @param $id
     * @return null
     */
    protected function _getItemByPageBlockItem($id)
    {
        $BlockItems = TableRegistry::get('Pages.PagecontainerBlockItems');
        $Blocks = TableRegistry::get('Pages.PagecontainerBlocks');
        $Containers = TableRegistry::get('Pages.Pagecontainers');
        $Items = TableRegistry::get('Navigations');

        if ($item = $BlockItems->findById($id)->first()) {
            if ($item = $Blocks->findById($item->pagecontainer_block_id)->first()) {
                if ($item = $Containers->findById($item->pagecontainer_id)->first()) {
                    if ($item = $Items->findById($item->navigation_id)->first()) {
                        $item->plugin = 'Pages';
                        return $item;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Get item according to the news
     *
     * @param $id
     * @return null
     */
    protected function _getItemByNews($id)
    {
        $News = TableRegistry::get('News.News');

        if ($news = $News->findById($id)->first()) {
            $news->plugin = 'News';
            $news->name = $news->title;
            return $news;
        }
        return null;

    }

    /**
     * Get item according to the users
     *
     * @param $id
     * @return null
     */
    protected function _getItemByUsers($id)
    {
        $Users = TableRegistry::get('Users.Users');
        if ($user = $Users->findById($id)->first()) {
            $user->plugin = 'Users';
            $user->name = $user->firstname;
            return $user;
        }
        return null;
    }

    /**
     * Get item by filename from redactor
     *
     * @param $filename
     * @return null
     */
    protected function _getItemByRedactor($filename)
    {
        $translateTable = TableRegistry::get('i18n');
        $result = $translateTable->find('all')->where(['content LIKE "%' . $filename . '%"'])->first();
        if (!empty($result)) {
            switch ($result->model) {
                case "PagecontainerBlockItems":
                    return $this->_getItemByPageBlockItem($result->foreign_key);
                    break;
                case "ItemcontainerBlockItems":
                    return $this->_getItemByItemBlockItem($result->foreign_key);
                    break;

            }
        }
        return null;
    }

    protected function _getItemByHeaders($id)
    {
        $table = TableRegistry::get('Headers.Headers');
        if ($item = $table->findById($id)->first()) {
            $item->plugin = 'Headers';
            $item->name = 'header #' . $id;
            return $item;
        }
        return null;
    }

    /**
     * Get item according to the mailing plugin
     *
     * @param $id
     * @return mixed
     */
    protected function _getItemByMailings($id)
    {
        $MailingBlocks = TableRegistry::get('MailingBlocks');
        $Mailings = TableRegistry::get('Mailings.Mailings');
        if ($block = $MailingBlocks->findById($id)->first()) {
            if ($mailing = $Mailings->findById($block->mailing_id)->first()) {
                $mailing->plugin = 'Mailings';
                $mailing->name = $mailing->title;
                return $mailing;
            }
        }
    }
}