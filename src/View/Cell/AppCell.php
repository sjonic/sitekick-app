<?php

namespace App\View\Cell;

use Cake\View\Cell;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\I18n;

class AppCell extends Cell {

    public function navbar($languages, $activeLanguage, $settings, $types, $authUser, $back_url, $front_url, $SitekickAreaActive) {

        # Set locale User
        I18n::setLocale( $this->request->getSession()->read('Auth.User.locale') );

        if( $this->request->getParam('prefix') == 'sitekick' ) {
            $this->set('languages', $languages);
            $this->set('activeLanguage', $activeLanguage);
        } else {
            $this->set('load_translations', true);
        }

        $this->set('settings', $settings);
        $this->set('types', $types);
        $this->set('authUser', $authUser);
        $this->set('back_url', $back_url);
        $this->set('front_url', $front_url);
        $this->set('SitekickAreaActive', $SitekickAreaActive);
    }
}