<?php

namespace App\View\Helper;

use App\Controller\Component\NextGenComponent;
use App\Controller\Component\UserComponent;
use Cake\Filesystem\Folder;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\View\Helper\HtmlHelper as RootHelper;
use Cake\Core\Configure;
use Mobile_Detect;
use App\Model;


class InlineHelper extends RootHelper
{


    public $helpers = ['Html', 'Url', 'Element'];
    static $scripts = [];

    /**
     * Inline media edit
     * @param string $options
     * @throws \ImagickException
     */

    public function inlineImage($options)
    {

        // check entity
        if (is_array($options['entity'])) {
            $options['entity'] = json_decode(json_encode($options['entity']), FALSE);
        }

        //extract plugin
        $className = get_class($options['entity']);
        $arrClass = explode("\\", $className);
        $arrClassReversed = array_reverse($arrClass);
        $plugin = reset($arrClassReversed);
        $pluginSettings = reset($arrClass);

        //overrule plugin if needed
        if (isset($options['plugin'])) {
            $plugin = $options['plugin'];
            $pluginSettings = $options['plugin'];
        }

        //get settings if none found use fallback
        $sitekickSettings = json_decode(Configure::read('settings'), true);

        if (empty($options['entity']->options)) {
            if (in_array($pluginSettings, ["ItemFields", "ItemsFields", "Items"])) {
                $settings = $sitekickSettings["Items"];
            } else {
                $settings = $sitekickSettings[$pluginSettings];
            }
        } else {
            $settings = json_decode($options['entity']->options, TRUE);
        }


        // fallback for options
        if (isset($options['entity']->fields)) {
            if (!empty($options['entity']->fields->{$options['field']}) && isset(reset($options['entity']->fields->{$options['field']})->options)) {
                $settings = json_decode(reset($options['entity']->fields->{$options['field']})->options, TRUE);
            }
        }

        //check if cropper overrides
        if (isset($options['single'])) {
            $settings['single'] = $options['single'];
        }

        //check if cropper overrides
        if (isset($options['cropper'])) {
            $settings['cropper'] = $options['cropper'];
        }


        //if isset field then find correct entity for field
        if (isset($options['field']) && ($plugin = "Items" or $plugin = "ItemsFields")) {

            if (!empty($options['entity']->fields->{$options['field']})) {
                $options['entity']->files = $options['entity']->fields->{$options['field']};
                if (isset($options['entity']->files)) {
                    $plugin = $options['entity']->files;
                }

                if (is_array($plugin) && !empty($plugin)) {
                    $plugin = reset($plugin)->plugin;
                } else {
                    $plugin = 'Items';
                }
            }
        }

        //determine id
        $itemId = $options['entity']->id;

        if (isset($options['id'])) {
            $itemId = $options['id'];
        }

        //overrule plugin if needed
        if (isset($options['plugin'])) {
            $plugin = $options['plugin'];
        }

        //show image/images or show cropper
        if (!$this->request->getSession()->read('Auth.User')) {

            if (empty($options['return'])) {

                echo '<span class="image-wrapper">';
                if ( !empty($options['entity']->files) && count($options['entity']->files) > 1) {
                    echo $this->_View->Element('Pages.gallery', [
                        'images' => $options['entity']->files,
                        'options' => json_decode($options['entity']->options)
                    ]);
                } elseif (!empty($options['entity']->files)) {

                    //check for alt tag
                    $alt = reset($options['entity']->files)->alt;
                    $title = reset($options['entity']->files)->title;


                    //set correct plugin
                    if (strtolower($plugin) == "pagecontainerblockitem") {
                        $plugin = "Pages";
                    }

                    if (strtolower($plugin) == "itemblockitem") {
                        $plugin = "Items";
                    }

                    if (strtolower($plugin) == "itemfields") {
                        $plugin = "Items";
                    }


                    if (strtolower($plugin) == "headerfiles") {
                        $plugin = "Headers";
                    }

                    if ($plugin == "news") {
                        $plugin = "News";
                    }

                    //set lazyload on by default
                    $lazyload = 1;

                    //set default lazyload from plugin setting
                    if (isset($sitekickSettings[$plugin]['lazyload'])) {
                        $lazyload = $sitekickSettings[$plugin]['lazyload'];
                    }

                    //overrule lazyload per image if set (from image options)
                    $imageOptions = json_decode($options['entity']->options);
                    $fileOptions = json_decode(reset($options['entity']->files)->options);
                    if (isset($imageOptions->lazyload)) {
                        $lazyload = $imageOptions->lazyload;
                    }

                    // check if cropped loaded or original file
                    $fileUrl = reset($options['entity']->files)->cropped;

                    if (empty($options['force_desktop'])) {
                        $mobileDetect = new Mobile_Detect();
                        if ($mobileDetect->isMobile() && !$mobileDetect->isTablet()) {
                            $mobileUrl = reset($options['entity']->files)->mobile;
                            if( !empty($mobileUrl) ){
                                $fileUrl = $mobileUrl;
                            }

                        } elseif( $mobileDetect->isTablet() ){
                            $tableteUrl = reset($options['entity']->files)->tablet;
                            if( !empty($tableteUrl) ){
                                $fileUrl = $tableteUrl;
                            }
                        }

                    }

                    if (isset($settings['cropper']) && $settings['cropper'] != true) {
                        $fileUrl = reset($options['entity']->files)->url;
                    }

                    // create nice lazyload preview
                    $path = str_replace( Router::url('/', true), '', $fileUrl);
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $base64 = 'data:image/gif;base64,R0lGODlhDwAPAMQAAODg4Ofn5/X19ePj4/b29vLy8u/v7+np6e3t7fT09MfJydrb2+jo6N/f397e3vDw8PHx8ejp6f7+/vj4+Obm5snKyvPz8+rq6uHh4eTk5P///+Li4gAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDUgNzkuMTYzNDk5LCAyMDE4LzA4LzEzLTE2OjQwOjIyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOSAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpCQjg4MUY3OTQ4OTgxMUU5QjM3RkQzQUQ3QzFGMzc0MyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpCQjg4MUY3QTQ4OTgxMUU5QjM3RkQzQUQ3QzFGMzc0MyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkJCODgxRjc3NDg5ODExRTlCMzdGRDNBRDdDMUYzNzQzIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkJCODgxRjc4NDg5ODExRTlCMzdGRDNBRDdDMUYzNzQzIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAAAAAAAsAAAAAA8ADwAABU9gJo5kuZ1oqq4si7VqdmHZALQYQlDV8myvFIYhEBgUBk0iiBoUBBaCREMt3E45QmJC7WogzID4cImQL+gBCsPGAByNNnMFmMPssFZpTwoBADs=';
                    if( file_exists(WWW_ROOT . DS . $path) && in_array(strtolower($type), ['jpg', 'jpeg', 'gif', 'png', 'webp', 'jpg2']) ){
                        $imagick = new \Imagick( WWW_ROOT . DS . $path );
                        $extension = $this->_getSupportedFormat();
                        $imagick->setImageFormat($extension);
                        $imagick->scaleImage(30,0);
                        $imagick->stripImage();
                        $lazyFolder = new Folder(WWW_ROOT . DS . 'files' . DS . 'lazyload', true);
                        $lazyName   = strtolower(Text::slug($path)) . '.' . $extension;
                        $imagick->writeImage( $lazyFolder->path . DS . $lazyName);
                        $base64 = $this->Url->build('/files/lazyload/' . $lazyName);
                    }

                    //lazyload image
                    if ($lazyload == 1) {
                        $imgOptions = [
                            'data-original' => $fileUrl,
                            'class' => 'sitekick-lazyload',
                            'title' => $title,
                            'alt'=> $alt
                        ];
                        if( !empty($fileOptions->cropper_width) ){
                            $imgOptions['width'] = $fileOptions->cropper_width;
                        }
                        if( !empty($fileOptions->cropper_width) ){
                           $imgOptions['height'] = $fileOptions->cropper_height;
                        }
                        $image = $this->Html->image($base64, $imgOptions);
                    } else {
                        $image = $this->Html->image($fileUrl, ['alt' => $alt, 'title' => $title]);
                    }

                    //check link options
                    $target = '';
                    $href = '';

                    //if href/target set for image
                    if (!empty(reset($options['entity']->files)->href)) {
                        $href = reset($options['entity']->files)->href;
                    }

                    if (!empty(reset($options['entity']->files)->target)) {
                        $target = reset($options['entity']->files)->target;
                    }

                    //overrule image href/target if isset in options
                    if (!empty($options['href'])) {
                        $href = $options['href'];
                    }

                    if (!empty($options['target'])) {
                        $target = $options['target'];
                    }


                    if (!empty($href)) {
                        echo $this->Html->Link($image, $href, ['target' => $target, 'escape' => false, 'alt' => $alt, 'title' => $title]);
                    }

                    if (empty($href)) {
                        echo $image;
                    }


                    echo $this->Html->script('lazyload.min.js', ['block' => 'scriptBottom']);


                }
                echo '</span>';

            }

        } else {

            # Hide cropper if device is mobile and files are empty
            if (!($this->isTablet == false && $this->request->isMobile() == true && empty($options['entity']->files))) {
                echo $this->_View->Element('Media.Sitekick/images', [
                    'item' => $options['entity'],
                    'id' => $itemId,
                    'single' => (isset($settings['single']) && $settings['single'] == false) ? false : true,
                    'cropper' => (isset($settings['cropper']) && $settings['cropper'] == false) ? false : true,
                    'plugin' => $plugin,
                    'settings' => $settings
                ]);
            }
        }
    }


    /**
     * Inline edit
     * @param $options
     * [
     *  (optional) editor: regular | title
     *  entity: object (if array will be converted to object)
     *  field: fieldname
     *    (optional) controller: controller name
     *  (optional) plugin: plugin name
     * ]
     */
    public function edit($options)
    {

        $user = $this->request->session()->read('Auth.User');

        // check entity
        if (is_array($options['entity'])) {
            $options['entity'] = json_decode(json_encode($options['entity']), FALSE);;
        }

        // default options
        $editor = (empty($options['editor'])) ? 'regular' : $options['editor'];

        //extract plugin
        $className = get_class($options['entity']);
        $arrClass = explode("\\", $className);
        $plugin = reset($arrClass);
        $dataField = 'name';

        $controller = $plugin;

        if ($plugin == "Pages") {
            $controller = "Pagecontainers";
        }

        //overrule controller if needed
        if (isset($options['controller'])) {
            $controller = $options['controller'];
        }

        //overrule plugin if needed
        if (isset($options['plugin'])) {
            $plugin = $options['plugin'];
        }


        //is field of main entity
        if (isset($options['entity']->{$options['field']})) {
            $dataField = $options['field'];
            $content = $options['entity']->{$options['field']};
        } else {
            //find data field (fields is)
            if (isset($options['entity']->item_fields)) {
                foreach ($options['entity']->item_fields as $itemField) {
                    if (is_array($itemField)) {
                        if (!empty($itemField['field']['key']) && $itemField['field']['key'] == $options['field']) {
                            $dataField = $itemField['field_id'];
                            break;
                        }
                    } elseif (is_object($itemField)) {
                        if (!empty($itemField->field) && $itemField->field->key == $options['field']) {
                            $dataField = $itemField->field_id;
                            break;

                        }
                    }
                }
            }

            $content = (isset($options['entity']->fields->{$options['field']})) ? $options['entity']->fields->{$options['field']} : '';
        }

        // Check if sitekick_load element exists in content, if so, replace with the correct element
        $settings = Configure::read("settings");
        $settings = json_decode($settings);
        preg_match('/\[sitekick_element (.*)\]/', $content, $match);
        if (!empty($match[1])) {
            $src = explode('=', $match[1]);
            $src = str_replace('"', '', $src[1]);

            if (file_exists(ROOT . '/plugins' . DS . $settings->Base->theme . '/src/Template/Element/' . $src . '.ctp')) {
                $element = file_get_contents(ROOT . '/plugins' . DS . $settings->Base->theme . '/src/Template/Element/' . $src . '.ctp');
                $content = str_replace($match[0], $element, $content);
            }
        }

        // Check if sitekick_load element exists in content, if so, replace with the correct element
        preg_match('/\[sitekick_load (.*)\]/', $content, $extractLoader); // Check if shortcode exists in content

        if (!empty($extractLoader[1]) && !$this->request->session()->read('Auth.User')) {
            $shortcode_options = $this->Html->processShortcode($content);
            if (is_array($shortcode_options)) {
                $template = isset($shortcode_options['template']) ? $shortcode_options['template'] : $shortcode_options['type'];
            }

            $html = (is_array($shortcode_options)) ? $this->_View->cell($shortcode_options['plugin'], [$shortcode_options])->render($template) : "";
            $content = preg_replace('/\[sitekick_load (.*)\]/', $html, $content);
        }


        if (!$user) {

            if (!empty($options['href'])) {
                echo $this->Html->Link($content, $options['href'], ['escape' => false]);
            }

            if (empty($options['href'])) {
                echo $content;
            }


        } else {
            if (empty($extractLoader[1]) || $user['role'] == 'admin') {
                echo '<span class="inline-edit inline-edit-' . strtolower($controller) . '" data-plugin="' . $plugin . '" data-controller="' . $controller . '" data-id="' . $options['entity']->id . '-' . $dataField . '" data-field="' . $dataField . '" data-editor="' . $editor . '">' . ((!empty($content)) ? $content : '<i>' . __('Klik om te bewerken') . '</i>') . '</span>';
            } else {
                echo 'Hier worden items uitgelezen.';
            }
        }
    }

    /**
     * Get next-gen image formats based on browser
     * @return string
     */
    protected function _getSupportedFormat()
    {
        $nextGenComponent = new NextGenComponent( new \Cake\Controller\ComponentRegistry(), [] );

        $allowedFormates = \Imagick::queryFormats('*');
        $nextGenExtension = 'jpg';
        if( $nextGenComponent->isCompatible() ){
            $ext = $nextGenComponent->getExtension();
            if( in_array(strtoupper($ext), $allowedFormates) ){
                $nextGenExtension = $ext;
            }
        }
        return $nextGenExtension;
    }
}