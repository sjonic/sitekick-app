<?php
namespace App\View\Helper;

use Cake\View\Helper;

class isAllowedHelper extends Helper {


	/**
	 * Check if allowed based on user role
	 * @param $role
	 *
	 * @return bool
	 */
	public function role($role) {

		if(isset($this->_View->viewVars['authUser'])) {

			$authUser = $this->_View->viewVars['authUser'];
			if($authUser['role'] == $role){
				return true;
			}

		}

		return false;
	}


	/**
	 * Check if allow based on plugin
	 * @param $plugin
	 *
	 * @return bool
	 */
	public function plugin($plugin) {



		if(isset($this->_View->viewVars['authUser'])) {
			$authUser = $this->_View->viewVars['authUser'];


            if ( $authUser['superadmin'] == 1 or $authUser['role'] == 'admin') {
                return true;
            }


			$pluginPermissions = $this->_View->viewVars['pluginPermissions'];

			foreach ( $pluginPermissions as $permission ) {
				if ( $permission->plugin == $plugin && $permission->allow == 1 ) {
					return true;
				}
			}


			if ( $authUser['superadmin'] == 1) {
				return true;
			}

			if($authUser['role'] == 'admin' && $plugin != "Users"){
				return true;
			}

		}

		return false;
	}
}