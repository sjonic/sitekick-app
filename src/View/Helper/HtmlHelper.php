<?php

namespace App\View\Helper;

use App\Controller\Component\NextGenComponent;
use App\Controller\Component\UserComponent;
use Cake\Filesystem\Folder;
use Cake\ORM\TableRegistry;
use Cake\Routing\Route\Route;
use Cake\View\Helper\HtmlHelper as RootHelper;
use Cake\Routing\Router;

class HtmlHelper extends RootHelper
{

    /**
     * Create a link
     * @param array|string $title
     * @param null $url
     * @param array $options
     * @return null|string
     */
    function link($title, $url = null, array $options = [])
    {
        // the first param is an array for fetching a navigation
        if (is_array($title) && !empty($title[1]) && is_int($title[1])) {

            list($plugin, $page_id) = $title;

            if (strtolower($plugin) == 'pages') {
                $source = TableRegistry::getTableLocator()->get('Navigations');
                $page = $source->find()->where(['Navigations.id' => $page_id])->first();
                if( !empty($page) ){
                    $title = $page->name;
                    $url = '/' . $page->slug;
                }
            } else {
                $source = TableRegistry::getTableLocator()->get(ucfirst($plugin));
                $page = $source->find()->where([ucfirst($plugin) . '.id' => $page_id])->first();

                if ($plugin == 'news') {
                    $plugin_url = __('news') . '/';
                }

                if ($plugin == 'items') {
                    $types = TableRegistry::getTableLocator()->get('Types');
                    $type = $types->find()->where(['Types.content_type' => $page->type])->first();
                    $plugin_url = $type->menu_slug . '/';
                }

                $title = $page->name;
                $url = '/' . $plugin_url . $page->slug;
            }
        }

        $escapeTitle = true;

        // check for multilanguage ( restricted for sitekick)
        $languages = TableRegistry::getTableLocator()->get('Languages');
        $check = $languages->find()->where(['active' => 1])->count();

        $active = $this->request->session()->read('Config.language');

        if ($this->request->param('prefix') == "sitekick" && $this->request->session()->check('Config.language_sitekick')) {
            $active = $this->request->session()->read('Config.language_sitekick');
        }

        if (!is_array($url) && strpos((string)$url, 'javascript') !== FALSE) {

        } elseif (is_array($url)) {
            if ($check > 1) {
                $url['lang'] = $active->abbreviation;
            } else {
                $url['lang'] = '';
            }

        } elseif (strpos((string)$url, "sitekick") === FALSE) {

            if ($check > 1 && strpos($url, 'http') === FALSE
                && strpos($url, 'mailto:') === FALSE
                && strpos($url, 'tel:') === FALSE
                && strpos($url, '#') === FALSE
                && strpos($url, '/' . $active->abbreviation . '/') === FALSE
                && (isset($options['lang']) && $options['lang'] !== FALSE || !isset($options['lang']))) {
                $url = '/' . $active->abbreviation . $url;
            }
        }

        if ($url !== null) {
            $url = $this->Url->build($url);
        } else {
            $url = $this->Url->build($title);
            $title = htmlspecialchars_decode($url, ENT_QUOTES);
            $title = h(urldecode($title));
            $escapeTitle = false;
        }

        if (isset($options['escapeTitle'])) {
            $escapeTitle = $options['escapeTitle'];
            unset($options['escapeTitle']);
        } elseif (isset($options['escape'])) {
            $escapeTitle = $options['escape'];
        }

        if ($escapeTitle === true) {
            $title = h($title);
        } elseif (is_string($escapeTitle)) {
            $title = htmlentities($title, ENT_QUOTES, $escapeTitle);
        }

        $confirmMessage = null;
        if (isset($options['confirm'])) {
            $confirmMessage = $options['confirm'];
            unset($options['confirm']);
        }
        if ($confirmMessage) {
            $options['onclick'] = $this->_confirm($confirmMessage, 'return true;', 'return false;', $options);
        }

        $templater = $this->templater();

        $options = [
            'url' => $url,
            'attrs' => $templater->formatAttributes($options),
            'content' => $title
        ];


        return $templater->format('link', $options);
    }

    /**
     * Fetch a page and generate a link for it
     * @param $plugin
     * @param $page_id
     * @return string
     */
    function pagelink($plugin, $page_id)
    {

        $root = Router::url('/', true);

        if ($plugin == 'pages') {
            $source = TableRegistry::getTableLocator()->get('Navigations');
            $page = $source->find()->where(['Navigations.id' => $page_id])->first();
            $plugin_url = "";
        } else {
            $source = TableRegistry::getTableLocator()->get(ucfirst($plugin));
            $page = $source->find()->where([ucfirst($plugin) . '.id' => $page_id])->first();

            if ($plugin == 'news') {
                $plugin_url = __('news') . '/';
            }

            if ($plugin == 'items') {
                $types = TableRegistry::getTableLocator()->get('Types');
                $type = $types->find()->where(['Types.content_type' => $page->type])->first();
                $plugin_url = $type->menu_slug . '/';
            }
        }

        # check for multilanguage ( restricted for sitekick)
        $languages = TableRegistry::getTableLocator()->get('Languages');
        $check = $languages->find()->where(['active' => 1])->count();

        if ($check > 1) {
            $lang = $this->request->session()->read('Config.language');
            $pagelink = $root . $lang->abbreviation . '/' . $plugin_url . $page->slug;
        } else {
            $pagelink = $root . $plugin_url . $page->slug;
        }

        return $pagelink;
    }


    /**
     * Fetch an object for path to image and generate an image
     * @param $image
     * @param $options
     * @return string
     */
    function image($path, array $options = [])
    {

        if (is_int($path)) {
            $files = TableRegistry::getTableLocator()->get('Files');
            $image = $files->find()->where(['Files.id' => $path])->first();

            if ($image) {

                if (!empty($image['title'])) {
                    $title = ' title="' . $image['title'] . '"';
                } else {
                    $title = '';
                }

                if (!empty($image['target'])) {
                    $target = ' target="' . $image['target'] . '"';
                } else {
                    $target = '';
                }

                $additional = '';
                if (!empty($options)) {
                    foreach ($options as $key => $value) {
                        $additional .= ' ' . $key . '="' . $value . '"';
                    }
                }

                $img = '';
                if (isset($image['href']) && !empty($image['href'])) {
                    $img .= '<a href="' . $image['href'] . '" ' . $title . '' . $target . '>';
                }

                if( empty($options['nonextgen']) ){
                    $imagePath = $this->getNextGenPath($image['cropped']);
                } else {
                    $imagePath = $image['cropped'];
                }

                $img .= '<img src="' . $imagePath . '" alt="' . $image['cropped'] . '"' . $title . '' . $additional . ' />';

                if (isset($image['href']) && !empty($image['href'])) {
                    $img .= '</a>';
                }

                return $img;

            } else {
                return 'Ongeldige afbeelding';
            }

        } elseif (is_object($path) || is_array($path)) {

            if (is_object($path)) {
                $path = (array)$path;
            }

            $image = reset($path);

            if (isset($image['file'])) {

                if (!empty($image['title'])) {
                    $title = ' title="' . $image['title'] . '"';
                } else {
                    $title = '';
                }

                if (!empty($image['target'])) {
                    $target = ' target="' . $image['target'] . '"';
                } else {
                    $target = '';
                }

                $additional = '';
                if (!empty($options)) {
                    foreach ($options as $key => $value) {
                        $additional .= ' ' . $key . '="' . $value . '"';
                    }
                }

                $img = '';
                if (isset($image['href']) && !empty($image['href'])) {
                    $img .= '<a href="' . $image['href'] . '" ' . $title . '' . $target . '>';
                }

                $alt = (!empty($image['alt'])) ? $image['alt'] : '';

                if( empty($options['nonextgen']) ){
                    $imagePath = $this->getNextGenPath($image['cropped']);
                } else {
                    $imagePath = $image['cropped'];
                }

                $img .= '<img src="' . $imagePath . '" alt="' . $alt . '"' . $title . '' . $additional . ' />';

                if (isset($image['href']) && !empty($image['href'])) {
                    $img .= '</a>';
                }
                return $img;
            } else {
                return 'Ongeldige afbeelding';
            }
        } else {
            $path = $this->Url->image($path, $options);
            if( empty($options['nonextgen']) ) {
                $path = $this->getNextGenPath($path);
            }
            $options = array_diff_key($options, ['fullBase' => null, 'pathPrefix' => null]);

            if (!isset($options['alt'])) {
                $options['alt'] = '';
            }

            $url = false;
            if (!empty($options['url'])) {
                $url = $options['url'];
                unset($options['url']);
            }

            $templater = $this->templater();
            $image = $templater->format('image', [
                'url' => $path,
                'attrs' => $templater->formatAttributes($options),
            ]);

            if ($url) {
                return $templater->format('link', [
                    'url' => $this->Url->build($url),
                    'attrs' => null,
                    'content' => $image
                ]);
            }

            return $image;
        }
    }


    /**
     * Process shorcodes
     * @param $content
     * @return array
     */
    function processShortcode($content)
    {
        // Check if content has shortcode
        if (false === strpos($content, '[')) return $content;

        // Get shortcode
        preg_match('/\[sitekick_load (.*)\]/', $content, $shortcode);

        $options = null;

        if (!empty($shortcode)) {

            // Read custom filters
            $filters = explode(' ', $shortcode[1]);
            $options = [];

            //Build options array
            foreach ($filters as $key => $filter) {
                if (empty($filter)) continue;

                // Split filter at equal char
                $filter = explode('=', $filter, 2);

                if (in_array($filter[0], ['plugin', 'order', 'limit', 'type', 'template', 'contain', 'debug', 'pagination', 'page'])) {

                    if ($filter[0] == 'limit') {
                        $filter[1] = str_replace(';', ' OFFSET ', $filter[1]);
                    }

                    $filter[1] = str_replace('"', '', $filter[1]);
                    $options[$filter[0]] = $filter[1];
                    unset($filters[$key]);
                } else {
                    // Check if filter has custom operator

                    if (preg_match('/\[.*\]/', $filter[1], $operator)) {
                        $filter['operator'] = $operator[0];
                        $filter[1] = str_replace($filter['operator'], "", $filter[1]);
                    } else {
                        $filter['operator'] = "[==]";
                    }

                    if (substr(str_replace('"', '', $filter[1]), 0, 1) == '_') {
                        $key = substr(str_replace('"', '', $filter[1]), 1);
                        if (isset($this->request->query[$key])) {
                            if ($filter['operator'] == '[%%]') {
                                $filter[1] = '"%' . trim($this->request->query[$key], '"') . '%"';
                            } else {
                                $filter[1] = '"' . trim($this->request->query[$key], '"') . '"';
                            }
                            $options['filters'][$filter[0]] = [
                                'value' => $filter[1],
                                'operator' => $filter['operator']
                            ];
                        }
                    } else {
                        $options['filters'][$filter[0]] = [
                            'value' => $filter[1],
                            'operator' => $filter['operator']
                        ];
                    }
                }
            }
        }

        return $options;
    }

    /**
     * Add breadcrumbs
     * @param $page_id
     * @param string $plugin
     * @param int $levels
     * @return array
     */
    function getBreadcrumbs($page_id, $plugin = 'pages', $levels = 5)
    {

        $root = Router::url('/', true);

        # check for multilanguage ( restricted for sitekick)
        $languages = TableRegistry::getTableLocator()->get('Languages');
        $check = $languages->find()->where(['active' => 1])->count();

        if ($check > 1) {
            $lang = $this->request->session()->read('Config.language');
            $root = $root . $lang->abbreviation . '/';
        }

        $breadcrumbs = [];
        if ($plugin == 'pages') {

            $source = TableRegistry::getTableLocator()->get('Navigations');

            for ($i = 0; $i < $levels; $i++) {

                $page = $source->find()->where(['Navigations.id' => $page_id])->first();
                $breadcrumbs[] = [
                    'name' => $page->name,
                    'slug' => (!empty($page->has_content)) ? $root . $page->slug : $this->request->here . '#'
                ];

                if ($page->parent_id > 0) {
                    $page_id = $page->parent_id;
                } else {
                    break;
                }
            }
        }

        $breadcrumbs = array_reverse($breadcrumbs);
        return $breadcrumbs;
    }

    /**
     * Minify script
     */
    function minifyScript($url, $options = [])
    {
        if (!is_array($url)) {
            $url = [$url];
        }

        $theme = $this->theme;
        $useThemeDir = true;

        // check if theme webroot is in the normal webroot
        if (is_dir(WWW_ROOT . '/' . strtolower($theme))) {
            $useThemeDir = false;
        }

        $combineLinks = [];
        $externalLinks = [];

        foreach ($url as $link) {
            // check if link contains plugin/theme link
            if (strpos($link, $this->theme . '.') !== false) {
                if ($useThemeDir) {
                    $link = str_replace($this->theme . '.', Router::url('/plugins/' . $this->theme . '/webroot/js/'), $link);
                } else {
                    $link = str_replace($this->theme . '.', Router::url(strtolower($this->theme) . '/js/'), $link);
                }

                if (strpos($link, '.js') === false) {
                    $link .= '.js';
                }

                $combineLinks[] = $link;
            } elseif (strpos($link, '//') !== false) {
                $externalLinks[] = $link;
            } else {
                $link = Router::url('/webroot/js/') . $link;
                $combineLinks[] = $link;
            }

        }

        $out = $this->formatTemplate('javascriptlink', [
            'url' => Router::url('/min') . '/f=' . implode(',', $combineLinks),
            'attrs' => $this->templater()->formatAttributes($options, ['block', 'once']),
        ]);

        // add external scripts too
        if (!empty($externalLinks)) {
            foreach ($externalLinks as $scriptLink) {
                $out .= $this->formatTemplate('javascriptlink', [
                    'url' => $scriptLink,
                    'attrs' => $this->templater()->formatAttributes($options, ['block', 'once']),
                ]);
            }
        }

        if (empty($options['block'])) {
            return $out;
        }

        if ($options['block'] === true) {
            $options['block'] = __FUNCTION__;
        }
        $this->_View->append($options['block'], $out);
    }


    /**
     * Start minifieing HTML
     * @param array $options
     */
    public function minifyStart(array $options = [])
    {
        ob_start();
    }

    /**
     * End of minify part
     */
    public function minifyEnd()
    {
        $buffer = ob_get_clean();
        echo preg_replace('#(?ix)(?>[^\S ]\s*|\s{2,})(?=(?:(?:[^<]++|<(?!/?(?:textarea|pre)\b))*+)(?:<(?>textarea|pre)\b|\z))#', ' ', $buffer);
    }

    /**
     * Get a JPEG2000 or WEBP image format if it's available for the correct browser
     * @param string $path
     * @param boolean $force
     * @return string
     * @throws \ImagickException
     */
    function getNextGenPath($path = '', $force = false)
    {
        $nextGenComponent = new NextGenComponent( new \Cake\Controller\ComponentRegistry(), [] );
        $allowedFormates = \Imagick::queryFormats('*');


        // check if the requested file is from within the uploads-path
        if (strpos($path, 'files/') !== false || $force === true) {

            // check if the browser has the correct version
            if ( $nextGenComponent->isCompatible() ) {

                // convert path to server-path
                $path = explode('?', $path);
                $path = reset($path);
                $fileUrl = trim(str_replace(Router::url('/', true), '', $path), '/');
                $filePath = WWW_ROOT . $fileUrl;


                // get correct filename + extension
                $fileParts = explode('/', $filePath);
                $filename = end($fileParts);
                $extension = pathinfo($filename, PATHINFO_EXTENSION);
                $filedir = str_replace($filename, '', $fileUrl);

                $newExtension = $nextGenComponent->getExtension();

                if (!empty($newExtension) && in_array(strtoupper($newExtension), $allowedFormates)) {
                    if (in_array($extension, ['jpg', 'jpeg', 'png', 'gif'])) {

                        // set new location + filename
                        $newImageLocation = new Folder(WWW_ROOT . 'files' . DS . $newExtension . DS . $filedir, true);
                        $newFilename = $filename . '.' . $newExtension;

                        // convert file (if it's not already converted)
                        $success = false;
                        if (file_exists($newImageLocation->path . $newFilename)) {
                            // file exists
                            $success = true;
                        } elseif (file_exists($filePath)) {
                            try {
                                $imagick = new \Imagick($filePath);
                                $imagick->setImageFormat($newExtension);
                                $imagick->writeImage($newImageLocation->path . $newFilename);
                                $success = true;
                            } catch (\ImagickException $error) {
                                // oops imagick error....
                            }
                        }

                        if ($success === true) {
                            $path = Router::url('/files/' . $newExtension . '/' . $filedir . $newFilename);
                        }

                    }
                }
            }
        }
        return $path;
    }
}